/*
 * mkbldcdriver-v03.c
 *
 * Created: 1/14/2018 8:20:39 PM
 * Author : Jake
 */ 


#include "sam.h"
#include "hardware.h"
#include "encoder.h"
#include <stdio.h>

void comticker_init(uint32_t cc){
	// Timers: in 32 bit mode we pair two - obscure datasheet reading later, they pair in a predefined way: 0 with 1...
	// a word of warning: with the same code, a 16-bit timer was not working. I am mystified.
	TC2->COUNT32.CTRLA.bit.ENABLE = 0;
	TC3->COUNT32.CTRLA.bit.ENABLE = 0;
	// unmask clocks
	MCLK->APBBMASK.reg |= MCLK_APBBMASK_TC2 | MCLK_APBBMASK_TC3; // at 15.8.9
	// generate a gclk
	GCLK->GENCTRL[11].reg = GCLK_GENCTRL_SRC(GCLK_GENCTRL_SRC_DFLL) | GCLK_GENCTRL_GENEN;
	while(GCLK->SYNCBUSY.reg & GCLK_SYNCBUSY_GENCTRL(11));
	// ship gclk to their channels
	GCLK->PCHCTRL[TC2_GCLK_ID].reg = GCLK_PCHCTRL_CHEN | GCLK_PCHCTRL_GEN(11);
	GCLK->PCHCTRL[TC3_GCLK_ID].reg = GCLK_PCHCTRL_CHEN | GCLK_PCHCTRL_GEN(11);
	// turn on in mode, presync
	TC2->COUNT32.CTRLA.reg = TC_CTRLA_MODE_COUNT32 | TC_CTRLA_PRESCSYNC_PRESC | TC_CTRLA_PRESCALER_DIV16 | TC_CTRLA_CAPTEN0;// | TC_CTRLA_CAPTEN1;
	TC3->COUNT32.CTRLA.reg = TC_CTRLA_MODE_COUNT32 | TC_CTRLA_PRESCSYNC_PRESC | TC_CTRLA_PRESCALER_DIV16 | TC_CTRLA_CAPTEN0;// | TC_CTRLA_CAPTEN1;
	// do frequency match
	TC2->COUNT32.WAVE.reg = TC_WAVE_WAVEGEN_MFRQ;
	TC3->COUNT32.WAVE.reg = TC_WAVE_WAVEGEN_MFRQ;
	// allow interrupt to trigger on this event (compare channel 0)
	TC2->COUNT32.INTENSET.bit.MC0 = 1;
	TC2->COUNT32.INTENSET.bit.MC1 = 1; // don't know why, but had to turn this on to get the interrupts
	// set period
	while(TC2->COUNT32.SYNCBUSY.bit.CC0);
	TC2->COUNT32.CC[0].reg = 165;
	while(TC2->COUNT32.SYNCBUSY.bit.CC1);
	TC2->COUNT32.CC[1].reg = 165;
	// enable, sync for enable write
	TC2->COUNT32.CTRLA.bit.ENABLE = 1;
	while(TC2->COUNT32.SYNCBUSY.bit.ENABLE);
	TC3->COUNT32.CTRLA.bit.ENABLE = 1;
	while(TC3->COUNT32.SYNCBUSY.bit.ENABLE);
}

int main(void)
{
    /* Initialize the SAM system */
    SystemInit();
	SysTick_Config(2500000);
	
	// LED
	stlb = pin_new(&PORT->Group[0], 23);
	pin_output(&stlb);
	
	// interrupts
	__enable_irq();
	NVIC_EnableIRQ(SERCOM4_0_IRQn); //up1tx
	NVIC_EnableIRQ(SERCOM4_2_IRQn); //up1rx
	NVIC_EnableIRQ(SERCOM5_0_IRQn);
	NVIC_EnableIRQ(SERCOM5_2_IRQn);
	NVIC_EnableIRQ(TC2_IRQn);
	
	// Rinbuffers for UARTs
	rb_init(&up1_rbrx);
	rb_init(&up1_rbtx);
	rb_init(&up1_rbrx);
	rb_init(&up1_rbtx);
	
	// UARTS 
	// UP1:
	// SERCOM4 | RX: PB13 | TX: PB12
	// UP2:
	// SERCOM5 | RX: PB3 | TX: PB2
	up1 = uart_new(SERCOM4, &PORT->Group[1], &up1_rbrx, &up1_rbtx, 12, 13, HARDWARE_IS_APBD, HARDWARE_ON_PERIPHERAL_C); 
	MCLK->APBDMASK.reg |= MCLK_APBDMASK_SERCOM4;
	uart_init(&up1, 6, SERCOM4_GCLK_ID_CORE, 63018); // baud: 45402 for 921600, 63018 for 115200
	up2 = uart_new(SERCOM5, &PORT->Group[1], &up2_rbrx, &up2_rbtx, 3, 2, HARDWARE_IS_APBD, HARDWARE_ON_PERIPHERAL_D);
	MCLK->APBDMASK.reg |= MCLK_APBDMASK_SERCOM5;
	uart_init(&up2, 7, SERCOM5_GCLK_ID_CORE, 63018);
	
	// AS5147 SPI
	// SERCOM0
	// MISO: SER0, PA4
	// SCK: SER1, PA5
	// CSN: SER2, PA6
	// MOSI: SER3, PA7
	// AS5147 is CPOL = 0, CPHA = 1, 16 bit words (use 1 32 bit word, 2nd half will be reply: has to load)
	spi_encoder = spi_new(SERCOM0, &PORT->Group[0], 4, 7, 5, 6, HARDWARE_IS_APBA, HARDWARE_ON_PERIPHERAL_D);
	MCLK->APBAMASK.reg |= MCLK_APBAMASK_SERCOM0;
	spi_init(&spi_encoder, 8, SERCOM0_GCLK_ID_CORE, 6, 0, 2, 0, 1, 0, 0);
	
	// check all PWM lines
	// pins for enable, fault, pwmmode, etc
	pwmsetup_foc();
	
	// DRV Outputs
	// EN_GATE PA19: high to drive gates
	// M_PWM PA20: drive low for 6 channel pwm, high for 3 channel
	// DC_CAL PB16: drive high to calibrate
	// M_GAIN PB17: drive low, gain = 10, high, gain + 40
	en_gate = pin_new(&PORT->Group[0], 19);
	pin_output(&en_gate);
	pin_clear(&en_gate);
	m_pwm = pin_new(&PORT->Group[0], 20);
	pin_output(&m_pwm);
	pin_clear(&m_pwm);
	dc_cal = pin_new(&PORT->Group[1], 16);
	pin_output(&dc_cal);
	pin_clear(&dc_cal);
	m_gain = pin_new(&PORT->Group[1], 17);
	pin_output(&m_gain);
	pin_clear(&m_gain);
	
	// DRV Inputs
	// FAULT PA21: input, open drain when active, use pullup
	// OCTW PA22: input, open drain when active, use pullup
	fault = pin_new(&PORT->Group[0], 21);
	pin_input(&fault);
	pin_pullup(&fault);
	octw = pin_new(&PORT->Group[0], 22);
	pin_input(&octw);
	pin_pullup(&octw);
	
	// enable or don't
	//pin_clear(&en_gate);
	pin_set(&en_gate);
	
	// other modes
	pin_clear(&m_pwm);
	pin_clear(&m_gain);
	pin_clear(&dc_cal);
	
	bldc_init(&bldc, BLDC_RESOLUTION, BLDC_MODULO, BLDC_INIT_OFFSET, BLDC_REVERSE);
	bldc_command(&bldc, 90, 1);
	
	// startup timer for 5khz commutation loop
	comticker_init(64);
	
    while (1) 
    {
		if(!pin_read(&octw)){
			pin_clear(&stlb);
		}
		// timer should be running, doing bldc things
		// use these while() loops to handle network
    }
}

uint8_t loopcnt;

void SysTick_Handler(void){
	pin_set(&stlb);
	loopcnt ++;
	uart_sendchar_buffered(&up1, loopcnt);
	uint32_t reading;
	//encoder_read(&reading);
	reading = bldc.reading;
	uint8_t d1 = reading >> 8;
	uint8_t d2 = reading;
	uart_sendchar_buffered(&up1, d1);
	uart_sendchar_buffered(&up1, d2);
}

void SERCOM4_0_Handler(void){
	uart_txhandler(&up1);
}

void SERCOM4_2_Handler(void){
	uart_rxhandler(&up1);
}

void SERCOM5_0_Handler(void){
	uart_txhandler(&up2);
}

void SERCOM5_2_Handler(void){
	uart_rxhandler(&up2);
}

void TC2_Handler(void){
	TC2->COUNT32.INTFLAG.bit.MC0 = 1;
	TC2->COUNT32.INTFLAG.bit.MC1 = 1;
	bldc_update(&bldc);
}