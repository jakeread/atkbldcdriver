/*
 * encoder.c
 *
 * Created: 2/19/2018 11:11:57 AM
 *  Author: Jake
 */ 

#include "encoder.h"
#include "hardware.h"

void encoder_read(uint32_t *data_return){
	uint8_t data_tx[2] = {0xFF, 0xFF}; // read command: 1s, noop: 0s
	uint8_t data_rx[2];
	spi_txrxchars_polled(&spi_encoder, data_tx, 2, data_rx);
	data_tx[0] = 0x00;
	data_tx[1] = 0x00;
	spi_txrxchars_polled(&spi_encoder, data_tx, 2, data_rx);
	* data_return = (data_rx[0] & 0b00111111) << 8 | data_rx[1];
}