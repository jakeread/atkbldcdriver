/*
 * bldc_pfoc.c
 *
 * Created: 2/19/2018 10:40:54 AM
 *  Author: Jake
 */ 

#include "bldc_pfoc.h"
#include "encoder.h"
#include "sinelut.h"
#include "pwm_foc.h"
#include "hardware.h"

void bldc_init(bldc_t *bldc, uint32_t resolution, uint32_t modulo, uint32_t offset, uint32_t reverse){
	bldc->resolution = resolution;
	bldc->modulo = modulo;
	bldc->offset = offset;
	bldc->reverse = reverse;
	bldc->modmap = modulo / 1024;
}

// super simple throttle
void bldc_command(bldc_t *bldc, uint32_t scalar, uint32_t dir){
	if(scalar > BLDC_MAX_SCALAR){
		bldc->scalar = BLDC_MAX_SCALAR;
	} else {
		bldc->scalar = scalar;
	}
	bldc->dir = dir;
}

void bldc_update(bldc_t *bldc){
	// read encoder, where 0 - 2^14 is 0 - 2*PI rads mechanical phase
	encoder_read(&bldc->reading);		
	// reverse the reading if that flag is set
	if(bldc->reverse){
		bldc->reading = bldc->resolution - bldc->reading;
	}
	
	// electric position and offsetting 
	// one modulo is one full electric phase, is some division of full rotations
	// so, elecpos is 0 - modulo (uint) describing 0 - 2*PI rads of an electric phase
	bldc->elecpos = (bldc->reading + bldc->offset) % bldc->modulo;
	
	// now we map the phases from 0 - modulo (uint) -> 0 - 1024 (uint) 
	// this is the depth of our lookup table for sin()
	// we computed this value at init, as modulo / lut_depth;
	// we compute the modpos once
	bldc->modpos = bldc->elecpos / bldc->modmap;
	if(bldc->dir){
		bldc->phaseu = bldc->modpos;
		bldc->phasev = bldc->modpos + 341; // + 120* in lut phase
		bldc->phasew = bldc->modpos + 682; // + 240*
	} else {
		bldc->phaseu = bldc->modpos + 682;
		bldc->phasev = bldc->modpos + 341; // + 120* in lut phase
		bldc->phasew = bldc->modpos; // + 240*
	}

	
	// now we check for wrap arounds in the lut phase
	(bldc->phaseu > 1023) ? (bldc->phaseu -= 1023) : (0);
	(bldc->phasev > 1023) ? (bldc->phasev -= 1023) : (0);
	(bldc->phasew > 1023) ? (bldc->phasew -= 1023) : (0);
	
	// and set the pwm value to be the sin of it's current electric phase,
	// where the sinelut returns a value 0 - 255
	// midpoint (0 volts, equal time on / off) being 128
	// and multiply that value by a scalar
	bldc->pwmu = (sinelut[bldc->phaseu] * bldc->scalar) / 255;
	bldc->pwmv = sinelut[bldc->phasev] * bldc->scalar / 255;
	bldc->pwmw = sinelut[bldc->phasew] * bldc->scalar / 255;
	
	// finally, sending new pwm values
	// we assert the minimum value to be 5 and the max to be 250
	// this leaves some time for deadtime insertion (set with TCC_WEXCTRL_DTHS(1) in PWM setup)
	pwmupdate_foc(bldc->pwmu,bldc->pwmv,bldc->pwmw);
}