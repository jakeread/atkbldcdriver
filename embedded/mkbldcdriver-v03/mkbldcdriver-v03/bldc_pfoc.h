/*
 * bldc_pfoc.h
 *
 * Created: 2/19/2018 10:40:36 AM
 *  Author: Jake
 */ 


#ifndef BLDC_PFOC_H_
#define BLDC_PFOC_H_

#include "sam.h"

#define BLDC_MAX_SCALAR 255
#define BLDC_INIT_OFFSET 700
#define BLDC_RESOLUTION 16834
#define BLDC_MODULO 2340
#define BLDC_REVERSE 1

// struct to wrap commutation information
typedef struct{
	uint32_t resolution;
	uint32_t modulo;
	uint32_t offset;
	uint32_t reverse;
	uint32_t modmap;
	
	uint32_t scalar;
	uint32_t dir;
	
	uint32_t reading;
	uint32_t elecpos;
	uint32_t modpos;
	
	uint32_t phaseu, pwmu;
	uint32_t phasev, pwmv;
	uint32_t phasew, pwmw;
}bldc_t;

void bldc_init(bldc_t *bldc, uint32_t resolution, uint32_t modulo, uint32_t offset, uint32_t reverse);

void bldc_command(bldc_t *bldc, uint32_t scalar, uint32_t dir);

void bldc_update(bldc_t *bldc);

#endif /* BLDC_PFOC_H_ */