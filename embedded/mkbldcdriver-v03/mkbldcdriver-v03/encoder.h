/*
 * encoder.h
 *
 * Created: 2/19/2018 11:11:23 AM
 *  Author: Jake
 */ 


#ifndef ENCODER_H_
#define ENCODER_H_

#include "sam.h"

void encoder_read(uint32_t *data_return);

#endif /* ENCODER_H_ */