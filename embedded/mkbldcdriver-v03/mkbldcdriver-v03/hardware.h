/*
 * hardware.h
 *
 * Created: 2/17/2018 10:28:57 PM
 *  Author: Jake
 */ 


#ifndef HARDWARE_H_
#define HARDWARE_H_

#include "pin.h"
#include "ringbuffer.h"
#include "uartport.h"
#include "spiport.h"
#include "bldc_pfoc.h"
#include "pwm_foc.h"

pin_t stlb;

ringbuffer_t up1_rbrx;
ringbuffer_t up1_rbtx;
ringbuffer_t up2_rbrx;
ringbuffer_t up2_rbtx;

uartport_t up1;
uartport_t up2;

spiport_t spi_encoder;

pin_t en_gate;
pin_t m_pwm;
pin_t dc_cal;
pin_t m_gain;
pin_t fault;
pin_t octw;

bldc_t bldc;

pin_t testp;

#endif /* HARDWARE_H_ */