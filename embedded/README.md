# AutomataKit BLDC Driver Firmware Notes

Firmware for the board follows the [automatakit](https://gitlab.cba.mit.edu/jakeread/automatakit) architecture, and is available here for perusal.

Communication is handled asynchronously: bytes received on the UART are loaded into a ringbuffer, where they are later parsed in to packets. Parsing happens as oven as possible. 

Firmware for the xmega is under development, but you can see [firmware notes for the atsamd51 board](/embedded/atsamd51.md), and incidentally I've also done this with the [atsams70](/embedded/atsams70.md). Clearly, a few trips around the same yak. To shave. If you know what I mean.

## Fast and Dirty 6-Step Timer-Based Commutation Mode

To get spindles spinning with only a wee amount of c-programming time, I'm going to write a dirty/nasty/simple commutation scheme. 

## 3-Channel Hi / Lo Side PWM with AWeX

XMega has some built in functionality for motor / h-bridge drive, namely the AWeX (waveform extension) module that runs on top of the timer/counter system (which runs PWM). 

This is set-up in the code, and I've verified I have the write outputs writing to the right outputs (haha). Nice.

![pwm](images/pwms-awex-init-xmega.png)

## 6-Step Commutation

To do this quickly, I'm just going to yoke a timer to a blind 6-step commutation cycle. 

This worked out fairly well, I'll add some video ... I run one timer to commutate blindly, and another to accelerate to target speeds. The issue now is that the motor is easily stalled and as soon as it's out of phase, so I'll need some feedback. It might be neat to try this with back-emf sensing, but I'm privy to using the encoder, then we can set real speed settings and ignore eRPM.

In any case, I'd say it's enough to start trying to cut things, with some more current available. Hopefully I won't fry anything in the process: hand on the e-stop!

![video of olcomm](/video/atkbldc-openloop-com.mp4)

## Closing a loop on 6-Step Commutation

I know I can do this if I can manage to read my encoder successfully. I could also probably do it with the ADC, but I would want an oscilloscope to go down that path, and I don't have one here.

So, we use a logic analyzer to try reading this AMS5047

Got this running, feels pretty mediocre. I need to understand commutation a bit better, and do this with a fan setup etc. I perhaps just have not counted my poles properly, but I want to do this with an oscilloscope, and understand how we might search for those offsets etc. 

https://geekshavefeelings.com/posts/sensorless-brushless-cant-even

bulletproof commutation is encoder-based - low speed, at stall, with variable motors, etc, electromechanical sensing is always buggy. encoder perhaps not fast enough to commutate every cycle, so we need to write a filer / estimator that continues rolling the commutation cycle forward with only periodic input from the encoder - i.e. we contantly estimate speed, update that estimation every time we sample the encoder, commutate according to that estimation 

# Notes

 - read shane colton's pdf on sensorless field oriented control, and his park-transform free FOC solution: sounds cool. finish phase search algorithm.
 - FOC is basically finding the right phase offset to correct for inductance... with the inputs of current current vector, current position, we should be able so search for the optimum voltage vector. 2D.
 - two things: 
  - direct encoder -> commutation loop is too dumb, but close. just needs state estimated rotor position, can't read encoder often enough: use simple linear guess from speed or do kalman filter
  - to test current controller / sensing hardware, take the rotor off and write a very simple controller for the I term on one phase. duh.