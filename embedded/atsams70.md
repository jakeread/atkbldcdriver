# Depreciated Code for BLDC on the ATSAMS70

## Background

Here I'm using the chip I set up for my Networking project, it's an ATSAMS70 with four 'ports' for message passing, and a bells-and-whistles header that I'm thinking will be useful for doing-other-stuff (i.e. this motor controller is one of such things).

The board is documented [here](https://github.com/jakeread/tinynets/tree/master/circuit) and code for the switch (as well as useful bits for some peripheral setup is [here](https://github.com/jakeread/tinynets/tree/master/embedded)). 

## The plan

- do PWM Setup
- do motor turning around at low level, on some set frequency / duty
- do UART interface for Frequency / Duty
- do MODS UART

## PWM Setup

OK, I'm going to start by turning on the PWM peripherals I'll use to control my switches. A few initial spec's:

Channels 
 - 3 channels, 6 outputs: one hi / one low - per phase

Base Frequency
 - In selecting a base frequency for the PWM, a real engineer would look at the time constant of their motor. The coils of a motor are basically big inductors, so when voltage is switched 'hi' to them, it takes some time for the current in the coils to rise. The 'time constant' of a system is a pretty broad way to say on what-timescale-does-stuff-happen. So, in a small motor with relativley low inductance, we'll have a small time constant (say, of a few us[microseconds]). I'm not exactly sure what the units are like here, but I think this is like the time it takes for current to rise to 66% of what a full-out 'on' current level would be. This has something to do with impulse response. 
 - For the motors I'm using (smallish), I know (purely imperical) that a base frequency on the order of 10kHz is what I'm aiming at - a period of 100us. In microcontroller time, this is fairly slow! Additionally, this frequency generates an audible buzz (we hear the coils 'banging' on the mechanical system of the motor at this frequency) so being able to push this frequency outside of the audible range is awesome. 

Deatime Insertion 
 - Because the circuit is setup with a high and low side mosfet, and the fets don't switch instantaneously, if we were to switch the high off and low on instantaneously, there would be a brief moment when the high and low switches were both on, allowing current to 'shoot through' from VCC to GND, and this is bad. To get around this we do something called 'deadtime insertion' - just putting a few us of off-time on both channels, between switching. There is a register for this, and additionally the DRV8302 does this automatically - so I maybe don't need to worry about it off the bat, but if it's straightforward I'll do that.

ADC and PWM Synchronization
 - I'm reading current off of 'current shunts' that are 'below' my low-side mosfet.
 - Current only flows through these shunts when the low-side mosfets are on! Otherwise, they are open to nothing, nothing flows, no measurement etc.
 - I have to coordinate my ADC sampling with the *center* of the on-cycle of the low-side PWM. There are ways to do this with the peripherals, but it's cross-linking peripherals in the event system, so this will be a challenge.
 - I don't need to do current control *yet* so I'm going to save this struggle for later on!

I'm tempted to use the ASF modules for this.. 

However, I didn't! I got through this massive set of registers (holy shit there are PWM options for days) by carefully reading... guess.. the datasheet! Sweet deliverance of knowledge from the Si gods... In the ATSAMS70 Datasheet particularely, bouncing between 47.6.6 (PWM initialization sequence) and 47.6 (Functional Description) and 47.7 (Register Descriptions). I was able to work through it. I have *one* pwm line operational, now I'm looking at how to get the low-side of that signal operating as well... Here's a trace from my logic analyzer, digital and analog signals of the high side of this channel. I have the frequency correct - a good start.

![pwm-one-channel](https://gitlab.cba.mit.edu/jakeread/mkbldcdriver/raw/master/images/programming-pwm-alive.jpg)

So my second channel is not enabled because I've made an error in labelling pins! Here's the group prior to my noticing the error:

![pwm-one-channel](https://gitlab.cba.mit.edu/jakeread/mkbldcdriver/raw/master/images/pwm-pins-prior.jpg)

and corrected

![pwm-one-channel](https://gitlab.cba.mit.edu/jakeread/mkbldcdriver/raw/master/images/pwm-pins-shifted.jpg)

OK, so I'm going to have to keep track of this carefully, and I'm probably going to have to jumper wires between boards, rather than using these stacking headers. Welp. This is why we do incremental development.

With this confusion out of the way, here's a complete channel - high and low sides of my gates:

![pwm-one-channel](https://gitlab.cba.mit.edu/jakeread/mkbldcdriver/raw/master/images/programming-pwm-fullchannel.jpg)

And a detail on the deadtime insertion:

![pwm-one-channel](https://gitlab.cba.mit.edu/jakeread/mkbldcdriver/raw/master/images/programming-pwm-fullchannel-deadtime.jpg)

Here's the code I'm running:

```C

void setuppwm(void){
	PMC->PMC_PCER1 = 1 << 28; // turn on peripheral clock for PWM 1
	// these are pins for channels 1, 2, 3 in lo / hi pairs
	PIOD->PIO_PDR = PIO_PER_P2 | PIO_PER_P3 | PIO_PER_P4 | PIO_PER_P5 | PIO_PER_P6 | PIO_PER_P7; // disable PIO
	PIOD->PIO_ABCDSR[0] = PIO_PER_P2 | PIO_PER_P3 | PIO_PER_P4 | PIO_PER_P5 | PIO_PER_P6 | PIO_PER_P7; // enable peripheral B
	//PIOD->PIO_ABCDSR[1] // already these are 0s, we want that [01] is peripheral B
	
	// PREA Clock is Peripheral Clock / 64
	// PREA Clock supplies Clock A, Clock A is PREA / 24
	// Clock B is off
	PWM1->PWM_CLK = PWM_CLK_PREA(2) | PWM_CLK_DIVA(7) | PWM_CLK_DIVB_CLKB_POFF;
	// Channel 0 uses CLCKA, uses Center Aligned, has dead time, is waveform aligned (with others?)
	PWM1->PWM_CH_NUM[1].PWM_CMR  = PWM_CMR_CPRE_CLKA | PWM_CMR_CES | PWM_CMR_DTE | PWM_CMR_CALG;
	// Channel 0 uses this period
	// (2 * 2^PREA * DIVA * CPRD) / f_peripheralClock
	// so for 10kHz target frequency, we want 1 / 10000, 
	// have (2 * 64 * 12 * x )/ 150000000
	// ~ spreadsheets are your friends ~
	PWM1->PWM_CH_NUM[1].PWM_CPRD = PWM_CPRD_CPRD(255);
	// set duty cycle, initially, to low
	// this is between 0 and CPRD, so it looks like shooting for a big CPRD is (y)
	PWM1->PWM_CH_NUM[1].PWM_CDTY = PWM_CDTY_CDTY(1);
	// configure deadtime generator, between 0 and CPRD - CDTY
	PWM1->PWM_CH_NUM[1].PWM_DT = PWM_DT_DTH(2) | PWM_DT_DTL(2);
	// configure update mode
	// mode 1 is 'manual write of double buffer registers and automatic update of synchronous channels'
	// other modes are for manual write / manual  update and DMA mode
	// also add sync. channels - spec that 0, 1, 2 are ~ on the same wavelength, man ~
	PWM1->PWM_SCM = PWM_SCM_UPDM_MODE1 | PWM_SCM_SYNC0;
	
	// now, boot it up
	PWM1->PWM_ENA = PWM_ENA_CHID1; // just channel 0 for now
	
	// understand: compare? clock? update? duty cycle register? period register?
}

int main (void)
{
	board_init();
	sysclk_init();
	wdt_disable(WDT);
	
	setuppwm();
	
	PMC->PMC_PCER0 = 1 << ID_PIOA;
	
	PIOA->PIO_PER |= PIO_PER_P27;
	PIOA->PIO_OER = PIO_PER_P27;
	
	startupflash();
	
	while(1){
		// to update, must use duty update register, not just 'duty' 
		PWM1->PWM_CH_NUM[1].PWM_CDTYUPD = PWM_CDTY_CDTY(20);
		delay_ms(100);
		PWM1->PWM_CH_NUM[1].PWM_CDTYUPD = PWM_CDTY_CDTY(155);
		delay_ms(50);
	}
}
```

Now I'll set up three channels for my three half-bridges, write a function to do sinusoid open-loop commutation, and maybe even test that out on a motor! I'll also write a function to change the global duty cycle.

Here's my shorthand pin-mapping

![pwm-one-channel](https://gitlab.cba.mit.edu/jakeread/mkbldcdriver/raw/master/images/pwm-pins-mapping.jpg)

I want to push these values to my phases:

![pwm-one-channel](https://gitlab.cba.mit.edu/jakeread/mkbldcdriver/raw/master/images/diagram-three-phase.png)

Where the '0' point will be 1/2 of a full-on pwm phase (so, with my CPRD of 255, 127 - this is where the half bridge is switched hi half of the time and low the other half), -1 is a pwm duty of 0 (where the half bridge is switched low 100% of the time) and 1 is a value of 255 (where the half bridge is switched high 100% of the time). 

Here we go, a sinusoidal PWM:

![pwm-one-channel](https://gitlab.cba.mit.edu/jakeread/mkbldcdriver/raw/master/images/programming-pwm-sinusoidal.jpg)

I think I'm ready to check all of my other motor controller IO's - like, do I need to assert, also, an enable pin? I forget. Then I'll plug it in, and flip the switch, ho-ha! Yep - I just have on extra PIO to add, an Enable Pin. Set this up.

## UART

OK, I'm going to bring up the UART. I have a block of code for this from my networks project that I'll try bringing in.

This works, largely. I'm bringin the UART Baudrate way down to 9600... I'm getting characters returning on the line, now I need to setup the interrupt system to handle transmitted characters, and echo them.

Figuring out how the heck the interrupts work was a PITA. Kind of interesting, though - the Interrupt System is technically an ARM-Cortex domain thing, and so Atmel's datasheets barely touch on it... they say things like 'configure the interrupt controller before doing this' - but NOWHERE does it explain how to do that. Heck.

Here's my brief:
 - There are a set # of interrupts, they are enumerated in the software framework, and naming is pretty straightforward, for example, UART1 Interrupt Number is UART1_IRQn ...
 - There are a few globally defined NVIC setup functions, which take these IRQ Numbers as arguments. Their naming convention is also pretty straightforward
  - NVIC_DisableIRQ(IRQn) // disables!
  - NVIC_SetPriority(IRQn, uint8_t priority) // 0-255 priority, 0 being the highest
  - NVIC_EnableIRQ(IRQn) // enables!
 - When one of these interrupts fires, it directs to a pre-defined Handler. For example, the UART1 handler is UART1_Handler(){// code for handler} - Atmel Studio's autocomplete makes this work excellently.
 - Critically, most interrupts require that you clear some register, usually just by reading it.
  - in the UART example, I clear the event by reading the UART data in register. This will vary between peripherals, and it's not documented anywhere, so poke around!

Here's my Setup Code (runs in main())

```C
	NVIC_DisableIRQ(UART1_IRQn);
	NVIC_ClearPendingIRQ(UART1_IRQn);
	NVIC_SetPriority(UART1_IRQn, 9);
	NVIC_EnableIRQ(UART1_IRQn);
```

of course, I'm also setting up the UART peripheral... to see that, you can check out the code in this repo!

Here's the interrupt routine - properly, I would quickly push this data into a ringbuffer (to be implemented) and exit the routine - ISR's (interrupt service routines) should be very small, lest you get a barrage of them... buffering is bueno. Here, I'm just passing the received character back - this is an echo test, a good way to make sure everything is working as it should. It is, hurray!

```C
void UART1_Handler(){
	if(UART1->UART_SR & UART_SR_RXRDY){
		uint8_t data = UART1->UART_RHR; // read the incoming data
		while(!(UART1->UART_SR & UART_SR_TXRDY)); // hang-10 to make sure the UART is ready to send
		UART1->UART_THR = data;
	} else {
		//
	}
}
```

Note - the handler does not distinguish between an RXReady event and the other interrupts - a TXReady, for example, so in this routine I would have to also check which has fired and act accordintly...

Some ARM Documentation is [here](http://infocenter.arm.com/help/index.jsp) - navigate to Cortex-M7 Documentation, the latest revision, and look for NVIC.

From here, I brought up the hardware - a satisfying adventure. Go [here](https://gitlab.cba.mit.edu/jakeread/mkbldcdriver/tree/master/circuit) for that.

Then I worked (briefly) on implementing a control interface for this using mods. That should be [here](http://fab.cba.mit.edu/classes/863.17/CBA/people/jakeread/) in week 12.

## Closing Loops

Next up, I want to roll an encoder reading into the system, and run simple closed loop sinusoidal commutation. Then I'll try rolling a brief PID loop around that to find positions. Then I'll be satisfied with this spiral.

However, at the moment I'm having an issue where the chip is resetting, and things seem to be getting really hot. I see some smoke, I'm not sure where it's coming from, and it continues to work - so I'm confused. Sounds like I'm maybe smoking out the motor a little bit? Thinking I should maybe chill out to a 12v power supply - these motors have an Rm of only about .1 ohms, so if I accidentaly dump 240v into them for any appreciable time I'll see 240amps after whatever inductive time constant. That's deadly!

Actually my leading theory right now is that I'm just commutating so slowly the phase current is allowed to reach really big numbers, hence the smokey smokey. I'll implement the closed loop commutation to remedy this.

Or perhaps I'm driving the current sense amps way out of range and that's f'ing other things up?

OK actually I solved this earlier - something to do with the ATSAMS70 becoming unhappy and resetting when not enough power on the 3v3 line. *shrugman* - next rev on this board I'll address that. Power electronics: hard.

I'm also going to try booting up the FLIR Camera.

But, for now, SPI.

## SPI AS5047

OK. Long road ahead. Here's my list 
 - Pinouts / Pins / through the header, through the ATSAM
 - Peripheral Wakeup
 - Parity / Clock / etc setup
 - Reading Addresses / tiny abstraction for the AS5047 particularly.
  - parity bit checking important b/c previous experience with this encoder shitting the bed when presented with big EMF environment.
 - fin section

I'm using the ATSAM's SPI Peripheral. I am *really* swimming in the datasheet at the moment. There's a lot to configure!

Here we go, some code. 

```C
void setupspi(void){
	// using SPI0, PID 21
	// MISO, PD20, Peripheral B
	// MOSI, PD21, Peripheral B
	// SPCK, PD22, Peripheral B
	// NPCS, PA31, Peripheral A
	
	PMC->PMC_PCER0 = 1 << ID_SPI0;
	
	PIOD->PIO_PDR |= PIO_PER_P20 | PIO_PER_P21 | PIO_PER_P22;
	PIOA->PIO_PDR |= PIO_PER_P31;
	
	SPI0->SPI_CR |= SPI_CR_SPIEN;
	
	SPI0->SPI_MR |= SPI_MR_MSTR | SPI_MR_PCS(1);
	
	SPI0->SPI_CSR[1] |= SPI_CSR_BITS_16_BIT | SPI_CSR_NCPHA | SPI_CSR_SCBR(200);
}
```

Turned out to be kind of boring.

So SPI is *really fast* - i.e. I can run this at ~5MBPS no problem, and I'm only doing a 32 bit transfer each time. What's more, SPI is like synchronous duplex, as in during the same time shift out 32 bits, 32 bits come in. This is the major advantage of a clocked / master:slave relationship. This means that for a total of 64 bit transferred, we take only 6.4us. Holy heck. I'm not even going to bother writing non-blocking code.

```C
void readencoder(uint16_t *data){
			
	while(!(SPI0->SPI_SR & SPI_SR_TXEMPTY)); // wait 4 ready
	SPI0->SPI_TDR = SPI_TDR_TD(0xFFFF);// | SPI_TDR_PCS(1);
	while(!(SPI0->SPI_SR & SPI_SR_TXEMPTY)); // wait 4 ready
	SPI0->SPI_TDR = SPI_TDR_TD(0x0000);// | SPI_TDR_PCS(1);
	*data = SPI0->SPI_RDR & SPI_RDR_RD_Msk;
	
	//uint16_t pard = data >> 15;
	*data = *data << 2; // shift parity and error bit out
	*data /= 4;
}
```

OK - I've got this set up properly. Now I'll try plugging the encoder in an reading some values!

Great, and with a shift in some setting (I was wrong) I'm reading SPI values nicely. Next test is to see if these values get all messed up when I turn the motor on (update: nope, yay), introducing lots of EMF and noise into the system... Then I'll write some open-loop commutation using the encoder values (update: done). Then I'll think about writing a simple PID position loop. If I get through all of that tonight, I'll go to bed (haha).

OK - I got closed-loop commutation working. This is where I take a position reading on the rotor and use that to advance the sinusoidal voltage I am sending to the phases such that *roughly* I get a magnetic field that is 90deg out of phase from the rotor's magnetic field. This is the max torque scenario.

```C

		uint16_t encoder;
		
		readencoder(&encoder);
		
		if(reverse){
			encoder = resolution - encoder;
		}
		uint16_t elecpos = (encoder + offset) % modulo; // 0 -> 2*PI in rads
		
		// to update, must use duty update register, not just 'duty' 
		if(dir){
			phaseu = elecpos/(modulo / 1024);
			phasev = elecpos/(modulo / 1024) + 341;
			phasew = elecpos/(modulo / 1024) + 682;
		} else {
			phaseu = elecpos/(modulo / 1024) + 682;
			phasev = elecpos/(modulo / 1024) + 341;
			phasew = elecpos/(modulo / 1024);
		}
		
		if (phaseu > 1023){
			phaseu -= 1023;
		}
		if(phasev > 1023){
			phasev -= 1023;
		}
		if(phasew > 1023){
			phasew -= 1023;
		}
```

Pretty slick, the key is adjusting the offset.

[Here](https://gitlab.cba.mit.edu/jakeread/mkbldcdriver/raw/master/video/clcomm.m4v) is a video of the motor turning *nicely* using closed-loop commutation. Not sure why this is upside-down, but that doesn't seem important. Also - you'll hear the 10KHz pwm buzz - this is something I plan on eliminating (it's simple - I just push the PWM frequency out of the audible range) but I have left it just in the audible range so that I can be sure when the gate-drivers are on (I don't want to fry anything!).

And that's right. Those are some BFC's.

# The EMI Problem

Below, a log of me writing notes when I find this intermittent, horrible, reset condition. 

Also: a note, I'm having odd reset events occasionally. Related to the earlier issue w/ usb power. I think I need to revisit the 'bfpsu' issue - wherein I want a 24V PSU that will deliver nice consistent power ~ 65 amps (that's about as much juice as is possible to pull out of a wall socket). I may need to build this, as these don't seem readily available. My other thought is to find a big switching PSU, and hook up a *giant* capacitor to the output to smooth it out.

OMG it's the SWITCH is being super intermittent. Don't use a switch rated for 7.5 amps past 7.5 amps. Heck!

OK - like 8th loop on this. I fixed that wiring and was still having a reset issue. Back at it now, added extra capacitance to the 5V output, and a diode that I had previously omitted soldering on. Seems better, but I make no promises to myself.

Another update on this - I've shortened the motor lead wires and this seems to have killed the problem. Again, no expectations - I've been wrong a few times. But I suspect EMI was the issue, and shorter lines (smaller antennas) could be the solution! Hurray, maybe :| . 

![emi](https://gitlab.cba.mit.edu/jakeread/mkbldcdriver/raw/master/images/emi.jpg)

So this shortened motor wires did seem to really kill the problem. Fin.