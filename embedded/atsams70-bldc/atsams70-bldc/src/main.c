/**
 * \file
 *
 * \brief Empty user application template
 *
 */

/**
 * \mainpage User Application template doxygen documentation
 *
 * \par Empty user application template
 *
 * Bare minimum empty user application template
 *
 * \par Content
 *
 * -# Include the ASF header files (through asf.h)
 * -# "Insert system clock initialization code here" comment
 * -# Minimal main function that starts with a call to board_init()
 * -# "Insert application code here" comment
 *
 */

/*
 * Include header files for all drivers that have been imported from
 * Atmel Software Framework (ASF).
 */
/*
 * Support and FAQ: visit <a href="http://www.atmel.com/design-support/">Atmel Support</a>
 */
#include <asf.h>
#include "pin.h"
#include "tinyport.h"
#include "sinelut.h"

// status lights
pin_t stlr; // PD13
pin_t stlg; // PD14
pin_t stlb; // PA27

// on and cal
pin_t enablepin;
pin_t dccalpin;

// uart port
tinyport_t tp2;

void startupflash(void){
	delay_ms(125);
	pin_clear(&stlb);
	pin_clear(&stlr);
	delay_ms(125);
	pin_set(&stlb);
	pin_set(&stlr);
	delay_ms(50);
	pin_clear(&stlb);
	pin_clear(&stlr);
	delay_ms(50);
	pin_set(&stlb);
	pin_set(&stlr);
	delay_ms(50);
	pin_clear(&stlb);
	pin_clear(&stlr);
	delay_ms(50);
	pin_set(&stlb);
	pin_set(&stlr);
}

void uarthello(void){
	tp_putchar(&tp2, 'h');
	delay_ms(1);
	tp_putchar(&tp2, 'e');
	delay_ms(1);
	tp_putchar(&tp2, 'l');
	delay_ms(1);
	tp_putchar(&tp2, 'l');
	delay_ms(1);
	tp_putchar(&tp2, 'o');
	delay_ms(1);
	tp_putchar(&tp2, 0x0A);
	delay_ms(1);
}

void setuppio(void){
	PMC->PMC_PCER0 = 1 << ID_PIOA;
	PMC->PMC_PCER0 = 1 << ID_PIOD;
	
	stlr = pin_new(PIOD, PIO_PER_P13);
	pin_output(&stlr);
	pin_set(&stlr);
	stlg = pin_new(PIOD, PIO_PER_P14);
	pin_output(&stlg);
	pin_set(&stlg);
	stlb = pin_new(PIOA, PIO_PER_P27);
	pin_output(&stlb);
	pin_set(&stlb);
	
	enablepin = pin_new(PIOA, PIO_PER_P23);
	pin_output(&enablepin);
	dccalpin = pin_new(PIOA, PIO_PER_P16);
	pin_output(&dccalpin);
}

void gates(int onoff){
	if(onoff){
		pin_set(&enablepin);
		pin_clear(&stlr);
	} else {
		pin_clear(&enablepin);
		pin_set(&stlr);
	}
}

void abcdselects(void){
	PIOD->PIO_ABCDSR[0] =	PIO_PER_P2 | PIO_PER_P3 | PIO_PER_P4 | PIO_PER_P5 | PIO_PER_P6 | PIO_PER_P7 // peripheral b for pwm
							| PIO_PER_P20 | PIO_PER_P21 | PIO_PER_P22; // peripheral b for spi
							
	// PIOA abcdsr set already to 0 for A peripheral selects for SPI NPCS1.
}

void setuppwm(void){
	// All MK-PWMs are Peripheral B
	// PWM0 is PID 31 position 31 in PCER0, PWM1 is PID 60, position 28 in PCER1
	// PWM1L0: PD0
	// PWM1H0: PD1
	// PWM1L1: PD2
	// PWM1H1: PD3
	// PWM1L2: PD4
	// PWM1H2: PD5
	
	PMC->PMC_PCER1 = 1 << 28; // turn on peripheral clock for PWM 1
	// these are pins for channels 1, 2, 3 in lo / hi pairs
	PIOD->PIO_PDR = PIO_PER_P2 | PIO_PER_P3 | PIO_PER_P4 | PIO_PER_P5 | PIO_PER_P6 | PIO_PER_P7; // disable PIO
	//PIOD->PIO_ABCDSR[1] // already these are 0s, we want that [01] is peripheral B
	
	// PREA Clock is Peripheral Clock / 64
	// PREA Clock supplies Clock A, Clock A is PREA / 24
	// Clock B is off
	PWM1->PWM_CLK = PWM_CLK_PREA(2) | PWM_CLK_DIVA(7) | PWM_CLK_DIVB_CLKB_POFF;
	// Channel 0 uses CLCKA, uses Center Aligned, has dead time, is waveform aligned (with others?)
	PWM1->PWM_CH_NUM[1].PWM_CMR  = PWM_CMR_CPRE_CLKA | PWM_CMR_CES | PWM_CMR_DTE | PWM_CMR_CALG | PWM_CMR_CPOL;
	PWM1->PWM_CH_NUM[2].PWM_CMR  = PWM_CMR_CPRE_CLKA | PWM_CMR_CES | PWM_CMR_DTE | PWM_CMR_CALG | PWM_CMR_CPOL;
	PWM1->PWM_CH_NUM[3].PWM_CMR  = PWM_CMR_CPRE_CLKA | PWM_CMR_CES | PWM_CMR_DTE | PWM_CMR_CALG | PWM_CMR_CPOL;
	// Channel 0 uses this period
	// (2 * 2^PREA * DIVA * CPRD) / f_peripheralClock
	// so for 10kHz target frequency, we want 1 / 10000, 
	// have (2 * 64 * 12 * x )/ 150000000
	// ~ spreadsheets are your friends ~
	PWM1->PWM_CH_NUM[1].PWM_CPRD = PWM_CPRD_CPRD(255);
	PWM1->PWM_CH_NUM[2].PWM_CPRD = PWM_CPRD_CPRD(255);
	PWM1->PWM_CH_NUM[3].PWM_CPRD = PWM_CPRD_CPRD(255);
	// set duty cycle, initially, to low
	// this is between 0 and CPRD, so it looks like shooting for a big CPRD is (y)
	PWM1->PWM_CH_NUM[1].PWM_CDTY = PWM_CDTY_CDTY(1);
	PWM1->PWM_CH_NUM[2].PWM_CDTY = PWM_CDTY_CDTY(1);
	PWM1->PWM_CH_NUM[3].PWM_CDTY = PWM_CDTY_CDTY(1);
	// configure deadtime generator, between 0 and CPRD - CDTY
	PWM1->PWM_CH_NUM[1].PWM_DT = PWM_DT_DTH(2) | PWM_DT_DTL(2);
	PWM1->PWM_CH_NUM[2].PWM_DT = PWM_DT_DTH(2) | PWM_DT_DTL(2);
	PWM1->PWM_CH_NUM[3].PWM_DT = PWM_DT_DTH(2) | PWM_DT_DTL(2);
	// configure update mode
	// mode 1 is 'manual write of double buffer registers and automatic update of synchronous channels'
	// other modes are for manual write / manual  update and DMA mode
	// also add sync. channels - spec that 0, 1, 2 are ~ on the same wavelength, man ~
	PWM1->PWM_SCM = PWM_SCM_UPDM_MODE1;// | PWM_SCM_SYNC1 | PWM_SCM_SYNC2 | PWM_SCM_SYNC3;
	
	// now, boot it up
	PWM1->PWM_ENA = PWM_ENA_CHID1 | PWM_ENA_CHID2 | PWM_ENA_CHID3; // just channel 0 for now
}

void setupuart(void){
	// port 2
	// peripheral a
	// uart0
	// rx: pa9
	// tx: pa10
	
	PMC->PMC_PCER0 = 1 << ID_PIOA;
	PMC->PMC_PCER0 = 1 << ID_UART0; // UART1
	
	//tp2, uart0, peripheral a
	//PIOA->PIO_ABCDSR[0] = PIO_PER_P5;
	//PIOA->PIO_ABCDSR[0] = PIO_PER_P4;
	//PIOA->PIO_ABCDSR[1] |= PIO_PER_P5;
	//PIOA->PIO_ABCDSR[1] |= PIO_PER_P4;
}

void setupspi(void){
	// using SPI0, PID 21
	// MISO, PD20, Peripheral B
	// MOSI, PD21, Peripheral B
	// SPCK, PD22, Peripheral B
	// NPCS, PA31, Peripheral A
	
	PMC->PMC_PCER0 = 1 << ID_SPI0;
	
	PIOD->PIO_PDR |= PIO_PER_P20 | PIO_PER_P21 | PIO_PER_P22;
	PIOA->PIO_PDR |= PIO_PER_P31;
	
	SPI0->SPI_CR |= SPI_CR_SPIEN;
	
	SPI0->SPI_MR |= SPI_MR_MSTR | SPI_MR_PCS(1);
	
	SPI0->SPI_CSR[1] |= SPI_CSR_BITS_16_BIT | SPI_CSR_NCPHA | SPI_CSR_SCBR(200);
}

void setupinterrupts(void){
	NVIC_DisableIRQ(UART0_IRQn);
	NVIC_ClearPendingIRQ(UART0_IRQn);
	NVIC_SetPriority(UART0_IRQn, 9);
	NVIC_EnableIRQ(UART0_IRQn);
}

uint8_t d1;
uint8_t d2;
int32_t dstate = 0;
uint32_t mready = 0;

void UART0_Handler(){
	if(UART0->UART_SR & UART_SR_RXRDY){
		if(dstate){
			d2 = UART0->UART_RHR;
			dstate = 0;
			mready = 1;
		} else {
			d1 = UART0->UART_RHR;
			dstate = 1;
		}
	} else {
		// bail
	}
}

void readencoder(uint16_t *data){
			
	while(!(SPI0->SPI_SR & SPI_SR_TXEMPTY)); // wait 4 ready
	SPI0->SPI_TDR = SPI_TDR_TD(0xFFFF);// | SPI_TDR_PCS(1);
	while(!(SPI0->SPI_SR & SPI_SR_TXEMPTY)); // wait 4 ready
	SPI0->SPI_TDR = SPI_TDR_TD(0x0000);// | SPI_TDR_PCS(1);
	*data = SPI0->SPI_RDR & SPI_RDR_RD_Msk;
	
	//uint16_t pard = data >> 15;
	*data = *data << 2; // shift parity and error bit out
	*data /= 4;
}

#define IS_ENCODER_COM 0
#define IS_OPENLOOP_COM 1

int main (void)
{
	/* Insert system clock initialization code here (sysclk_init()). */

	board_init();
	sysclk_init();
	wdt_disable(WDT);
	abcdselects();
	setuppwm();
	setuppio();
	setupuart();
	tp2 = tinyport_new(UART0, PIOA, PERIPHERAL_A, PIO_PER_P9, PIO_PER_P10);
	tp_init(&tp2);
	setupinterrupts();
	setupspi();
	
	startupflash();
	uarthello();
	
	uint8_t maxscalar = 64;

	uint8_t scalar = 45; // 0 -> 255 (these will be chars from serial)
	uint8_t period = 20; // us between ++ in phase
	
	uint32_t phaseu = 0; // 0 -> 1023
	uint32_t phasev = 341; // advanced 120 degrees
	uint32_t phasew = 682; // advanced 240 degrees
	
	gates(1); // boots the gates, or don't
	
	uint32_t putCounter = 0;
	
	uint16_t offset = 100;
	uint16_t reverse = 1; // encoder
	uint16_t dir = 1; // commutation
	uint16_t resolution = 16384;
	uint16_t modulo = 2340; // AS5047_RESOLUTION / MOTOR_POLEPAIRS -> MT4108 = 1489 || MT5208 = 2340
	
	while(1){
		// begin message handling
		/* Command Table
	
		0 <0> | <1> - run close or open gates
		1 <val> - open loop terq
		2 <val> - period to run open loop on
		3 <0> | - read encoder, or cal, 
	
		*/
			
		if(mready){
			switch(d1){
				
				case 0:
				if(d2 == 1){
					gates(1);
					} else {
					gates(0);
				}
				break;

				case 1:
				scalar = min(d2, maxscalar);
				break;
				
				case 2:
				period = max(d2, 5);
				break;
				
				case 3:
				if (d2 == 0){
					reverse = 0;
					dir = 0;
				} else {
					reverse = 1;
					dir = 1;
				}
				break;
				
				default:
				gates(0); // bail, shutdown gates
				tp_putchar(&tp2, 'X');
				break;
			}
			tp_putchar(&tp2, d1);
			tp_putchar(&tp2, d2);
			mready = 0;
		} // end message handler
		
		
#if IS_ENCODER_COM
		uint16_t encoder;
		
		readencoder(&encoder);
		
		if(reverse){
			encoder = resolution - encoder;
		}
		uint16_t elecpos = (encoder + offset) % modulo; // 0 -> 2*PI in rads
		
		// to update, must use duty update register, not just 'duty' 
		if(dir){
			phaseu = elecpos/(modulo / 1024);
			phasev = elecpos/(modulo / 1024) + 341;
			phasew = elecpos/(modulo / 1024) + 682;
		} else {
			phaseu = elecpos/(modulo / 1024) + 682;
			phasev = elecpos/(modulo / 1024) + 341;
			phasew = elecpos/(modulo / 1024);
		}
		
		if (phaseu > 1023){
			phaseu -= 1023;
		}
		if(phasev > 1023){
			phasev -= 1023;
		}
		if(phasew > 1023){
			phasew -= 1023;
		}
		
		/*
		putCounter ++;
		if(!(putCounter % 100)){
			tp_putchar(&tp2, phasew / (1024 / 255));
		}
		*/
#endif

#if IS_OPENLOOP_COM

		phaseu ++;
		phasev ++;
		phasew ++;

		if (phaseu > 1023){
			phaseu = 0;
		}
		if(phasev > 1023){
			phasev = 0;
		}
		if(phasew > 1023){
			phasew = 0;
		}

#endif

		PWM1->PWM_CH_NUM[1].PWM_CDTYUPD = PWM_CDTY_CDTY(sinelut[phaseu] * scalar / 255);
		PWM1->PWM_CH_NUM[2].PWM_CDTYUPD = PWM_CDTY_CDTY(sinelut[phasev] * scalar / 255);
		PWM1->PWM_CH_NUM[3].PWM_CDTYUPD = PWM_CDTY_CDTY(sinelut[phasew] * scalar / 255);
		
		delay_us(500); // nets about 4khz loop, granting 2 pwm cycles per update. lots!
		
		if(pin_get_state(&stlb)){ // ? 
			pin_clear(&stlb);
		} else {
			pin_set(&stlb);
		}
	}
}
