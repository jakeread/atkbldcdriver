/*
 * bldc.h
 *
 * Created: 7/12/2018 1:52:26 PM
 *  Author: Jake
 */ 


#ifndef BLDC_H_
#define BLDC_H_

#include "avr/io.h"

typedef struct{
	uint8_t comState;
	uint8_t comDir;
	uint16_t comDuty;
	
	int32_t currentSpeed;
	int32_t targetSpeed;
	uint16_t comPeriod;
}bldc_t;

void bldc_init(bldc_t *bldc);

void bldc_shutdown(bldc_t *bldc);

void bldc_setDuty(bldc_t *bldc, uint32_t duty);

void bldc_setTargetSpeed(bldc_t *bldc, int32_t speed);

void bldc_setSpeed(bldc_t *bldc, int32_t speed);

void bldc_start(bldc_t *bldc, int32_t speed, uint32_t duty);

void bldc_newSpeed(bldc_t *bldc, int32_t speed, uint32_t duty);


#endif /* BLDC_H_ */