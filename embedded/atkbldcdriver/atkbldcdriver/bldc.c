/*
 * bldc.c
 *
 * Created: 7/12/2018 1:52:34 PM
 *  Author: Jake
 */ 

#include "bldc.h"
#include "hardware.h"

void bldc_init(bldc_t *bldc){
	bldc->comState = 0;
	bldc->comDir = 1;
	bldc->comDuty = 0;
	
	bldc->targetSpeed = 0;
	bldc->currentSpeed = 0;
}

void bldc_shutdown(bldc_t *bldc){
	pin_clear(&drvEnPin);
	bldc->comState = 0;
	bldc->comDir = 1;
	bldc->comDuty = 0;
}

void bldc_setDuty(bldc_t *bldc, uint32_t duty){
	// blind pwm duty 0-512
	// hard stop at 256 to avoid breaking things 
	(duty > 512) ? duty = 512 : (0);
	bldc->comDuty = (uint16_t) duty;
}

void bldc_setTargetSpeed(bldc_t *bldc, int32_t speed){
	// assert limits
	if(speed > 20000){
		speed = 20000;
	} else if (speed < -20000){
		speed = -20000;
	}
	
	bldc->targetSpeed = speed;
}

void bldc_setSpeed(bldc_t *bldc, int32_t speed){
	// speed in eRPM
	// assert max
	uint32_t sAbs = abs(speed);
	(sAbs > 20000) ? sAbs = 20000 : (0);
	// check dir
	if(speed == 0){
		bldc_shutdown(bldc);
	} else if (speed > 0){
		bldc->comDir = 1;
	} else {
		bldc->comDir = 0;
	}
	
	// base time, and we want 6 steps / rev, and rpm-rps
	bldc->comPeriod = COMTICKER_TICKS_SECOND / (sAbs / 10); 
	
	// set a new timer period
	uint8_t ctPerBufL = (uint8_t) bldc->comPeriod;
	uint8_t ctPerBufH = (uint8_t) (bldc->comPeriod >> 8);
	TCD0.PERBUFL = ctPerBufL;
	TCD0.PERBUFH = ctPerBufH;
}

void bldc_newSpeed(bldc_t *bldc, int32_t speed, uint32_t duty){
	bldc_setDuty(bldc, duty);
	bldc_setTargetSpeed(bldc, speed);
}