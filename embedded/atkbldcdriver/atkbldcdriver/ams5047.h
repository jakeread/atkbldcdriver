/*
 * ams5047.h
 *
 * Created: 2/7/2018 10:17:49 PM
 *  Author: Jake
 */ 


#ifndef AMS5047_H_
#define AMS5047_H_

#include "spiport.h"
#include "pin.h"

// TODO: adding updates (microstep, current)
// in that, calculating current
// TODO: reading stallguard, understanding if is already doing closed loop?

typedef struct{
	spiport_t *spi;
}ams5047_t;

uint16_t ams5047_readOp;
uint16_t ams5047_noOp;
uint8_t ams5047_readBytes[2];
uint8_t ams5047_noOpBytes[2];

uint8_t ams5047_resultBytes[2];

void ams5047_init(ams5047_t *ams, spiport_t *spi);

void ams5047_read(ams5047_t *ams, uint16_t *result);

#endif /* AMS5047_H_ */