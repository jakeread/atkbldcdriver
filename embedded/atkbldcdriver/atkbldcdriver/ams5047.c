/*
 * ams5047x.c
 *
 * Created: 2/7/2018 10:17:39 PM
 *  Author: Jake
 */ 

#include "ams5047.h"

void ams5047_init(ams5047_t *ams, spiport_t *spi){
	ams->spi = spi;
	ams5047_readOp = (1 << 15) | (1 << 14) | 0x3FFF; // parity bit, 1 for read / notwrite, 1s for read-op address
	ams5047_readBytes[0] = ams5047_readOp >> 8;
	ams5047_readBytes[1] = (uint8_t)ams5047_readOp;
	ams5047_noOp = (1 << 15);  // parity bit, 0's for no-op
	ams5047_noOpBytes[0] = ams5047_noOp >> 8;
	ams5047_noOpBytes[1] = (uint8_t)ams5047_noOp;
}

void ams5047_read(ams5047_t *ams, uint16_t *result){
	// at baud, takes ~ 20us (bauda = 12, baudb = 0) so max 50kHz
	spi_txchars_polled(ams->spi, ams5047_readBytes, 2);
	// issue noop, read rx'd on this tx (one step back)
	spi_txrxchars_polled(ams->spi, ams5047_readBytes, ams5047_resultBytes, 2);
	*result = ((ams5047_resultBytes[0] & 0b00111111) << 8) | ams5047_resultBytes[1];
}