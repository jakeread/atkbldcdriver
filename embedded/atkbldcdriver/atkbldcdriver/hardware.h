/*
 * hardware.h
 *
 * Created: 6/18/2018 12:18:05 PM
 *  Author: Jake
 */ 


#ifndef HARDWARE_H_
#define HARDWARE_H_

#include "pin.h"
#include "ringbuffer.h"
#include "uartport.h"
#include "spiport.h"
#include "atkport.h"
#include "atkhandler.h"
#include "ams5047.h"
#include "bldc.h"

// results in 1MBaud
#define SYSTEM_BAUDA 3
#define SYSTEM_BAUDB 0
#define SYSTEM_NUM_UPS 1

// ticker bases

#define COMTICKER_TICKS_SECOND 750000

pin_t stlclk;
pin_t stlerr;

// UP0

ringbuffer_t up0rbrx;
ringbuffer_t up0rbtx;

uartport_t up0;

pin_t up0rxled;
pin_t up0txled;

atkport_t atkp0;

// UPS

uartport_t *ups[SYSTEM_NUM_UPS];

// Hardware
// SPI

spiport_t spiEncoder;
pin_t spiEncCsPin;

ams5047_t ams5047;

// DRV8302 Business

pin_t drvEnPin;
pin_t drvModePwm;
pin_t drvModeGain;
pin_t drvDcCal;

pin_t drvFault;
pin_t drvOCTW;

// pwm pins

pin_t lo1;
pin_t hi1;
pin_t lo2;
pin_t hi2;
pin_t lo3;
pin_t hi3;

// controller functions 

bldc_t bldc;

uint8_t comState;
uint16_t comDuty;

// bldc_t bldc;

pin_t tstpin1;
pin_t tstpin2;

#endif /* HARDWARE_H_ */