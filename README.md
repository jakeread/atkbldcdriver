# AutomataKit BLDC Driver

![bldcdriver](images/atkbldc.jpg)

## What

The MMKBLDC Project is my attempt at building a decent controller for Brushless Motors. BLDCs are excellent sources of mechatronic actuation: they're [power dense](https://jakeread.pages.cba.mit.edu/actuators/) and serve the motion control justice present in most state of the art robotics. However, controlling them is non-trivial, and requires the combination of some EE expertise, some control expertise, and some hardware know-how.

![board](/images/routed.png)

![schematic](/images/schematic.png)

## Why

This project is largely a follow-on to [my Teensy-Powered Brushless Motor Controller](https://github.com/jakeread/tesc), and with this new work, may the TESC project RIP. A moment of silence.

TESC, April 2016 - August 2016  
*As the world turns,  
so did those motors.  
Once around is never enough*  

## How

The driver uses an ATxmega microcontroller to drive phase voltages through a gate driver, through half-bridges, through motor windings. An encoder reads motor position back to the microcontrolelr, and shunt resistors measure current flowing through the motor coils. With this information, we can use some maths to determine where the motor's magnetic field is, and control that to drive position and torque outputs.

## Development Notes

See [circuit chatter](/circuit), [firmware chatter](/embedded)

At the moment, it's doing open loop 6-step commutation.

![video of olcomm](/video/atkbldc-openloop-com.mp4)

## Learning About Brushless Motors and their Control

[See the (ongoing) notes I put together to explain the wizardry of BLDC driving under / eductation.](/education)

# Viable Commands

*none yet - firmware in development*

### Test

Keycode: **127**
 - to test networking, this will reply with a payload packet containing ```127, 12, 24, 48``` and will toggle an LED on the board

### Reset

Keycode: **128**
 - issues a software microcontroller reset
 - if the microcontroller is already hung up, this will not work

### eRPM Target

Keycode: **141**
Argments: Electrical RPM, int32_t, in electric phase periods / minute
Returns: Ack, uint8_t, 141
 - sets motor spinning at the set electrical rpm.

### Read Current RPM

Keycode: **142**
Arguments: none
Returns: Electrical RPM at time of read, in electric phase periods / minute

# Reproducing This Work

All automatakit works are open source, and while we cannot sell you boards, if you have an interesting application, get in touch to ask about collaborating.

To reproduce boards and load code, see the document ['Reproducing Automatakit Work'](https://gitlab.cba.mit.edu/jakeread/automatakit/reproducing)