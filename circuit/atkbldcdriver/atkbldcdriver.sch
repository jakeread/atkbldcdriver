<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.2.2">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="yes" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="yes" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="yes" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="yes" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="yes" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="yes" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="yes" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="yes" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="yes" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="yes" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="yes" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="yes" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="yes" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="yes" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="yes" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="yes" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="yes" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="yes" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="yes" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="yes" active="no"/>
<layer number="20" name="Dimension" color="24" fill="1" visible="yes" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="yes" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="yes" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="yes" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="yes" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="yes" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="yes" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="yes" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="yes" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="yes" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="yes" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="yes" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="yes" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="yes" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="yes" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="yes" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="yes" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="yes" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="yes" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="yes" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="yes" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="yes" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="yes" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="yes" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="no" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="no" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="no" active="yes"/>
<layer number="104" name="Name" color="7" fill="1" visible="no" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="no" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="no" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="no" active="yes"/>
<layer number="108" name="tplace-old" color="10" fill="1" visible="no" active="yes"/>
<layer number="109" name="ref-old" color="11" fill="1" visible="no" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="no" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="no" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="113" name="IDFDebug" color="7" fill="1" visible="no" active="yes"/>
<layer number="114" name="Badge_Outline" color="7" fill="1" visible="yes" active="yes"/>
<layer number="115" name="ReferenceISLANDS" color="7" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="no" active="yes"/>
<layer number="117" name="BACKMAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="no" active="yes"/>
<layer number="119" name="KAP_TEKEN" color="7" fill="1" visible="yes" active="yes"/>
<layer number="120" name="KAP_MAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="no" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="no" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="no" active="yes"/>
<layer number="130" name="SMDSTROOK" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="133" name="bottom_silk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="no" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="no" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="no" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="no" active="yes"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="no" active="yes"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="no" active="yes"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="no" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="no" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="no" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="no" active="yes"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="no" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="no" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="no" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="no" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="no" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="no" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="231" name="231bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="232" name="Eagle3D_PG2" color="14" fill="2" visible="no" active="yes"/>
<layer number="233" name="Eagle3D_PG3" color="14" fill="4" visible="no" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="no" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="no" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="no" active="yes"/>
<layer number="255" name="routoute" color="7" fill="1" visible="no" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="supply1" urn="urn:adsk.eagle:library:371">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="+3V3" urn="urn:adsk.eagle:symbol:26950/1" library_version="1">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+3V3" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="GND" urn="urn:adsk.eagle:symbol:26925/1" library_version="1">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="VCC" urn="urn:adsk.eagle:symbol:26928/1" library_version="1">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="VCC" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="+3V3" urn="urn:adsk.eagle:component:26981/1" prefix="+3V3" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="+3V3" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GND" urn="urn:adsk.eagle:component:26954/1" prefix="GND" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="VCC" urn="urn:adsk.eagle:component:26957/1" prefix="P+" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="VCC" symbol="VCC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="fab">
<packages>
<package name="AYZ0102AGRLC">
<wire x1="-3.6" y1="-1.5" x2="3.6" y2="-1.5" width="0.127" layer="21"/>
<wire x1="3.6" y1="-1.5" x2="3.6" y2="1.5" width="0.127" layer="21"/>
<wire x1="3.6" y1="1.5" x2="1.5" y2="1.5" width="0.127" layer="21"/>
<wire x1="1.5" y1="1.5" x2="-1.5" y2="1.5" width="0.127" layer="21"/>
<wire x1="-1.5" y1="1.5" x2="-3.6" y2="1.5" width="0.127" layer="21"/>
<wire x1="-3.6" y1="1.5" x2="-3.6" y2="-1.5" width="0.127" layer="21"/>
<wire x1="0" y1="1.6" x2="0" y2="2.5" width="0.127" layer="21"/>
<wire x1="0" y1="2.5" x2="1.5" y2="2.5" width="0.127" layer="21"/>
<wire x1="1.5" y1="2.5" x2="1.5" y2="1.5" width="0.127" layer="21"/>
<smd name="2" x="0" y="-2.3" dx="1.27" dy="1.397" layer="1"/>
<smd name="3" x="-2.5" y="-2.3" dx="1.27" dy="1.397" layer="1"/>
<smd name="1" x="2.5" y="-2.3" dx="1.27" dy="1.397" layer="1"/>
<text x="-2.794" y="2.794" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.302" y="-4.572" size="1.27" layer="27">&gt;VALUE</text>
<hole x="-1.5" y="0.4" drill="0.85"/>
<hole x="1.5" y="0.4" drill="0.85"/>
</package>
<package name="SLIDESWITCH-CL-SB-22A">
<smd name="P$1" x="-2.5" y="1.4" dx="1.2" dy="1.6" layer="1" rot="R180"/>
<smd name="P$2" x="0" y="1.4" dx="1.2" dy="1.6" layer="1" rot="R180"/>
<smd name="P$3" x="2.5" y="1.4" dx="1.2" dy="1.6" layer="1" rot="R180"/>
<smd name="P$4" x="-2.5" y="-1.4" dx="1.2" dy="1.6" layer="1" rot="R180"/>
<smd name="P$5" x="0" y="-1.4" dx="1.2" dy="1.6" layer="1" rot="R180"/>
<smd name="P$6" x="2.5" y="-1.4" dx="1.2" dy="1.6" layer="1" rot="R180"/>
<wire x1="-4.25" y1="1.75" x2="4.25" y2="1.75" width="0.127" layer="48"/>
<wire x1="4.25" y1="1.75" x2="4.25" y2="-1.75" width="0.127" layer="48"/>
<wire x1="4.25" y1="-1.75" x2="-4.25" y2="-1.75" width="0.127" layer="48"/>
<wire x1="-4.25" y1="-1.75" x2="-4.25" y2="1.75" width="0.127" layer="48"/>
<wire x1="-2" y1="1.8" x2="-2" y2="3.7" width="0.127" layer="48"/>
<wire x1="-2" y1="3.7" x2="-0.5" y2="3.7" width="0.127" layer="48"/>
<wire x1="-0.5" y1="3.7" x2="-0.5" y2="1.8" width="0.127" layer="48"/>
<wire x1="-2.5" y1="1.2" x2="-2.5" y2="0.5" width="0.127" layer="48"/>
<wire x1="0" y1="1.2" x2="0" y2="0.5" width="0.127" layer="48"/>
<wire x1="0" y1="-0.5" x2="0" y2="-1.3" width="0.127" layer="48"/>
<wire x1="-2.5" y1="-0.5" x2="-2.5" y2="-1.4" width="0.127" layer="48"/>
<wire x1="2.5" y1="-0.5" x2="2.5" y2="-1.3" width="0.127" layer="48"/>
<wire x1="2.5" y1="0.5" x2="2.5" y2="1.3" width="0.127" layer="48"/>
<wire x1="-0.1" y1="0.5" x2="0.1" y2="0.5" width="0.127" layer="48"/>
<wire x1="-2.6" y1="0.5" x2="-2.4" y2="0.5" width="0.127" layer="48"/>
<wire x1="-2.6" y1="-0.5" x2="-2.4" y2="-0.5" width="0.127" layer="48"/>
<wire x1="-0.1" y1="-0.5" x2="0.1" y2="-0.5" width="0.127" layer="48"/>
<wire x1="2.4" y1="-0.5" x2="2.6" y2="-0.5" width="0.127" layer="48"/>
<wire x1="2.4" y1="0.5" x2="2.6" y2="0.5" width="0.127" layer="48"/>
<wire x1="-3" y1="0.3" x2="0.4" y2="0.3" width="0.127" layer="48"/>
<wire x1="0.4" y1="0.3" x2="0.4" y2="-0.3" width="0.127" layer="48"/>
<wire x1="0.4" y1="-0.3" x2="-3" y2="-0.3" width="0.127" layer="48"/>
<wire x1="-3" y1="-0.3" x2="-3" y2="0.3" width="0.127" layer="48"/>
</package>
</packages>
<symbols>
<symbol name="SWITCH-SPDT">
<wire x1="-2.54" y1="0" x2="2.54" y2="1.27" width="0.254" layer="94"/>
<circle x="2.54" y="2.54" radius="0.635" width="0.254" layer="94"/>
<circle x="2.54" y="-2.54" radius="0.635" width="0.254" layer="94"/>
<circle x="-2.54" y="0" radius="0.635" width="0.254" layer="94"/>
<text x="-4.064" y="4.318" size="1.778" layer="95">&gt;NAME</text>
<text x="-4.826" y="-6.096" size="1.778" layer="95">&gt;VALUE</text>
<pin name="1" x="5.08" y="2.54" visible="pad" length="short" rot="R180"/>
<pin name="2" x="-5.08" y="0" visible="pad" length="short"/>
<pin name="3" x="5.08" y="-2.54" visible="pad" length="short" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="SLIDE-SWITCH" prefix="S">
<description>SMD slide-switch AYZ0102AGRLC as found in the fablab inventory.  Includes the mounting holes.

&lt;p&gt;Made by Zaerc.</description>
<gates>
<gate name="G$1" symbol="SWITCH-SPDT" x="0" y="0"/>
</gates>
<devices>
<device name="" package="AYZ0102AGRLC">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMT" package="SLIDESWITCH-CL-SB-22A">
<connects>
<connect gate="G$1" pin="1" pad="P$1 P$4"/>
<connect gate="G$1" pin="2" pad="P$2 P$5"/>
<connect gate="G$1" pin="3" pad="P$3 P$6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="borkedlabs-passives">
<packages>
<package name="0805">
<smd name="1" x="-0.95" y="0" dx="0.7" dy="1.2" layer="1"/>
<smd name="2" x="0.95" y="0" dx="0.7" dy="1.2" layer="1"/>
<text x="-0.762" y="0.8255" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.016" y="-2.032" size="1.016" layer="27">&gt;VALUE</text>
</package>
<package name="0603-CAP">
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.332" x2="0.356" y2="0.332" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.319" x2="0.356" y2="-0.319" width="0.1016" layer="51"/>
<smd name="1" x="-0.8" y="0" dx="0.96" dy="0.8" layer="1"/>
<smd name="2" x="0.8" y="0" dx="0.96" dy="0.8" layer="1"/>
<text x="-0.889" y="1.397" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.016" y="-2.413" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4" x2="-0.3381" y2="0.4" layer="51"/>
<rectangle x1="0.3302" y1="-0.4" x2="0.8303" y2="0.4" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="0402-CAP">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="0" y1="0.0305" x2="0" y2="-0.0305" width="0.4064" layer="21"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.889" y="0.6985" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.0795" y="-2.413" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="1210">
<wire x1="-1.6" y1="1.3" x2="1.6" y2="1.3" width="0.127" layer="51"/>
<wire x1="1.6" y1="1.3" x2="1.6" y2="-1.3" width="0.127" layer="51"/>
<wire x1="1.6" y1="-1.3" x2="-1.6" y2="-1.3" width="0.127" layer="51"/>
<wire x1="-1.6" y1="-1.3" x2="-1.6" y2="1.3" width="0.127" layer="51"/>
<wire x1="-1.6" y1="1.3" x2="1.6" y2="1.3" width="0.2032" layer="21"/>
<wire x1="-1.6" y1="-1.3" x2="1.6" y2="-1.3" width="0.2032" layer="21"/>
<smd name="1" x="-1.6" y="0" dx="1.2" dy="2" layer="1"/>
<smd name="2" x="1.6" y="0" dx="1.2" dy="2" layer="1"/>
<text x="-2.07" y="1.77" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.17" y="-3.24" size="1.016" layer="27">&gt;VALUE</text>
</package>
<package name="1206">
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.27" y="1.143" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.397" y="-2.794" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="0603-RES">
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.889" y="1.397" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.016" y="-2.413" size="1.016" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<rectangle x1="-0.2286" y1="-0.381" x2="0.2286" y2="0.381" layer="21"/>
</package>
<package name="0402-RES">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.889" y="0.6985" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.0795" y="-1.778" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<rectangle x1="-0.2032" y1="-0.3556" x2="0.2032" y2="0.3556" layer="21"/>
</package>
<package name="R2010">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="-1.027" y1="1.245" x2="1.027" y2="1.245" width="0.1524" layer="21"/>
<wire x1="-1.002" y1="-1.245" x2="1.016" y2="-1.245" width="0.1524" layer="21"/>
<smd name="1" x="-2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<smd name="2" x="2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<text x="-2.54" y="1.5875" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.302" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
</package>
<package name="R2512">
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<smd name="1" x="-2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<smd name="2" x="2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<text x="-2.54" y="1.905" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
</package>
<package name="TO220ACS">
<description>&lt;B&gt;DIODE&lt;/B&gt;&lt;p&gt;
2-lead molded, vertical</description>
<wire x1="5.08" y1="-1.143" x2="4.953" y2="-4.064" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-4.318" x2="4.953" y2="-4.064" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-4.318" x2="-4.699" y2="-4.318" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="-4.064" x2="-4.699" y2="-4.318" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="-4.064" x2="-5.08" y2="-1.143" width="0.1524" layer="21"/>
<circle x="-4.4958" y="-3.7084" radius="0.254" width="0" layer="21"/>
<pad name="C" x="-2.54" y="-2.54" drill="1.016" shape="long" rot="R90"/>
<pad name="A" x="2.54" y="-2.54" drill="1.016" shape="long" rot="R90"/>
<text x="-5.08" y="-6.0452" size="1.016" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-7.62" size="1.016" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-5.334" y1="-0.762" x2="5.334" y2="0" layer="21"/>
<rectangle x1="-5.334" y1="-1.27" x2="-3.429" y2="-0.762" layer="21"/>
<rectangle x1="-3.429" y1="-1.27" x2="-1.651" y2="-0.762" layer="51"/>
<rectangle x1="3.429" y1="-1.27" x2="5.334" y2="-0.762" layer="21"/>
<rectangle x1="1.651" y1="-1.27" x2="3.429" y2="-0.762" layer="51"/>
<rectangle x1="-1.651" y1="-1.27" x2="1.651" y2="-0.762" layer="21"/>
</package>
<package name="6.6X6.6-CAP">
<wire x1="-3.25" y1="3.25" x2="1.55" y2="3.25" width="0.1016" layer="51"/>
<wire x1="1.55" y1="3.25" x2="3.25" y2="1.55" width="0.1016" layer="51"/>
<wire x1="3.25" y1="1.55" x2="3.25" y2="-1.55" width="0.1016" layer="51"/>
<wire x1="3.25" y1="-1.55" x2="1.55" y2="-3.25" width="0.1016" layer="51"/>
<wire x1="1.55" y1="-3.25" x2="-3.25" y2="-3.25" width="0.1016" layer="51"/>
<wire x1="-3.25" y1="-3.25" x2="-3.25" y2="3.25" width="0.1016" layer="51"/>
<wire x1="-2.1" y1="2.25" x2="-2.1" y2="-2.2" width="0.1016" layer="51"/>
<circle x="0" y="0" radius="3.1" width="0.1016" layer="51"/>
<smd name="+" x="2.4" y="0" dx="3" dy="1.4" layer="1"/>
<smd name="-" x="-2.4" y="0" dx="3" dy="1.4" layer="1"/>
<text x="-2.75" y="4" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.75" y="-4.975" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-3.65" y1="-0.35" x2="-3.05" y2="0.35" layer="51"/>
<rectangle x1="3.05" y1="-0.35" x2="3.65" y2="0.35" layer="51"/>
<polygon width="0.1016" layer="51">
<vertex x="-2.15" y="2.15"/>
<vertex x="-2.6" y="1.6"/>
<vertex x="-2.9" y="0.9"/>
<vertex x="-3.05" y="0"/>
<vertex x="-2.9" y="-0.95"/>
<vertex x="-2.55" y="-1.65"/>
<vertex x="-2.15" y="-2.15"/>
<vertex x="-2.15" y="2.1"/>
</polygon>
<text x="-0.762" y="1.27" size="0.6096" layer="21" font="vector">BFC</text>
<wire x1="1.905" y1="1.524" x2="1.397" y2="1.524" width="0.127" layer="21"/>
<wire x1="1.651" y1="1.778" x2="1.651" y2="1.27" width="0.127" layer="21"/>
<wire x1="-1.905" y1="1.524" x2="-1.397" y2="1.524" width="0.127" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="CAP">
<wire x1="0" y1="2.54" x2="0" y2="2.032" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="0.508" width="0.1524" layer="94"/>
<text x="1.524" y="2.921" size="1.778" layer="95">&gt;NAME</text>
<text x="1.524" y="-2.159" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.032" y1="0.508" x2="2.032" y2="1.016" layer="94"/>
<rectangle x1="-2.032" y1="1.524" x2="2.032" y2="2.032" layer="94"/>
<pin name="1" x="0" y="5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
<text x="1.524" y="-4.064" size="1.27" layer="97">&gt;PACKAGE</text>
<text x="1.524" y="-5.842" size="1.27" layer="97">&gt;VOLTAGE</text>
<text x="1.524" y="-7.62" size="1.27" layer="97">&gt;TYPE</text>
</symbol>
<symbol name="RESISTOR">
<wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.1524" layer="94"/>
<wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.1524" layer="94"/>
<wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.1524" layer="94"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<text x="-3.81" y="-6.858" size="1.27" layer="97">&gt;PRECISION</text>
<text x="-3.81" y="-5.08" size="1.27" layer="97">&gt;PACKAGE</text>
</symbol>
<symbol name="CAP-POL">
<wire x1="-2.54" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="-1.016" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-1" x2="2.4892" y2="-1.8542" width="0.254" layer="94" curve="-37.878202" cap="flat"/>
<wire x1="-2.4669" y1="-1.8504" x2="0" y2="-1.0161" width="0.254" layer="94" curve="-37.376341" cap="flat"/>
<text x="1.016" y="0.635" size="1.778" layer="95">&gt;NAME</text>
<text x="1.016" y="-4.191" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.253" y1="0.668" x2="-1.364" y2="0.795" layer="94"/>
<rectangle x1="-1.872" y1="0.287" x2="-1.745" y2="1.176" layer="94"/>
<pin name="+" x="0" y="2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="-" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="CAP" prefix="C" uservalue="yes">
<description>&lt;b&gt;Capacitor&lt;/b&gt;
Standard 0603 ceramic capacitor, and 0.1" leaded capacitor.</description>
<gates>
<gate name="G$1" symbol="CAP" x="0" y="0"/>
</gates>
<devices>
<device name="0805" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="0805"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VOLTAGE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="0603-CAP" package="0603-CAP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="0603"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VOLTAGE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="0402-CAP" package="0402-CAP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="0402"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VOLTAGE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="1210" package="1210">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="1210" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VOLTAGE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="1206" package="1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="1206" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VOLTAGE" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RESISTOR" prefix="R" uservalue="yes">
<description>&lt;b&gt;Resistor&lt;/b&gt;
Basic schematic elements and footprints for 0603, 1206, and PTH resistors.</description>
<gates>
<gate name="G$1" symbol="RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="1206" package="1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="1206" constant="no"/>
<attribute name="PRECISION" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="2010" package="R2010">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="2010"/>
<attribute name="PRECISION" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="0805-RES" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="0805"/>
<attribute name="PRECISION" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="0603-RES" package="0603-RES">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="0603"/>
<attribute name="PRECISION" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="0402-RES" package="0402-RES">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="0402"/>
<attribute name="PRECISION" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="2512" package="R2512">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="2512"/>
<attribute name="PRECISION" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TO220ACS" package="TO220ACS">
<connects>
<connect gate="G$1" pin="1" pad="A"/>
<connect gate="G$1" pin="2" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CAP-POL" prefix="C" uservalue="yes">
<gates>
<gate name="G$1" symbol="CAP-POL" x="0" y="0"/>
</gates>
<devices>
<device name="-6.6X6.6" package="6.6X6.6-CAP">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="power">
<packages>
<package name="PWRPAD_3-25MM">
<pad name="P$1" x="0" y="0" drill="3.25" diameter="5.75" thermals="no"/>
</package>
<package name="HTSSOP-56">
<smd name="P$1" x="-3.749990625" y="6.749984375" dx="1.5494" dy="0.2794" layer="1" rot="R180"/>
<smd name="P$2" x="-3.749990625" y="6.2499875" dx="1.5494" dy="0.2794" layer="1" rot="R180"/>
<smd name="P$3" x="-3.749990625" y="5.7499875" dx="1.5494" dy="0.2794" layer="1" rot="R180"/>
<smd name="P$4" x="-3.749990625" y="5.2499875" dx="1.5494" dy="0.2794" layer="1" rot="R180"/>
<smd name="P$5" x="-3.749990625" y="4.7499875" dx="1.5494" dy="0.2794" layer="1" rot="R180"/>
<smd name="P$6" x="-3.749990625" y="4.249990625" dx="1.5494" dy="0.2794" layer="1" rot="R180"/>
<smd name="P$7" x="-3.749990625" y="3.749990625" dx="1.5494" dy="0.2794" layer="1" rot="R180"/>
<smd name="P$8" x="-3.749990625" y="3.249990625" dx="1.5494" dy="0.2794" layer="1" rot="R180"/>
<smd name="P$9" x="-3.749990625" y="2.74999375" dx="1.5494" dy="0.2794" layer="1" rot="R180"/>
<smd name="P$10" x="-3.749990625" y="2.24999375" dx="1.5494" dy="0.2794" layer="1" rot="R180"/>
<smd name="P$11" x="-3.749990625" y="1.74999375" dx="1.5494" dy="0.2794" layer="1" rot="R180"/>
<smd name="P$12" x="-3.749990625" y="1.249996875" dx="1.5494" dy="0.2794" layer="1" rot="R180"/>
<smd name="P$13" x="-3.749990625" y="0.749996875" dx="1.5494" dy="0.2794" layer="1" rot="R180"/>
<smd name="P$14" x="-3.749990625" y="0.249996875" dx="1.5494" dy="0.2794" layer="1" rot="R180"/>
<smd name="P$15" x="-3.749990625" y="-0.25" dx="1.5494" dy="0.2794" layer="1" rot="R180"/>
<smd name="P$16" x="-3.749990625" y="-0.75" dx="1.5494" dy="0.2794" layer="1" rot="R180"/>
<smd name="P$17" x="-3.749990625" y="-1.25" dx="1.5494" dy="0.2794" layer="1" rot="R180"/>
<smd name="P$18" x="-3.749990625" y="-1.749996875" dx="1.5494" dy="0.2794" layer="1" rot="R180"/>
<smd name="P$19" x="-3.749990625" y="-2.249996875" dx="1.5494" dy="0.2794" layer="1" rot="R180"/>
<smd name="P$20" x="-3.749990625" y="-2.749996875" dx="1.5494" dy="0.2794" layer="1" rot="R180"/>
<smd name="P$21" x="-3.749990625" y="-3.24999375" dx="1.5494" dy="0.2794" layer="1" rot="R180"/>
<smd name="P$22" x="-3.749990625" y="-3.74999375" dx="1.5494" dy="0.2794" layer="1" rot="R180"/>
<smd name="P$23" x="-3.749990625" y="-4.24999375" dx="1.5494" dy="0.2794" layer="1" rot="R180"/>
<smd name="P$24" x="-3.749990625" y="-4.749990625" dx="1.5494" dy="0.2794" layer="1" rot="R180"/>
<smd name="P$25" x="-3.749990625" y="-5.249990625" dx="1.5494" dy="0.2794" layer="1" rot="R180"/>
<smd name="P$26" x="-3.749990625" y="-5.749990625" dx="1.5494" dy="0.2794" layer="1" rot="R180"/>
<smd name="P$27" x="-3.749990625" y="-6.2499875" dx="1.5494" dy="0.2794" layer="1" rot="R180"/>
<smd name="P$28" x="-3.749990625" y="-6.7499875" dx="1.5494" dy="0.2794" layer="1" rot="R180"/>
<smd name="P$29" x="3.74999375" y="-6.7499875" dx="1.5494" dy="0.2794" layer="1"/>
<smd name="P$30" x="3.74999375" y="-6.2499875" dx="1.5494" dy="0.2794" layer="1"/>
<smd name="P$31" x="3.74999375" y="-5.749990625" dx="1.5494" dy="0.2794" layer="1"/>
<smd name="P$32" x="3.74999375" y="-5.249990625" dx="1.5494" dy="0.2794" layer="1"/>
<smd name="P$33" x="3.74999375" y="-4.749990625" dx="1.5494" dy="0.2794" layer="1"/>
<smd name="P$34" x="3.74999375" y="-4.24999375" dx="1.5494" dy="0.2794" layer="1"/>
<smd name="P$35" x="3.74999375" y="-3.74999375" dx="1.5494" dy="0.2794" layer="1"/>
<smd name="P$36" x="3.74999375" y="-3.24999375" dx="1.5494" dy="0.2794" layer="1"/>
<smd name="P$37" x="3.74999375" y="-2.749996875" dx="1.5494" dy="0.2794" layer="1"/>
<smd name="P$38" x="3.74999375" y="-2.249996875" dx="1.5494" dy="0.2794" layer="1"/>
<smd name="P$39" x="3.74999375" y="-1.749996875" dx="1.5494" dy="0.2794" layer="1"/>
<smd name="P$40" x="3.74999375" y="-1.25" dx="1.5494" dy="0.2794" layer="1"/>
<smd name="P$41" x="3.74999375" y="-0.75" dx="1.5494" dy="0.2794" layer="1"/>
<smd name="P$42" x="3.74999375" y="-0.25" dx="1.5494" dy="0.2794" layer="1"/>
<smd name="P$43" x="3.74999375" y="0.249996875" dx="1.5494" dy="0.2794" layer="1"/>
<smd name="P$44" x="3.74999375" y="0.749996875" dx="1.5494" dy="0.2794" layer="1"/>
<smd name="P$45" x="3.74999375" y="1.249996875" dx="1.5494" dy="0.2794" layer="1"/>
<smd name="P$46" x="3.74999375" y="1.74999375" dx="1.5494" dy="0.2794" layer="1"/>
<smd name="P$47" x="3.74999375" y="2.24999375" dx="1.5494" dy="0.2794" layer="1"/>
<smd name="P$48" x="3.74999375" y="2.74999375" dx="1.5494" dy="0.2794" layer="1"/>
<smd name="P$49" x="3.74999375" y="3.249990625" dx="1.5494" dy="0.2794" layer="1"/>
<smd name="P$50" x="3.74999375" y="3.749990625" dx="1.5494" dy="0.2794" layer="1"/>
<smd name="P$51" x="3.74999375" y="4.249990625" dx="1.5494" dy="0.2794" layer="1"/>
<smd name="P$52" x="3.74999375" y="4.7499875" dx="1.5494" dy="0.2794" layer="1"/>
<smd name="P$53" x="3.74999375" y="5.2499875" dx="1.5494" dy="0.2794" layer="1"/>
<smd name="P$54" x="3.74999375" y="5.7499875" dx="1.5494" dy="0.2794" layer="1"/>
<smd name="P$55" x="3.74999375" y="6.2499875" dx="1.5494" dy="0.2794" layer="1"/>
<smd name="P$56" x="3.74999375" y="6.749984375" dx="1.5494" dy="0.2794" layer="1"/>
<smd name="P$57" x="0" y="0" dx="3.5814" dy="6.35" layer="1" thermals="no"/>
<wire x1="-3.185478125" y1="7.08869375" x2="-3.185478125" y2="-7.088696875" width="0.1524" layer="21"/>
<wire x1="-3.185478125" y1="-7.088696875" x2="3.185478125" y2="-7.088696875" width="0.127" layer="21"/>
<wire x1="3.185478125" y1="-7.088696875" x2="3.185478125" y2="7.08869375" width="0.127" layer="21"/>
<wire x1="3.185478125" y1="7.08869375" x2="-3.185478125" y2="7.08869375" width="0.127" layer="21"/>
<text x="-2.54" y="7.62" size="1.27" layer="104">&gt;NAME</text>
<text x="-2.54" y="-8.89" size="1.27" layer="127">&gt;VALUE</text>
<circle x="-2.286" y="6.477" radius="0.254" width="0.127" layer="21"/>
</package>
<package name="PWRPAD_4MM">
<pad name="P$1" x="0" y="0" drill="3.9878" diameter="6.35" thermals="no"/>
</package>
<package name="PWRPAD_SC-02_2-45MM">
<pad name="P$1" x="0" y="0" drill="2.45" diameter="4.24" thermals="no"/>
</package>
<package name="PWRPAD_2-65MM">
<pad name="P$1" x="0" y="0" drill="2.65" diameter="4.65" thermals="no"/>
</package>
<package name="PWRPAD_2-05MM">
<pad name="P$1" x="0" y="0" drill="2.05" diameter="3.8" thermals="no"/>
</package>
<package name="WSL2726">
<description>&lt;b&gt;SMD SHUNT RESISTOR&lt;/b&gt;</description>
<wire x1="-4.8768" y1="7.9756" x2="4.8768" y2="7.9756" width="0.2032" layer="51"/>
<wire x1="4.8768" y1="7.9756" x2="4.8768" y2="-0.3556" width="0.2032" layer="51"/>
<wire x1="4.8768" y1="-0.3556" x2="-4.8768" y2="-0.3556" width="0.2032" layer="51"/>
<wire x1="-4.8768" y1="-0.3556" x2="-4.8768" y2="7.9756" width="0.2032" layer="51"/>
<smd name="A" x="3.302" y="2.794" dx="2.4384" dy="5.588" layer="1"/>
<smd name="SA" x="3.302" y="6.9215" dx="2.4384" dy="0.889" layer="1"/>
<smd name="B" x="-3.302" y="2.794" dx="2.4384" dy="5.588" layer="1"/>
<smd name="SB" x="-3.302" y="6.9215" dx="2.4384" dy="0.889" layer="1"/>
<text x="-0.3556" y="-0.0508" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="1.5748" y="-0.0508" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
</package>
<package name="R2512">
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<smd name="A" x="-2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<smd name="B" x="2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<text x="-2.54" y="1.905" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<smd name="SA" x="-1.414" y="1.3" dx="0.5" dy="0.5" layer="1" rot="R180"/>
<smd name="SB" x="1.386" y="1.3" dx="0.5" dy="0.5" layer="1"/>
<wire x1="-1.4" y1="1.3" x2="-2.4" y2="1.3" width="0.2032" layer="1"/>
<wire x1="1.4" y1="1.3" x2="2.2" y2="1.3" width="0.2032" layer="1"/>
</package>
<package name="PWRPAD_3-25MM-SKINNY">
<pad name="P$1" x="0" y="0" drill="3.25" diameter="5.35" thermals="no"/>
</package>
<package name="PWRPAD_M3_STANDOFF">
<pad name="P$1" x="0" y="0" drill="4.4" diameter="7" thermals="no"/>
<polygon width="0.127" layer="31">
<vertex x="-0.6" y="3.6"/>
<vertex x="0.6" y="3.6"/>
<vertex x="0.4" y="2.1"/>
<vertex x="-0.4" y="2.1"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="0.6" y="-3.6"/>
<vertex x="-0.6" y="-3.6"/>
<vertex x="-0.4" y="-2.1"/>
<vertex x="0.4" y="-2.1"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-3.6" y="-0.6"/>
<vertex x="-3.6" y="0.6"/>
<vertex x="-2.1" y="0.4"/>
<vertex x="-2.1" y="-0.4"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="3.6" y="0.6"/>
<vertex x="3.6" y="-0.6"/>
<vertex x="2.1" y="-0.4"/>
<vertex x="2.1" y="0.4"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-2.95269375" y="2.13136875"/>
<vertex x="-2.104165625" y="2.979896875"/>
<vertex x="-1.19203125" y="1.784921875"/>
<vertex x="-1.75771875" y="1.2192375"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="2.99705625" y="-2.12131875"/>
<vertex x="2.148528125" y="-2.969846875"/>
<vertex x="1.23639375" y="-1.774871875"/>
<vertex x="1.80208125" y="-1.2091875"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-2.104165625" y="-2.969846875"/>
<vertex x="-2.95269375" y="-2.12131875"/>
<vertex x="-1.75771875" y="-1.2091875"/>
<vertex x="-1.19203125" y="-1.774871875"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="2.148528125" y="2.979896875"/>
<vertex x="2.99705625" y="2.13136875"/>
<vertex x="1.80208125" y="1.2192375"/>
<vertex x="1.23639375" y="1.784921875"/>
</polygon>
<circle x="0" y="0" radius="3.5" width="0.125" layer="51"/>
</package>
<package name="PWRPAD_M25_STANDOFF">
<pad name="P$1" x="0" y="0" drill="3.7" diameter="6" thermals="no"/>
<polygon width="0.127" layer="31">
<vertex x="-0.6" y="3"/>
<vertex x="0.6" y="3"/>
<vertex x="0.4" y="1.9"/>
<vertex x="-0.4" y="1.9"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="0.6" y="-3"/>
<vertex x="-0.6" y="-3"/>
<vertex x="-0.4" y="-1.9"/>
<vertex x="0.4" y="-1.9"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-3" y="-0.6"/>
<vertex x="-3" y="0.6"/>
<vertex x="-1.9" y="0.4"/>
<vertex x="-1.9" y="-0.4"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="3" y="0.5"/>
<vertex x="3" y="-0.7"/>
<vertex x="1.9" y="-0.5"/>
<vertex x="1.9" y="0.3"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-2.55269375" y="1.73136875"/>
<vertex x="-1.704165625" y="2.579896875"/>
<vertex x="-0.99203125" y="1.584921875"/>
<vertex x="-1.55771875" y="1.0192375"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="2.49705625" y="-1.72131875"/>
<vertex x="1.648528125" y="-2.569846875"/>
<vertex x="1.03639375" y="-1.574871875"/>
<vertex x="1.60208125" y="-1.0091875"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-1.704165625" y="-2.669846875"/>
<vertex x="-2.55269375" y="-1.82131875"/>
<vertex x="-1.55771875" y="-1.1091875"/>
<vertex x="-0.99203125" y="-1.674871875"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="1.748528125" y="2.579896875"/>
<vertex x="2.59705625" y="1.73136875"/>
<vertex x="1.60208125" y="1.0192375"/>
<vertex x="1.03639375" y="1.584921875"/>
</polygon>
</package>
<package name="DSOP-ADVANCE">
<smd name="P$1" x="-1.905" y="-2.8" dx="0.65" dy="1" layer="1"/>
<smd name="P$2" x="-0.635" y="-2.8" dx="0.65" dy="1" layer="1"/>
<smd name="P$3" x="0.635" y="-2.8" dx="0.65" dy="1" layer="1"/>
<smd name="P$4" x="1.905" y="-2.8" dx="0.65" dy="1" layer="1"/>
<smd name="P$5" x="0" y="0.95" dx="5.5" dy="4.7" layer="1"/>
<circle x="-2.7" y="-3.8" radius="0.22360625" width="0.127" layer="21"/>
<wire x1="-2.5" y1="3" x2="2.5" y2="3" width="0.127" layer="48"/>
<wire x1="2.5" y1="3" x2="2.5" y2="-3" width="0.127" layer="48"/>
<wire x1="2.5" y1="-3" x2="-2.5" y2="-3" width="0.127" layer="48"/>
<wire x1="-2.5" y1="-3" x2="-2.5" y2="3" width="0.127" layer="48"/>
</package>
<package name="SL-LINK">
<smd name="1" x="-0.1" y="0" dx="0.3" dy="0.3" layer="1"/>
<smd name="2" x="0.1" y="0" dx="0.3" dy="0.3" layer="1"/>
</package>
</packages>
<symbols>
<symbol name="PWRPAD">
<pin name="PWRPAD" x="-5.08" y="0" length="middle"/>
</symbol>
<symbol name="DRV8302">
<pin name="RT_CLK" x="-17.78" y="33.02" length="middle"/>
<pin name="COMP" x="-17.78" y="30.48" length="middle"/>
<pin name="VSENSE" x="-17.78" y="27.94" length="middle"/>
<pin name="PWRGND" x="-17.78" y="25.4" length="middle"/>
<pin name="NOCTW" x="-17.78" y="22.86" length="middle"/>
<pin name="NFAULT" x="-17.78" y="20.32" length="middle"/>
<pin name="DTC" x="-17.78" y="17.78" length="middle"/>
<pin name="M_PWM" x="-17.78" y="15.24" length="middle"/>
<pin name="M_OC" x="-17.78" y="12.7" length="middle"/>
<pin name="GAIN" x="-17.78" y="10.16" length="middle"/>
<pin name="OC_ADJ" x="-17.78" y="7.62" length="middle"/>
<pin name="DC_CAL" x="-17.78" y="5.08" length="middle"/>
<pin name="GVDD" x="-17.78" y="2.54" length="middle"/>
<pin name="CP1" x="-17.78" y="0" length="middle"/>
<pin name="CP2" x="-17.78" y="-2.54" length="middle"/>
<pin name="EN_GATE" x="-17.78" y="-5.08" length="middle"/>
<pin name="INH_A" x="-17.78" y="-7.62" length="middle"/>
<pin name="INL_A" x="-17.78" y="-10.16" length="middle"/>
<pin name="INH_B" x="-17.78" y="-12.7" length="middle"/>
<pin name="INL_B" x="-17.78" y="-15.24" length="middle"/>
<pin name="INH_C" x="-17.78" y="-17.78" length="middle"/>
<pin name="INL_C" x="-17.78" y="-20.32" length="middle"/>
<pin name="DVDD" x="-17.78" y="-22.86" length="middle"/>
<pin name="REF" x="-17.78" y="-25.4" length="middle"/>
<pin name="SO1" x="-17.78" y="-27.94" length="middle"/>
<pin name="SO2" x="-17.78" y="-30.48" length="middle"/>
<pin name="AVDD" x="-17.78" y="-33.02" length="middle"/>
<pin name="AGND" x="-17.78" y="-35.56" length="middle"/>
<pin name="PVDD1" x="17.78" y="-35.56" length="middle" rot="R180"/>
<pin name="SP2" x="17.78" y="-33.02" length="middle" rot="R180"/>
<pin name="SN2" x="17.78" y="-30.48" length="middle" rot="R180"/>
<pin name="SP1" x="17.78" y="-27.94" length="middle" rot="R180"/>
<pin name="SN1" x="17.78" y="-25.4" length="middle" rot="R180"/>
<pin name="SL_C" x="17.78" y="-22.86" length="middle" rot="R180"/>
<pin name="GL_C" x="17.78" y="-20.32" length="middle" rot="R180"/>
<pin name="SH_C" x="17.78" y="-17.78" length="middle" rot="R180"/>
<pin name="GH_C" x="17.78" y="-15.24" length="middle" rot="R180"/>
<pin name="BST_C" x="17.78" y="-12.7" length="middle" rot="R180"/>
<pin name="SL_B" x="17.78" y="-10.16" length="middle" rot="R180"/>
<pin name="GL_B" x="17.78" y="-7.62" length="middle" rot="R180"/>
<pin name="SH_B" x="17.78" y="-5.08" length="middle" rot="R180"/>
<pin name="GH_B" x="17.78" y="-2.54" length="middle" rot="R180"/>
<pin name="BST_B" x="17.78" y="0" length="middle" rot="R180"/>
<pin name="SL_A" x="17.78" y="2.54" length="middle" rot="R180"/>
<pin name="GL_A" x="17.78" y="5.08" length="middle" rot="R180"/>
<pin name="SH_A" x="17.78" y="7.62" length="middle" rot="R180"/>
<pin name="GH_A" x="17.78" y="10.16" length="middle" rot="R180"/>
<pin name="BST_A" x="17.78" y="12.7" length="middle" rot="R180"/>
<pin name="BIAS" x="17.78" y="15.24" length="middle" rot="R180"/>
<pin name="PH1" x="17.78" y="17.78" length="middle" rot="R180"/>
<pin name="PH2" x="17.78" y="20.32" length="middle" rot="R180"/>
<pin name="BST_BK" x="17.78" y="22.86" length="middle" rot="R180"/>
<pin name="PVDD2-1" x="17.78" y="25.4" length="middle" rot="R180"/>
<pin name="PVDD2-2" x="17.78" y="27.94" length="middle" rot="R180"/>
<pin name="EN_BUCK" x="17.78" y="30.48" length="middle" rot="R180"/>
<pin name="SS_TR" x="17.78" y="33.02" length="middle" rot="R180"/>
<wire x1="-12.7" y1="35.56" x2="-12.7" y2="-38.1" width="0.254" layer="94"/>
<wire x1="-12.7" y1="-38.1" x2="12.7" y2="-38.1" width="0.254" layer="94"/>
<wire x1="12.7" y1="-38.1" x2="12.7" y2="35.56" width="0.254" layer="94"/>
<wire x1="12.7" y1="35.56" x2="-12.7" y2="35.56" width="0.254" layer="94"/>
<pin name="GNDPAD" x="0" y="-43.18" length="middle" rot="R90"/>
</symbol>
<symbol name="R_SHUNT">
<wire x1="-3.81" y1="-0.889" x2="1.27" y2="-0.889" width="0.254" layer="94"/>
<wire x1="1.27" y1="0.889" x2="-3.81" y2="0.889" width="0.254" layer="94"/>
<wire x1="1.27" y1="-0.889" x2="1.27" y2="-0.254" width="0.254" layer="94"/>
<wire x1="1.27" y1="-0.254" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="0.889" width="0.254" layer="94"/>
<wire x1="-3.81" y1="-0.889" x2="-3.81" y2="-0.254" width="0.254" layer="94"/>
<wire x1="-3.81" y1="-0.254" x2="-3.81" y2="0" width="0.254" layer="94"/>
<wire x1="-3.81" y1="0" x2="-3.81" y2="0.889" width="0.254" layer="94"/>
<wire x1="-5.08" y1="0" x2="-3.81" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="-2.54" x2="-3.81" y2="-0.254" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="1.27" y2="-0.254" width="0.1524" layer="94"/>
<text x="-4.572" y="1.3716" size="1.778" layer="95">&gt;NAME</text>
<text x="-4.826" y="-4.445" size="1.778" layer="96">&gt;VALUE</text>
<pin name="SB" x="-7.62" y="-2.54" visible="off" length="short" direction="pas" swaplevel="2"/>
<pin name="SA" x="5.08" y="-2.54" visible="off" length="short" direction="pas" swaplevel="2" rot="R180"/>
<pin name="B" x="-7.62" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="A" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="NFET-TPW4R008NH">
<description>MOSFET N-channel - Enhancement mode</description>
<wire x1="0.762" y1="0.762" x2="0.762" y2="0" width="0.254" layer="94"/>
<wire x1="0.762" y1="0" x2="0.762" y2="-0.762" width="0.254" layer="94"/>
<wire x1="0.762" y1="3.175" x2="0.762" y2="2.54" width="0.254" layer="94"/>
<wire x1="0.762" y1="2.54" x2="0.762" y2="1.905" width="0.254" layer="94"/>
<wire x1="0.762" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0.762" y1="-1.905" x2="0.762" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0.762" y1="-2.54" x2="0.762" y2="-3.175" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="0.762" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="3.81" y1="2.54" x2="3.81" y2="0.508" width="0.1524" layer="94"/>
<wire x1="3.81" y1="0.508" x2="3.81" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="3.81" y1="-0.508" x2="3.81" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="3.81" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0.762" y1="2.54" x2="3.81" y2="2.54" width="0.1524" layer="94"/>
<circle x="2.54" y="-2.54" radius="0.254" width="0" layer="94"/>
<circle x="2.54" y="2.54" radius="0.254" width="0" layer="94"/>
<text x="-11.43" y="3.81" size="1.778" layer="96" rot="MR180">&gt;VALUE</text>
<text x="-11.43" y="1.27" size="1.778" layer="95" rot="MR180">&gt;NAME</text>
<pin name="S" x="2.54" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="G" x="-2.54" y="-2.54" visible="off" length="short" direction="pas"/>
<pin name="D" x="2.54" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
<polygon width="0.1524" layer="94">
<vertex x="1.016" y="0"/>
<vertex x="2.032" y="0.508"/>
<vertex x="2.032" y="-0.508"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="3.81" y="0.508"/>
<vertex x="3.302" y="-0.254"/>
<vertex x="4.318" y="-0.254"/>
</polygon>
<wire x1="3.302" y1="0.508" x2="3.81" y2="0.508" width="0.1524" layer="94"/>
<wire x1="3.81" y1="0.508" x2="4.318" y2="0.508" width="0.1524" layer="94"/>
</symbol>
<symbol name="SL-LINK">
<pin name="P$1" x="-12.7" y="0" length="middle"/>
<pin name="P$2" x="12.7" y="0" length="middle" rot="R180"/>
<wire x1="-7.62" y1="-2.54" x2="-7.62" y2="2.54" width="0.254" layer="94"/>
<wire x1="-7.62" y1="2.54" x2="7.62" y2="2.54" width="0.254" layer="94"/>
<wire x1="7.62" y1="2.54" x2="7.62" y2="-2.54" width="0.254" layer="94"/>
<wire x1="7.62" y1="-2.54" x2="-7.62" y2="-2.54" width="0.254" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="PWRPAD" prefix="J">
<gates>
<gate name="G$1" symbol="PWRPAD" x="0" y="0"/>
</gates>
<devices>
<device name="SC-02_2-45MM" package="PWRPAD_SC-02_2-45MM">
<connects>
<connect gate="G$1" pin="PWRPAD" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="4MM" package="PWRPAD_4MM">
<connects>
<connect gate="G$1" pin="PWRPAD" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M3" package="PWRPAD_3-25MM">
<connects>
<connect gate="G$1" pin="PWRPAD" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M2.5" package="PWRPAD_2-65MM">
<connects>
<connect gate="G$1" pin="PWRPAD" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M2" package="PWRPAD_2-05MM">
<connects>
<connect gate="G$1" pin="PWRPAD" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M3-STANDOFF" package="PWRPAD_M3_STANDOFF">
<connects>
<connect gate="G$1" pin="PWRPAD" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M3-SKINNY" package="PWRPAD_3-25MM-SKINNY">
<connects>
<connect gate="G$1" pin="PWRPAD" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M2.5_STANDOFF" package="PWRPAD_M25_STANDOFF">
<connects>
<connect gate="G$1" pin="PWRPAD" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DRV8302" prefix="U">
<gates>
<gate name="G$1" symbol="DRV8302" x="0" y="0"/>
</gates>
<devices>
<device name="" package="HTSSOP-56">
<connects>
<connect gate="G$1" pin="AGND" pad="P$28"/>
<connect gate="G$1" pin="AVDD" pad="P$27"/>
<connect gate="G$1" pin="BIAS" pad="P$49"/>
<connect gate="G$1" pin="BST_A" pad="P$48"/>
<connect gate="G$1" pin="BST_B" pad="P$43"/>
<connect gate="G$1" pin="BST_BK" pad="P$52"/>
<connect gate="G$1" pin="BST_C" pad="P$38"/>
<connect gate="G$1" pin="COMP" pad="P$2"/>
<connect gate="G$1" pin="CP1" pad="P$14"/>
<connect gate="G$1" pin="CP2" pad="P$15"/>
<connect gate="G$1" pin="DC_CAL" pad="P$12"/>
<connect gate="G$1" pin="DTC" pad="P$7"/>
<connect gate="G$1" pin="DVDD" pad="P$23"/>
<connect gate="G$1" pin="EN_BUCK" pad="P$55"/>
<connect gate="G$1" pin="EN_GATE" pad="P$16"/>
<connect gate="G$1" pin="GAIN" pad="P$10"/>
<connect gate="G$1" pin="GH_A" pad="P$47"/>
<connect gate="G$1" pin="GH_B" pad="P$42"/>
<connect gate="G$1" pin="GH_C" pad="P$37"/>
<connect gate="G$1" pin="GL_A" pad="P$45"/>
<connect gate="G$1" pin="GL_B" pad="P$40"/>
<connect gate="G$1" pin="GL_C" pad="P$35"/>
<connect gate="G$1" pin="GNDPAD" pad="P$57"/>
<connect gate="G$1" pin="GVDD" pad="P$13"/>
<connect gate="G$1" pin="INH_A" pad="P$17"/>
<connect gate="G$1" pin="INH_B" pad="P$19"/>
<connect gate="G$1" pin="INH_C" pad="P$21"/>
<connect gate="G$1" pin="INL_A" pad="P$18"/>
<connect gate="G$1" pin="INL_B" pad="P$20"/>
<connect gate="G$1" pin="INL_C" pad="P$22"/>
<connect gate="G$1" pin="M_OC" pad="P$9"/>
<connect gate="G$1" pin="M_PWM" pad="P$8"/>
<connect gate="G$1" pin="NFAULT" pad="P$6"/>
<connect gate="G$1" pin="NOCTW" pad="P$5"/>
<connect gate="G$1" pin="OC_ADJ" pad="P$11"/>
<connect gate="G$1" pin="PH1" pad="P$50"/>
<connect gate="G$1" pin="PH2" pad="P$51"/>
<connect gate="G$1" pin="PVDD1" pad="P$29"/>
<connect gate="G$1" pin="PVDD2-1" pad="P$53"/>
<connect gate="G$1" pin="PVDD2-2" pad="P$54"/>
<connect gate="G$1" pin="PWRGND" pad="P$4"/>
<connect gate="G$1" pin="REF" pad="P$24"/>
<connect gate="G$1" pin="RT_CLK" pad="P$1"/>
<connect gate="G$1" pin="SH_A" pad="P$46"/>
<connect gate="G$1" pin="SH_B" pad="P$41"/>
<connect gate="G$1" pin="SH_C" pad="P$36"/>
<connect gate="G$1" pin="SL_A" pad="P$44"/>
<connect gate="G$1" pin="SL_B" pad="P$39"/>
<connect gate="G$1" pin="SL_C" pad="P$34"/>
<connect gate="G$1" pin="SN1" pad="P$33"/>
<connect gate="G$1" pin="SN2" pad="P$31"/>
<connect gate="G$1" pin="SO1" pad="P$25"/>
<connect gate="G$1" pin="SO2" pad="P$26"/>
<connect gate="G$1" pin="SP1" pad="P$32"/>
<connect gate="G$1" pin="SP2" pad="P$30"/>
<connect gate="G$1" pin="SS_TR" pad="P$56"/>
<connect gate="G$1" pin="VSENSE" pad="P$3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="R_SHUNT" prefix="R" uservalue="yes">
<description>&lt;b&gt;Resistor&lt;/b&gt;
Basic schematic elements and footprints for 0603, 1206, and PTH resistors.</description>
<gates>
<gate name="G$1" symbol="R_SHUNT" x="2.54" y="0"/>
</gates>
<devices>
<device name="" package="WSL2726">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="B" pad="B"/>
<connect gate="G$1" pin="SA" pad="SA"/>
<connect gate="G$1" pin="SB" pad="SB"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2512" package="R2512">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="B" pad="B"/>
<connect gate="G$1" pin="SA" pad="SA"/>
<connect gate="G$1" pin="SB" pad="SB"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="NFET-TPW4R008NH" prefix="Q">
<gates>
<gate name="1" symbol="NFET-TPW4R008NH" x="0" y="0"/>
</gates>
<devices>
<device name="" package="DSOP-ADVANCE">
<connects>
<connect gate="1" pin="D" pad="P$5"/>
<connect gate="1" pin="G" pad="P$4"/>
<connect gate="1" pin="S" pad="P$1 P$2 P$3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SL-LINK" prefix="J">
<gates>
<gate name="1" symbol="SL-LINK" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SL-LINK">
<connects>
<connect gate="1" pin="P$1" pad="1"/>
<connect gate="1" pin="P$2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Connectors">
<description>&lt;h3&gt;SparkFun Connectors&lt;/h3&gt;
This library contains electrically-functional connectors. 
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="1X02">
<description>&lt;h3&gt;Plated Through Hole&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:2&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.635" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="1.143" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.143" diameter="1.8796" rot="R90"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<text x="-1.27" y="1.397" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.032" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="MOLEX-1X2">
<description>&lt;h3&gt;Molex 2-Pin Plated Through-Hole&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:2&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/2pin_molex_set_19iv10.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1.27" y1="3.048" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="3.81" y1="3.048" x2="3.81" y2="-2.54" width="0.127" layer="21"/>
<wire x1="3.81" y1="3.048" x2="-1.27" y2="3.048" width="0.127" layer="21"/>
<wire x1="3.81" y1="-2.54" x2="2.54" y2="-2.54" width="0.127" layer="21"/>
<wire x1="2.54" y1="-2.54" x2="0" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0" y1="-1.27" x2="2.54" y2="-1.27" width="0.127" layer="21"/>
<wire x1="2.54" y1="-1.27" x2="2.54" y2="-2.54" width="0.127" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796"/>
<text x="-1.27" y="3.302" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.794" size="0.6096" layer="27" font="vector" ratio="20" align="top-left">&gt;VALUE</text>
</package>
<package name="SCREWTERMINAL-3.5MM-2">
<description>&lt;h3&gt;Screw Terminal  3.5mm Pitch - 2 Pin PTH&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch: 3.5mm/138mil&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/Screw-Terminal-3.5mm.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<circle x="2" y="3" radius="0.2828" width="0.127" layer="51"/>
<wire x1="-1.75" y1="3.4" x2="5.25" y2="3.4" width="0.2032" layer="21"/>
<wire x1="5.25" y1="3.4" x2="5.25" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="5.25" y1="-2.8" x2="5.25" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="5.25" y1="-3.6" x2="-1.75" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-3.6" x2="-1.75" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-2.8" x2="-1.75" y2="3.4" width="0.2032" layer="21"/>
<wire x1="5.25" y1="-2.8" x2="-1.75" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-1.35" x2="-2.25" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-2.25" y1="-1.35" x2="-2.25" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-2.25" y1="-2.35" x2="-1.75" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="5.25" y1="3.15" x2="5.75" y2="3.15" width="0.2032" layer="51"/>
<wire x1="5.75" y1="3.15" x2="5.75" y2="2.15" width="0.2032" layer="51"/>
<wire x1="5.75" y1="2.15" x2="5.25" y2="2.15" width="0.2032" layer="51"/>
<pad name="1" x="0" y="0" drill="1.2" diameter="2.032" shape="square"/>
<pad name="2" x="3.5" y="0" drill="1.2" diameter="2.032"/>
<text x="-1.27" y="2.54" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="1.27" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="JST-2-SMD">
<description>&lt;h3&gt;JST-Right Angle Male Header SMT&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch: 2mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”http://www.4uconnector.com/online/object/4udrawing/20404.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;li&gt;JST_2MM_MALE&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-4" y1="-1" x2="-4" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="-4" y1="-4.5" x2="-3.2" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="-3.2" y1="-4.5" x2="-3.2" y2="-2" width="0.2032" layer="21"/>
<wire x1="-3.2" y1="-2" x2="-2" y2="-2" width="0.2032" layer="21"/>
<wire x1="2" y1="-2" x2="3.2" y2="-2" width="0.2032" layer="21"/>
<wire x1="3.2" y1="-2" x2="3.2" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="3.2" y1="-4.5" x2="4" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="4" y1="-4.5" x2="4" y2="-1" width="0.2032" layer="21"/>
<wire x1="2" y1="3" x2="-2" y2="3" width="0.2032" layer="21"/>
<smd name="1" x="-1" y="-3.7" dx="1" dy="4.6" layer="1"/>
<smd name="2" x="1" y="-3.7" dx="1" dy="4.6" layer="1"/>
<smd name="NC1" x="-3.4" y="1.5" dx="3.4" dy="1.6" layer="1" rot="R90"/>
<smd name="NC2" x="3.4" y="1.5" dx="3.4" dy="1.6" layer="1" rot="R90"/>
<text x="-1.397" y="1.778" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.651" y="0.635" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X02_BIG">
<description>&lt;h3&gt;Plated Through Hole&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:2&lt;/li&gt;
&lt;li&gt;Pin pitch:0.15"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="5.08" y2="-1.27" width="0.127" layer="21"/>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.127" layer="21"/>
<wire x1="5.08" y1="1.27" x2="-1.27" y2="1.27" width="0.127" layer="21"/>
<pad name="P$1" x="0" y="0" drill="1.0668"/>
<pad name="P$2" x="3.81" y="0" drill="1.0668"/>
<text x="-1.27" y="1.397" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.032" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="JST-2-SMD-VERT">
<description>&lt;h3&gt;JST-Vertical Male Header SMT &lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch: 2mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”http://www.4uconnector.com/online/object/4udrawing/20404.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-4.1" y1="2.97" x2="4.2" y2="2.97" width="0.2032" layer="51"/>
<wire x1="4.2" y1="2.97" x2="4.2" y2="-2.13" width="0.2032" layer="51"/>
<wire x1="4.2" y1="-2.13" x2="-4.1" y2="-2.13" width="0.2032" layer="51"/>
<wire x1="-4.1" y1="-2.13" x2="-4.1" y2="2.97" width="0.2032" layer="51"/>
<wire x1="-4.1" y1="3" x2="4.2" y2="3" width="0.2032" layer="21"/>
<wire x1="4.2" y1="3" x2="4.2" y2="2.3" width="0.2032" layer="21"/>
<wire x1="-4.1" y1="3" x2="-4.1" y2="2.3" width="0.2032" layer="21"/>
<wire x1="2" y1="-2.1" x2="4.2" y2="-2.1" width="0.2032" layer="21"/>
<wire x1="4.2" y1="-2.1" x2="4.2" y2="-1.7" width="0.2032" layer="21"/>
<wire x1="-2" y1="-2.1" x2="-4.1" y2="-2.1" width="0.2032" layer="21"/>
<wire x1="-4.1" y1="-2.1" x2="-4.1" y2="-1.8" width="0.2032" layer="21"/>
<smd name="P$1" x="-3.4" y="0.27" dx="3" dy="1.6" layer="1" rot="R90"/>
<smd name="P$2" x="3.4" y="0.27" dx="3" dy="1.6" layer="1" rot="R90"/>
<smd name="VCC" x="-1" y="-2" dx="1" dy="5.5" layer="1"/>
<smd name="GND" x="1" y="-2" dx="1" dy="5.5" layer="1"/>
<text x="-3.81" y="3.302" size="0.6096" layer="25" font="vector" ratio="20">&gt;Name</text>
<text x="-3.81" y="2.21" size="0.6096" layer="27" font="vector" ratio="20">&gt;Value</text>
</package>
<package name="SCREWTERMINAL-5MM-2">
<description>&lt;h3&gt;Screw Terminal  5mm Pitch -2 Pin PTH&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch: 5mm/197mil&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/Screw-Terminal-5mm.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-3.1" y1="4.2" x2="8.1" y2="4.2" width="0.2032" layer="21"/>
<wire x1="8.1" y1="4.2" x2="8.1" y2="-2.3" width="0.2032" layer="21"/>
<wire x1="8.1" y1="-2.3" x2="8.1" y2="-3.3" width="0.2032" layer="21"/>
<wire x1="8.1" y1="-3.3" x2="-3.1" y2="-3.3" width="0.2032" layer="21"/>
<wire x1="-3.1" y1="-3.3" x2="-3.1" y2="-2.3" width="0.2032" layer="21"/>
<wire x1="-3.1" y1="-2.3" x2="-3.1" y2="4.2" width="0.2032" layer="21"/>
<wire x1="8.1" y1="-2.3" x2="-3.1" y2="-2.3" width="0.2032" layer="21"/>
<wire x1="-3.1" y1="-1.35" x2="-3.7" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-3.7" y1="-1.35" x2="-3.7" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-3.7" y1="-2.35" x2="-3.1" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="8.1" y1="4" x2="8.7" y2="4" width="0.2032" layer="51"/>
<wire x1="8.7" y1="4" x2="8.7" y2="3" width="0.2032" layer="51"/>
<wire x1="8.7" y1="3" x2="8.1" y2="3" width="0.2032" layer="51"/>
<circle x="2.5" y="3.7" radius="0.2828" width="0.127" layer="51"/>
<pad name="1" x="0" y="0" drill="1.3" diameter="2.032" shape="square"/>
<pad name="2" x="5" y="0" drill="1.3" diameter="2.032"/>
<text x="-1.27" y="2.54" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="1.27" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X02_LOCK">
<description>&lt;h3&gt;Plated Through Hole - Locking Footprint&lt;/h3&gt;
Holes are staggered by 0.005" from center to hold pins while soldering. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:2&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.635" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="-0.1778" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.7178" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<rectangle x1="-0.2921" y1="-0.2921" x2="0.2921" y2="0.2921" layer="51"/>
<rectangle x1="2.2479" y1="-0.2921" x2="2.8321" y2="0.2921" layer="51"/>
<text x="-1.27" y="1.397" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.032" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="MOLEX-1X2_LOCK">
<description>&lt;h3&gt;Molex 2-Pin Plated Through-Hole Locking Footprint&lt;/h3&gt;
Holes are offset from center by 0.005" to hold pins in place during soldering. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:2&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/2pin_molex_set_19iv10.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1.27" y1="3.048" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="3.81" y1="3.048" x2="3.81" y2="-2.54" width="0.127" layer="21"/>
<wire x1="3.81" y1="3.048" x2="-1.27" y2="3.048" width="0.127" layer="21"/>
<wire x1="3.81" y1="-2.54" x2="2.54" y2="-2.54" width="0.127" layer="21"/>
<wire x1="2.54" y1="-2.54" x2="0" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.27" width="0.127" layer="21"/>
<wire x1="0" y1="-1.27" x2="2.54" y2="-1.27" width="0.127" layer="21"/>
<wire x1="2.54" y1="-1.27" x2="2.54" y2="-2.54" width="0.127" layer="21"/>
<pad name="1" x="-0.127" y="0" drill="1.016" diameter="1.8796" shape="square"/>
<pad name="2" x="2.667" y="0" drill="1.016" diameter="1.8796"/>
<rectangle x1="-0.2921" y1="-0.2921" x2="0.2921" y2="0.2921" layer="51"/>
<rectangle x1="2.2479" y1="-0.2921" x2="2.8321" y2="0.2921" layer="51"/>
<text x="-1.27" y="3.302" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.794" size="0.6096" layer="27" font="vector" ratio="20" align="top-left">&gt;VALUE</text>
</package>
<package name="1X02_LOCK_LONGPADS">
<description>&lt;h3&gt;Plated Through Hole - Long Pads with Locking Footprint&lt;/h3&gt;
Pins are staggered by 0.005" from center to hold pins in place while soldering. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:2&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="1.651" y1="0" x2="0.889" y2="0" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.016" y2="0" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="0.9906" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.9906" x2="-0.9906" y2="1.27" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-0.9906" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.9906" x2="-0.9906" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0" x2="3.556" y2="0" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0" x2="3.81" y2="-0.9906" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.9906" x2="3.5306" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0" x2="3.81" y2="0.9906" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.9906" x2="3.5306" y2="1.27" width="0.2032" layer="21"/>
<pad name="1" x="-0.127" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="2.667" y="0" drill="1.016" shape="long" rot="R90"/>
<rectangle x1="-0.2921" y1="-0.2921" x2="0.2921" y2="0.2921" layer="51"/>
<rectangle x1="2.2479" y1="-0.2921" x2="2.8321" y2="0.2921" layer="51"/>
<text x="-1.27" y="1.651" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.286" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="SCREWTERMINAL-3.5MM-2_LOCK">
<description>&lt;h3&gt;Screw Terminal  3.5mm Pitch - 2 Pin PTH Locking&lt;/h3&gt;
Holes are offset from center 0.005" to hold pins in place during soldering. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch: 3.5mm/138mil&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/Screw-Terminal-3.5mm.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1.75" y1="3.4" x2="5.25" y2="3.4" width="0.2032" layer="21"/>
<wire x1="5.25" y1="3.4" x2="5.25" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="5.25" y1="-2.8" x2="5.25" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="5.25" y1="-3.6" x2="-1.75" y2="-3.6" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-3.6" x2="-1.75" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-2.8" x2="-1.75" y2="3.4" width="0.2032" layer="21"/>
<wire x1="5.25" y1="-2.8" x2="-1.75" y2="-2.8" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-1.35" x2="-2.15" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-2.15" y1="-1.35" x2="-2.15" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-2.15" y1="-2.35" x2="-1.75" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="5.25" y1="3.15" x2="5.65" y2="3.15" width="0.2032" layer="51"/>
<wire x1="5.65" y1="3.15" x2="5.65" y2="2.15" width="0.2032" layer="51"/>
<wire x1="5.65" y1="2.15" x2="5.25" y2="2.15" width="0.2032" layer="51"/>
<circle x="2" y="3" radius="0.2828" width="0.127" layer="51"/>
<circle x="0" y="0" radius="0.4318" width="0.0254" layer="51"/>
<circle x="3.5" y="0" radius="0.4318" width="0.0254" layer="51"/>
<pad name="1" x="-0.1778" y="0" drill="1.2" diameter="2.032" shape="square"/>
<pad name="2" x="3.6778" y="0" drill="1.2" diameter="2.032"/>
<text x="-1.27" y="2.54" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="1.27" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X02_LONGPADS">
<description>&lt;h3&gt;Plated Through Hole - Long Pads without Silk Outline&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:2&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<pad name="1" x="0" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.1176" diameter="1.8796" shape="long" rot="R90"/>
<text x="-1.27" y="2.032" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.397" y="-2.667" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X02_NO_SILK">
<description>&lt;h3&gt;Plated Through Hole - No Silk Outline&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:2&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.016" diameter="1.8796" rot="R90"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<text x="-1.27" y="1.397" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.032" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="JST-2-PTH">
<description>&lt;h3&gt;JST 2 Pin Right Angle Plated Through  Hole&lt;/h3&gt;
tDocu indicate polarity for connections that match SparkFun LiPo battery terminations. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch:2mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/Connectors/JST%282%29-01548.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<pad name="1" x="-1" y="0" drill="0.7" diameter="1.6"/>
<pad name="2" x="1" y="0" drill="0.7" diameter="1.6"/>
<text x="-1.27" y="5.27" size="0.6096" layer="25" font="vector" ratio="20">&gt;Name</text>
<text x="-1.27" y="2.73" size="0.6096" layer="27" font="vector" ratio="20">&gt;Value</text>
<text x="0.6" y="0.7" size="1.27" layer="51">+</text>
<text x="-1.4" y="0.7" size="1.27" layer="51">-</text>
<wire x1="-2.95" y1="-1.6" x2="-2.95" y2="6" width="0.2032" layer="21"/>
<wire x1="-2.95" y1="6" x2="2.95" y2="6" width="0.2032" layer="21"/>
<wire x1="2.95" y1="6" x2="2.95" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="-2.95" y1="-1.6" x2="-2.3" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="2.95" y1="-1.6" x2="2.3" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-1.6" x2="-2.3" y2="0" width="0.2032" layer="21"/>
<wire x1="2.3" y1="-1.6" x2="2.3" y2="0" width="0.2032" layer="21"/>
</package>
<package name="1X02_XTRA_BIG">
<description>&lt;h3&gt;Plated Through Hole - 0.1" holes&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:2&lt;/li&gt;
&lt;li&gt;Pin pitch:0.2"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-5.08" y1="2.54" x2="-5.08" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-5.08" y1="-2.54" x2="5.08" y2="-2.54" width="0.127" layer="21"/>
<wire x1="5.08" y1="-2.54" x2="5.08" y2="2.54" width="0.127" layer="21"/>
<wire x1="5.08" y1="2.54" x2="-5.08" y2="2.54" width="0.127" layer="21"/>
<pad name="1" x="-2.54" y="0" drill="2.0574" diameter="3.556"/>
<pad name="2" x="2.54" y="0" drill="2.0574" diameter="3.556"/>
<text x="-5.08" y="2.667" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-5.08" y="-3.302" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X02_PP_HOLES_ONLY">
<description>&lt;h3&gt;Pogo Pins Connector - No Silk Outline&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:2&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<circle x="0" y="0" radius="0.635" width="0.127" layer="51"/>
<circle x="2.54" y="0" radius="0.635" width="0.127" layer="51"/>
<pad name="1" x="0" y="0" drill="0.889" diameter="0.8128" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="0.889" diameter="0.8128" rot="R90"/>
<hole x="0" y="0" drill="1.4732"/>
<hole x="2.54" y="0" drill="1.4732"/>
<text x="-1.27" y="1.143" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-1.778" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="SCREWTERMINAL-3.5MM-2-NS">
<description>&lt;h3&gt;Screw Terminal  3.5mm Pitch - 2 Pin PTH No Silk Outline&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch: 3.5mm/138mil&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/Screw-Terminal-3.5mm.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1.75" y1="3.4" x2="5.25" y2="3.4" width="0.2032" layer="51"/>
<wire x1="5.25" y1="3.4" x2="5.25" y2="-2.8" width="0.2032" layer="51"/>
<wire x1="5.25" y1="-2.8" x2="5.25" y2="-3.6" width="0.2032" layer="51"/>
<wire x1="5.25" y1="-3.6" x2="-1.75" y2="-3.6" width="0.2032" layer="51"/>
<wire x1="-1.75" y1="-3.6" x2="-1.75" y2="-2.8" width="0.2032" layer="51"/>
<wire x1="-1.75" y1="-2.8" x2="-1.75" y2="3.4" width="0.2032" layer="51"/>
<wire x1="5.25" y1="-2.8" x2="-1.75" y2="-2.8" width="0.2032" layer="51"/>
<wire x1="-1.75" y1="-1.35" x2="-2.15" y2="-1.35" width="0.2032" layer="51"/>
<wire x1="-2.15" y1="-1.35" x2="-2.15" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="-2.15" y1="-2.35" x2="-1.75" y2="-2.35" width="0.2032" layer="51"/>
<wire x1="5.25" y1="3.15" x2="5.65" y2="3.15" width="0.2032" layer="51"/>
<wire x1="5.65" y1="3.15" x2="5.65" y2="2.15" width="0.2032" layer="51"/>
<wire x1="5.65" y1="2.15" x2="5.25" y2="2.15" width="0.2032" layer="51"/>
<circle x="2" y="3" radius="0.2828" width="0.127" layer="51"/>
<pad name="1" x="0" y="0" drill="1.2" diameter="2.032" shape="square"/>
<pad name="2" x="3.5" y="0" drill="1.2" diameter="2.032"/>
<text x="-1.27" y="2.54" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="1.27" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="JST-2-PTH-NS">
<description>&lt;h3&gt;JST 2 Pin Right Angle Plated Through  Hole- No Silk&lt;/h3&gt;
tDocu indicate polarity for connections that match SparkFun LiPo battery terminations. 
&lt;br&gt; No silk outline of connector. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch:2mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/Connectors/JST%282%29-01548.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-2" y1="0" x2="-2" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="-2" y1="-1.8" x2="-3" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="-3" y1="-1.8" x2="-3" y2="6" width="0.2032" layer="51"/>
<wire x1="-3" y1="6" x2="3" y2="6" width="0.2032" layer="51"/>
<wire x1="3" y1="6" x2="3" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="3" y1="-1.8" x2="2" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="2" y1="-1.8" x2="2" y2="0" width="0.2032" layer="51"/>
<pad name="1" x="-1" y="0" drill="0.7" diameter="1.6"/>
<pad name="2" x="1" y="0" drill="0.7" diameter="1.6"/>
<text x="-1.27" y="5.27" size="0.6096" layer="25" font="vector" ratio="20">&gt;Name</text>
<text x="-1.27" y="4" size="0.6096" layer="27" font="vector" ratio="20">&gt;Value</text>
<text x="0.6" y="0.7" size="1.27" layer="51">+</text>
<text x="-1.4" y="0.7" size="1.27" layer="51">-</text>
</package>
<package name="JST-2-PTH-KIT">
<description>&lt;h3&gt;JST 2 Pin Right Angle Plated Through  Hole - KIT&lt;/h3&gt;
tDocu indicate polarity for connections that match SparkFun LiPo battery terminations. 
&lt;br&gt; This package has a smaller diameter top stop mask, which doesn't cover the diameter of the pad.
&lt;br&gt; This means only the bottom side of the pads' copper will be exposed. You'll only be able to solder to the bottom side.
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch:2mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/Connectors/JST%282%29-01548.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-2" y1="0" x2="-2" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="-2" y1="-1.8" x2="-3" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="-3" y1="-1.8" x2="-3" y2="6" width="0.2032" layer="51"/>
<wire x1="-3" y1="6" x2="3" y2="6" width="0.2032" layer="51"/>
<wire x1="3" y1="6" x2="3" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="3" y1="-1.8" x2="2" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="2" y1="-1.8" x2="2" y2="0" width="0.2032" layer="51"/>
<pad name="1" x="-1" y="0" drill="0.7" diameter="1.4478" stop="no"/>
<pad name="2" x="1" y="0" drill="0.7" diameter="1.4478" stop="no"/>
<text x="-1.27" y="5.27" size="0.6096" layer="25" font="vector" ratio="20">&gt;Name</text>
<text x="-1.27" y="4" size="0.6096" layer="27" font="vector" ratio="20">&gt;Value</text>
<text x="0.6" y="0.7" size="1.27" layer="51">+</text>
<text x="-1.4" y="0.7" size="1.27" layer="51">-</text>
<polygon width="0.127" layer="30">
<vertex x="-0.9975" y="-0.6604" curve="-90.025935"/>
<vertex x="-1.6604" y="0" curve="-90.017354"/>
<vertex x="-1" y="0.6604" curve="-90"/>
<vertex x="-0.3396" y="0" curve="-90.078137"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="-1" y="-0.2865" curve="-90.08005"/>
<vertex x="-1.2865" y="0" curve="-90.040011"/>
<vertex x="-1" y="0.2865" curve="-90"/>
<vertex x="-0.7135" y="0" curve="-90"/>
</polygon>
<polygon width="0.127" layer="30">
<vertex x="1.0025" y="-0.6604" curve="-90.025935"/>
<vertex x="0.3396" y="0" curve="-90.017354"/>
<vertex x="1" y="0.6604" curve="-90"/>
<vertex x="1.6604" y="0" curve="-90.078137"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="1" y="-0.2865" curve="-90.08005"/>
<vertex x="0.7135" y="0" curve="-90.040011"/>
<vertex x="1" y="0.2865" curve="-90"/>
<vertex x="1.2865" y="0" curve="-90"/>
</polygon>
</package>
<package name="SPRINGTERMINAL-2.54MM-2">
<description>&lt;h3&gt;Spring Terminal- PCB Mount 2 Pin PTH&lt;/h3&gt;
tDocu marks the spring arms
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 4&lt;/li&gt;
&lt;li&gt;Pin pitch: 0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/SpringTerminal.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-4.2" y1="7.88" x2="-4.2" y2="-2.8" width="0.254" layer="21"/>
<wire x1="-4.2" y1="-2.8" x2="-4.2" y2="-4.72" width="0.254" layer="51"/>
<wire x1="-4.2" y1="-4.72" x2="3.44" y2="-4.72" width="0.254" layer="51"/>
<wire x1="3.44" y1="-4.72" x2="3.44" y2="-2.8" width="0.254" layer="51"/>
<wire x1="3.44" y1="7.88" x2="-4.2" y2="7.88" width="0.254" layer="21"/>
<wire x1="0" y1="0" x2="0" y2="5.08" width="0.254" layer="1"/>
<wire x1="0" y1="0" x2="0" y2="5.08" width="0.254" layer="16"/>
<wire x1="2.54" y1="0" x2="2.54" y2="5.08" width="0.254" layer="16"/>
<wire x1="2.54" y1="0" x2="2.54" y2="5.08" width="0.254" layer="1"/>
<wire x1="-4.2" y1="-2.8" x2="3.44" y2="-2.8" width="0.254" layer="21"/>
<wire x1="3.44" y1="4" x2="3.44" y2="1" width="0.254" layer="21"/>
<wire x1="3.44" y1="7.88" x2="3.44" y2="6" width="0.254" layer="21"/>
<wire x1="3.44" y1="-0.9" x2="3.44" y2="-2.8" width="0.254" layer="21"/>
<pad name="1" x="0" y="0" drill="1.1" diameter="1.9"/>
<pad name="P$2" x="0" y="5.08" drill="1.1" diameter="1.9"/>
<pad name="P$3" x="2.54" y="5.08" drill="1.1" diameter="1.9"/>
<pad name="2" x="2.54" y="0" drill="1.1" diameter="1.9"/>
</package>
<package name="1X02_2.54_SCREWTERM">
<description>&lt;h3&gt;2 Pin Screw Terminal - 2.54mm&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:2&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_02&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<pad name="P2" x="0" y="0" drill="1.016" shape="square"/>
<pad name="P1" x="2.54" y="0" drill="1.016" shape="square"/>
<wire x1="-1.5" y1="3.25" x2="4" y2="3.25" width="0.2032" layer="21"/>
<wire x1="4" y1="3.25" x2="4" y2="2.5" width="0.2032" layer="21"/>
<wire x1="4" y1="2.5" x2="4" y2="-3.25" width="0.2032" layer="21"/>
<wire x1="4" y1="-3.25" x2="-1.5" y2="-3.25" width="0.2032" layer="21"/>
<wire x1="-1.5" y1="-3.25" x2="-1.5" y2="2.5" width="0.2032" layer="21"/>
<wire x1="-1.5" y1="2.5" x2="-1.5" y2="3.25" width="0.2032" layer="21"/>
<wire x1="-1.5" y1="2.5" x2="4" y2="2.5" width="0.2032" layer="21"/>
<text x="-1.27" y="3.429" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-4.064" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="1X02_POKEHOME">
<description>2 pin poke-home connector

part number 2062-2P from STA</description>
<wire x1="-7" y1="-4" x2="-7" y2="2" width="0.2032" layer="21"/>
<wire x1="-7" y1="2" x2="-7" y2="4" width="0.2032" layer="21"/>
<wire x1="4.7" y1="4" x2="4.7" y2="-4" width="0.2032" layer="21"/>
<wire x1="4.7" y1="-4" x2="-7" y2="-4" width="0.2032" layer="21"/>
<smd name="P2" x="5.25" y="-2" dx="3.5" dy="2" layer="1"/>
<smd name="P1" x="5.25" y="2" dx="3.5" dy="2" layer="1"/>
<smd name="P4" x="-4" y="-2" dx="6" dy="2" layer="1"/>
<smd name="P3" x="-4" y="2" dx="6" dy="2" layer="1"/>
<wire x1="-7" y1="4" x2="4.7" y2="4" width="0.2032" layer="21"/>
<text x="0.635" y="-3.175" size="0.4064" layer="25">&gt;NAME</text>
<text x="0.635" y="-1.905" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="1X02_RA_PTH_FEMALE">
<wire x1="-2.79" y1="4.25" x2="-2.79" y2="-4.25" width="0.1778" layer="21"/>
<text x="-1.397" y="0.762" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.524" y="-1.27" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
<wire x1="2.79" y1="4.25" x2="2.79" y2="-4.25" width="0.1778" layer="21"/>
<wire x1="-2.79" y1="4.25" x2="2.79" y2="4.25" width="0.1778" layer="21"/>
<wire x1="-2.79" y1="-4.25" x2="2.79" y2="-4.25" width="0.1778" layer="21"/>
<pad name="2" x="-1.27" y="-5.85" drill="0.8"/>
<pad name="1" x="1.27" y="-5.85" drill="0.8"/>
</package>
</packages>
<symbols>
<symbol name="CONN_02">
<description>&lt;h3&gt;2 Pin Connection&lt;/h3&gt;</description>
<wire x1="3.81" y1="-2.54" x2="-2.54" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="-2.54" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-2.54" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<text x="-2.54" y="-4.826" size="1.778" layer="96" font="vector">&gt;VALUE</text>
<text x="-2.54" y="5.588" size="1.778" layer="95" font="vector">&gt;NAME</text>
<pin name="1" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="CONN_02" prefix="J" uservalue="yes">
<description>&lt;h3&gt;Multi connection point. Often used as Generic Header-pin footprint for 0.1 inch spaced/style header connections&lt;/h3&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt;On any of the 0.1 inch spaced packages, you can populate with these:&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/116"&gt; Break Away Headers - Straight&lt;/a&gt; (PRT-00116)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/553"&gt; Break Away Male Headers - Right Angle&lt;/a&gt; (PRT-00553)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/115"&gt; Female Headers&lt;/a&gt; (PRT-00115)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/117"&gt; Break Away Headers - Machine Pin&lt;/a&gt; (PRT-00117)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/743"&gt; Break Away Female Headers - Swiss Machine Pin&lt;/a&gt; (PRT-00743)&lt;/li&gt;
&lt;/ul&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt; For SCREWTERMINALS and SPRING TERMINALS visit here:&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/search/results?term=Screw+Terminals"&gt; Screw Terimnals on SparkFun.com&lt;/a&gt; (5mm/3.5mm/2.54mm spacing)&lt;/li&gt;
&lt;/ul&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt;This device is also useful as a general connection point to wire up your design to another part of your project. Our various solder wires solder well into these plated through hole pads.&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/11375"&gt; Hook-Up Wire - Assortment (Stranded, 22 AWG)&lt;/a&gt; (PRT-11375)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/11367"&gt; Hook-Up Wire - Assortment (Solid Core, 22 AWG)&lt;/a&gt; (PRT-11367)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/categories/141"&gt; View the entire wire category on our website here&lt;/a&gt;&lt;/li&gt;
&lt;p&gt;&lt;/p&gt;
&lt;/ul&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt;Special notes:&lt;/b&gt;

 Molex polarized connector foot print use with: PRT-08233 with associated crimp pins and housings.&lt;br&gt;&lt;br&gt;

2.54_SCREWTERM for use with  PRT-10571.&lt;br&gt;&lt;br&gt;

3.5mm Screw Terminal footprints for  PRT-08084&lt;br&gt;&lt;br&gt;

5mm Screw Terminal footprints for use with PRT-08432</description>
<gates>
<gate name="G$1" symbol="CONN_02" x="-2.54" y="0"/>
</gates>
<devices>
<device name="" package="1X02">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="POLAR" package="MOLEX-1X2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="SF_ID" value="PRT-09918" constant="no"/>
</technology>
</technologies>
</device>
<device name="3.5MM" package="SCREWTERMINAL-3.5MM-2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08399" constant="no"/>
</technology>
</technologies>
</device>
<device name="-JST-2MM-SMT" package="JST-2-SMD">
<connects>
<connect gate="G$1" pin="1" pad="2"/>
<connect gate="G$1" pin="2" pad="1"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-11443"/>
</technology>
</technologies>
</device>
<device name="PTH2" package="1X02_BIG">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="4UCON-15767" package="JST-2-SMD-VERT">
<connects>
<connect gate="G$1" pin="1" pad="GND"/>
<connect gate="G$1" pin="2" pad="VCC"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="5MM" package="SCREWTERMINAL-5MM-2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="SF_SKU" value="PRT-08432" constant="no"/>
</technology>
</technologies>
</device>
<device name="LOCK" package="1X02_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="POLAR_LOCK" package="MOLEX-1X2_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="SF_ID" value="PRT-09918" constant="no"/>
</technology>
</technologies>
</device>
<device name="LOCK_LONGPADS" package="1X02_LOCK_LONGPADS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3.5MM_LOCK" package="SCREWTERMINAL-3.5MM-2_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08399" constant="no"/>
</technology>
</technologies>
</device>
<device name="PTH3" package="1X02_LONGPADS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1X02_NO_SILK" package="1X02_NO_SILK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST-PTH-2" package="JST-2-PTH">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-09863" constant="no"/>
<attribute name="SKU" value="PRT-09914" constant="no"/>
</technology>
</technologies>
</device>
<device name="PTH4" package="1X02_XTRA_BIG">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="POGO_PIN_HOLES_ONLY" package="1X02_PP_HOLES_ONLY">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3.5MM-NO_SILK" package="SCREWTERMINAL-3.5MM-2-NS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08399" constant="no"/>
</technology>
</technologies>
</device>
<device name="-JST-2-PTH-NO_SILK" package="JST-2-PTH-NS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="JST-PTH-2-KIT" package="JST-2-PTH-KIT">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SPRING-2.54-RA" package="SPRINGTERMINAL-2.54MM-2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2.54MM_SCREWTERM" package="1X02_2.54_SCREWTERM">
<connects>
<connect gate="G$1" pin="1" pad="P1"/>
<connect gate="G$1" pin="2" pad="P2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMALL_POKEHOME" package="1X02_POKEHOME">
<connects>
<connect gate="G$1" pin="1" pad="P1 P3"/>
<connect gate="G$1" pin="2" pad="P2 P4"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-13512"/>
</technology>
</technologies>
</device>
<device name="PTH_RA_FEMALE" package="1X02_RA_PTH_FEMALE">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-13700"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="connector">
<packages>
<package name="FIDUCIAL_1MM">
<smd name="1" x="0" y="0" dx="1" dy="1" layer="1" roundness="100" stop="no" cream="no"/>
<polygon width="0.127" layer="29">
<vertex x="-1" y="0" curve="90"/>
<vertex x="0" y="-1" curve="90"/>
<vertex x="1" y="0" curve="90"/>
<vertex x="0" y="1" curve="90"/>
</polygon>
<polygon width="0.127" layer="41">
<vertex x="-1" y="0" curve="90"/>
<vertex x="0" y="-1" curve="90"/>
<vertex x="1" y="0" curve="90"/>
<vertex x="0" y="1" curve="90"/>
</polygon>
<circle x="0" y="0" radius="0.4953" width="0" layer="51"/>
</package>
<package name="FIDUCIAL_RECT_1MM">
<smd name="P$1" x="0" y="0" dx="1" dy="1" layer="1"/>
<rectangle x1="-1" y1="-1" x2="1" y2="1" layer="39"/>
<rectangle x1="-1" y1="-1" x2="1" y2="1" layer="29"/>
<rectangle x1="-1" y1="-1" x2="1" y2="1" layer="41"/>
</package>
<package name="MINI-PCIE-ATK-MOTHER">
<smd name="15" x="-6.2" y="1.4" dx="0.55" dy="2.3" layer="1" cream="no"/>
<smd name="13" x="-7" y="1.4" dx="0.55" dy="2.3" layer="1" cream="no"/>
<smd name="11" x="-7.8" y="1.4" dx="0.55" dy="2.3" layer="1" cream="no"/>
<smd name="9" x="-8.6" y="1.4" dx="0.55" dy="2.3" layer="1" cream="no"/>
<smd name="7" x="-9.4" y="1.4" dx="0.55" dy="2.3" layer="1" cream="no"/>
<smd name="5" x="-10.2" y="1.4" dx="0.55" dy="2.3" layer="1" cream="no"/>
<smd name="3" x="-11" y="1.4" dx="0.55" dy="2.3" layer="1" cream="no"/>
<smd name="1" x="-11.8" y="1.4" dx="0.55" dy="2.3" layer="1" cream="no"/>
<smd name="17" x="-2.2" y="1.4" dx="0.55" dy="2.3" layer="1" cream="no"/>
<smd name="19" x="-1.4" y="1.4" dx="0.55" dy="2.3" layer="1" cream="no"/>
<smd name="21" x="-0.6" y="1.4" dx="0.55" dy="2.3" layer="1" cream="no"/>
<smd name="23" x="0.2" y="1.4" dx="0.55" dy="2.3" layer="1" cream="no"/>
<smd name="25" x="1" y="1.4" dx="0.55" dy="2.3" layer="1" cream="no"/>
<smd name="27" x="1.8" y="1.4" dx="0.55" dy="2.3" layer="1" cream="no"/>
<smd name="29" x="2.6" y="1.4" dx="0.55" dy="2.3" layer="1" cream="no"/>
<smd name="31" x="3.4" y="1.4" dx="0.55" dy="2.3" layer="1" cream="no"/>
<smd name="33" x="4.2" y="1.4" dx="0.55" dy="2.3" layer="1" cream="no"/>
<smd name="35" x="5" y="1.4" dx="0.55" dy="2.3" layer="1" cream="no"/>
<smd name="37" x="5.8" y="1.4" dx="0.55" dy="2.3" layer="1" cream="no"/>
<smd name="39" x="6.6" y="1.4" dx="0.55" dy="2.3" layer="1" cream="no"/>
<smd name="41" x="7.4" y="1.4" dx="0.55" dy="2.3" layer="1" cream="no"/>
<smd name="43" x="8.2" y="1.4" dx="0.55" dy="2.3" layer="1" cream="no"/>
<smd name="45" x="9" y="1.4" dx="0.55" dy="2.3" layer="1" cream="no"/>
<smd name="47" x="9.8" y="1.4" dx="0.55" dy="2.3" layer="1" cream="no"/>
<smd name="49" x="10.6" y="1.4" dx="0.55" dy="2.3" layer="1" cream="no"/>
<smd name="51" x="11.4" y="1.4" dx="0.55" dy="2.3" layer="1" cream="no"/>
<smd name="16" x="-5.8" y="1.4" dx="0.55" dy="2.3" layer="16" rot="R180" cream="no"/>
<wire x1="-4.6" y1="0" x2="-4.6" y2="3.25" width="0.127" layer="51"/>
<wire x1="-3.1" y1="3.25" x2="-3.1" y2="0" width="0.127" layer="51"/>
<smd name="14" x="-6.6" y="1.4" dx="0.55" dy="2.3" layer="16" rot="R180" cream="no"/>
<smd name="12" x="-7.4" y="1.4" dx="0.55" dy="2.3" layer="16" rot="R180" cream="no"/>
<smd name="10" x="-8.2" y="1.4" dx="0.55" dy="2.3" layer="16" rot="R180" cream="no"/>
<smd name="8" x="-9" y="1.4" dx="0.55" dy="2.3" layer="16" rot="R180" cream="no"/>
<smd name="6" x="-9.8" y="1.4" dx="0.55" dy="2.3" layer="16" rot="R180" cream="no"/>
<smd name="4" x="-10.6" y="1.4" dx="0.55" dy="2.3" layer="16" rot="R180" cream="no"/>
<smd name="2" x="-11.4" y="1.4" dx="0.55" dy="2.3" layer="16" rot="R180" cream="no"/>
<smd name="18" x="-1.8" y="1.4" dx="0.55" dy="2.3" layer="16" rot="R180" cream="no"/>
<smd name="20" x="-1" y="1.4" dx="0.55" dy="2.3" layer="16" rot="R180" cream="no"/>
<smd name="22" x="-0.2" y="1.4" dx="0.55" dy="2.3" layer="16" rot="R180" cream="no"/>
<smd name="24" x="0.6" y="1.4" dx="0.55" dy="2.3" layer="16" rot="R180" cream="no"/>
<smd name="26" x="1.4" y="1.4" dx="0.55" dy="2.3" layer="16" rot="R180" cream="no"/>
<smd name="28" x="2.2" y="1.4" dx="0.55" dy="2.3" layer="16" rot="R180" cream="no"/>
<smd name="30" x="3" y="1.4" dx="0.55" dy="2.3" layer="16" rot="R180" cream="no"/>
<smd name="32" x="3.8" y="1.4" dx="0.55" dy="2.3" layer="16" rot="R180" cream="no"/>
<smd name="34" x="4.6" y="1.4" dx="0.55" dy="2.3" layer="16" rot="R180" cream="no"/>
<smd name="36" x="5.4" y="1.4" dx="0.55" dy="2.3" layer="16" rot="R180" cream="no"/>
<smd name="38" x="6.2" y="1.4" dx="0.55" dy="2.3" layer="16" rot="R180" cream="no"/>
<smd name="40" x="7" y="1.4" dx="0.55" dy="2.3" layer="16" rot="R180" cream="no"/>
<smd name="42" x="7.8" y="1.4" dx="0.55" dy="2.3" layer="16" rot="R180" cream="no"/>
<smd name="44" x="8.6" y="1.4" dx="0.55" dy="2.3" layer="16" rot="R180" cream="no"/>
<smd name="46" x="9.4" y="1.4" dx="0.55" dy="2.3" layer="16" rot="R180" cream="no"/>
<smd name="48" x="10.2" y="1.4" dx="0.55" dy="2.3" layer="16" rot="R180" cream="no"/>
<smd name="50" x="11" y="1.4" dx="0.55" dy="2.3" layer="16" rot="R180" cream="no"/>
<smd name="52" x="11.8" y="1.4" dx="0.55" dy="2.3" layer="16" rot="R180" cream="no"/>
<wire x1="-3.1" y1="0" x2="12.85" y2="0" width="0.127" layer="51"/>
<wire x1="12.85" y1="0" x2="12.85" y2="3.25" width="0.127" layer="51"/>
<wire x1="15" y1="4" x2="13.6" y2="4" width="0.127" layer="51"/>
<wire x1="-13.6" y1="4" x2="-15" y2="4" width="0.127" layer="51"/>
<wire x1="-12.85" y1="3.25" x2="-13.6" y2="4" width="0.127" layer="51" curve="90"/>
<wire x1="12.85" y1="3.25" x2="13.6" y2="4" width="0.127" layer="51" curve="-90"/>
<wire x1="-3.1" y1="3.25" x2="-3.85" y2="4" width="0.127" layer="51" curve="90"/>
<wire x1="-4.6" y1="3.25" x2="-3.85" y2="4" width="0.127" layer="51" curve="-90"/>
<wire x1="-12.85" y1="3.25" x2="-12.85" y2="0" width="0.127" layer="51"/>
<wire x1="-12.85" y1="0" x2="-4.6" y2="0" width="0.127" layer="51"/>
<wire x1="-15" y1="-5.1" x2="15" y2="-5.1" width="0.127" layer="51"/>
<wire x1="15" y1="-5.1" x2="15" y2="-3.5" width="0.127" layer="51"/>
<wire x1="15" y1="-3.5" x2="15" y2="3.5" width="0.127" layer="51"/>
<wire x1="15" y1="3.5" x2="15" y2="5.1" width="0.127" layer="51"/>
<wire x1="15" y1="5.1" x2="-15" y2="5.1" width="0.127" layer="51"/>
<wire x1="-15" y1="5.1" x2="-15" y2="3.5" width="0.127" layer="51"/>
<wire x1="-15" y1="3.5" x2="-15" y2="-3.5" width="0.127" layer="51"/>
<wire x1="-15" y1="-3.5" x2="-15" y2="-5.1" width="0.127" layer="51"/>
<wire x1="-15" y1="-3.5" x2="15" y2="-3.5" width="0.127" layer="51"/>
<wire x1="-15" y1="3.5" x2="15" y2="3.5" width="0.127" layer="51"/>
<wire x1="-15.8" y1="-2.5" x2="15.8" y2="-2.5" width="0.127" layer="51"/>
<rectangle x1="-15.9" y1="-5.1" x2="-13.8" y2="-2.1" layer="51"/>
<rectangle x1="13.8" y1="-5.1" x2="15.9" y2="-2.1" layer="51"/>
<pad name="P$1" x="16.5" y="8.5" drill="4.5" diameter="6.5"/>
<pad name="P$2" x="-16.5" y="8.5" drill="4.5" diameter="6.5"/>
<polygon width="0.127" layer="31">
<vertex x="-16.8" y="11.6"/>
<vertex x="-16.8" y="11.1" curve="-73.739933"/>
<vertex x="-17" y="10.8" curve="60.752764"/>
<vertex x="-18.8" y="8.9" curve="-80.286401"/>
<vertex x="-19.2" y="8.7" curve="6.546325"/>
<vertex x="-19.6" y="8.7" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-16.2" y="5.4"/>
<vertex x="-16.2" y="5.9" curve="-73.739933"/>
<vertex x="-16" y="6.2" curve="60.752764"/>
<vertex x="-14.2" y="8" curve="-80.286401"/>
<vertex x="-13.8" y="8.3" curve="6.546325"/>
<vertex x="-13.4" y="8.3" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-19.6" y="8.2"/>
<vertex x="-19.1" y="8.2" curve="-73.739933"/>
<vertex x="-18.8" y="8" curve="60.752764"/>
<vertex x="-17" y="6.2" curve="-80.286401"/>
<vertex x="-16.7" y="5.8" curve="6.546325"/>
<vertex x="-16.7" y="5.4" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-13.4" y="8.8"/>
<vertex x="-13.9" y="8.8" curve="-73.739933"/>
<vertex x="-14.2" y="9" curve="60.752764"/>
<vertex x="-16.1" y="10.8" curve="-80.286401"/>
<vertex x="-16.3" y="11.1" curve="6.546325"/>
<vertex x="-16.3" y="11.6" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="16.2" y="11.6"/>
<vertex x="16.2" y="11.1" curve="-73.739933"/>
<vertex x="16" y="10.8" curve="60.752764"/>
<vertex x="14.2" y="8.9" curve="-80.286401"/>
<vertex x="13.8" y="8.7" curve="6.546325"/>
<vertex x="13.4" y="8.7" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="16.8" y="5.4"/>
<vertex x="16.8" y="5.9" curve="-73.739933"/>
<vertex x="17" y="6.2" curve="60.752764"/>
<vertex x="18.8" y="8" curve="-80.286401"/>
<vertex x="19.2" y="8.3" curve="6.546325"/>
<vertex x="19.6" y="8.3" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="13.4" y="8.2"/>
<vertex x="13.9" y="8.2" curve="-73.739933"/>
<vertex x="14.2" y="8" curve="60.752764"/>
<vertex x="16" y="6.2" curve="-80.286401"/>
<vertex x="16.3" y="5.8" curve="6.546325"/>
<vertex x="16.3" y="5.4" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="19.6" y="8.8"/>
<vertex x="19.1" y="8.8" curve="-73.739933"/>
<vertex x="18.8" y="9" curve="60.752764"/>
<vertex x="16.9" y="10.8" curve="-80.286401"/>
<vertex x="16.7" y="11.1" curve="6.546325"/>
<vertex x="16.7" y="11.6" curve="-90"/>
</polygon>
</package>
<package name="MINI-PCIE-ATK-DAUGHTER">
<smd name="15" x="-6.2" y="-4.1" dx="0.45" dy="2" layer="1"/>
<smd name="13" x="-7" y="-4.1" dx="0.45" dy="2" layer="1"/>
<smd name="11" x="-7.8" y="-4.1" dx="0.45" dy="2" layer="1"/>
<smd name="9" x="-8.6" y="-4.1" dx="0.45" dy="2" layer="1"/>
<smd name="7" x="-9.4" y="-4.1" dx="0.45" dy="2" layer="1"/>
<smd name="5" x="-10.2" y="-4.1" dx="0.45" dy="2" layer="1"/>
<smd name="3" x="-11" y="-4.1" dx="0.45" dy="2" layer="1"/>
<smd name="1" x="-11.8" y="-4.1" dx="0.45" dy="2" layer="1"/>
<smd name="17" x="-2.2" y="-4.1" dx="0.45" dy="2" layer="1"/>
<smd name="19" x="-1.4" y="-4.1" dx="0.45" dy="2" layer="1"/>
<smd name="21" x="-0.6" y="-4.1" dx="0.45" dy="2" layer="1"/>
<smd name="23" x="0.2" y="-4.1" dx="0.45" dy="2" layer="1"/>
<smd name="25" x="1" y="-4.1" dx="0.45" dy="2" layer="1"/>
<smd name="27" x="1.8" y="-4.1" dx="0.45" dy="2" layer="1"/>
<smd name="29" x="2.6" y="-4.1" dx="0.45" dy="2" layer="1"/>
<smd name="31" x="3.4" y="-4.1" dx="0.45" dy="2" layer="1"/>
<smd name="33" x="4.2" y="-4.1" dx="0.45" dy="2" layer="1"/>
<smd name="35" x="5" y="-4.1" dx="0.45" dy="2" layer="1"/>
<smd name="37" x="5.8" y="-4.1" dx="0.45" dy="2" layer="1"/>
<smd name="39" x="6.6" y="-4.1" dx="0.45" dy="2" layer="1"/>
<smd name="41" x="7.4" y="-4.1" dx="0.45" dy="2" layer="1"/>
<smd name="43" x="8.2" y="-4.1" dx="0.45" dy="2" layer="1"/>
<smd name="45" x="9" y="-4.1" dx="0.45" dy="2" layer="1"/>
<smd name="47" x="9.8" y="-4.1" dx="0.45" dy="2" layer="1"/>
<smd name="49" x="10.6" y="-4.1" dx="0.45" dy="2" layer="1"/>
<smd name="51" x="11.4" y="-4.1" dx="0.45" dy="2" layer="1"/>
<smd name="16" x="-5.8" y="4.1" dx="0.45" dy="2" layer="1" rot="R180"/>
<wire x1="-4.6" y1="0" x2="-4.6" y2="3.25" width="0.127" layer="51"/>
<wire x1="-3.1" y1="3.25" x2="-3.1" y2="0" width="0.127" layer="51"/>
<smd name="14" x="-6.6" y="4.1" dx="0.45" dy="2" layer="1" rot="R180"/>
<smd name="12" x="-7.4" y="4.1" dx="0.45" dy="2" layer="1" rot="R180"/>
<smd name="10" x="-8.2" y="4.1" dx="0.45" dy="2" layer="1" rot="R180"/>
<smd name="8" x="-9" y="4.1" dx="0.45" dy="2" layer="1" rot="R180"/>
<smd name="6" x="-9.8" y="4.1" dx="0.45" dy="2" layer="1" rot="R180"/>
<smd name="4" x="-10.6" y="4.1" dx="0.45" dy="2" layer="1" rot="R180"/>
<smd name="2" x="-11.4" y="4.1" dx="0.45" dy="2" layer="1" rot="R180"/>
<smd name="18" x="-1.8" y="4.1" dx="0.45" dy="2" layer="1" rot="R180"/>
<smd name="20" x="-1" y="4.1" dx="0.45" dy="2" layer="1" rot="R180"/>
<smd name="22" x="-0.2" y="4.1" dx="0.45" dy="2" layer="1" rot="R180"/>
<smd name="24" x="0.6" y="4.1" dx="0.45" dy="2" layer="1" rot="R180"/>
<smd name="26" x="1.4" y="4.1" dx="0.45" dy="2" layer="1" rot="R180"/>
<smd name="28" x="2.2" y="4.1" dx="0.45" dy="2" layer="1" rot="R180"/>
<smd name="30" x="3" y="4.1" dx="0.45" dy="2" layer="1" rot="R180"/>
<smd name="32" x="3.8" y="4.1" dx="0.45" dy="2" layer="1" rot="R180"/>
<smd name="34" x="4.6" y="4.1" dx="0.45" dy="2" layer="1" rot="R180"/>
<smd name="36" x="5.4" y="4.1" dx="0.45" dy="2" layer="1" rot="R180"/>
<smd name="38" x="6.2" y="4.1" dx="0.45" dy="2" layer="1" rot="R180"/>
<smd name="40" x="7" y="4.1" dx="0.45" dy="2" layer="1" rot="R180"/>
<smd name="42" x="7.8" y="4.1" dx="0.45" dy="2" layer="1" rot="R180"/>
<smd name="44" x="8.6" y="4.1" dx="0.45" dy="2" layer="1" rot="R180"/>
<smd name="46" x="9.4" y="4.1" dx="0.45" dy="2" layer="1" rot="R180"/>
<smd name="48" x="10.2" y="4.1" dx="0.45" dy="2" layer="1" rot="R180"/>
<smd name="50" x="11" y="4.1" dx="0.45" dy="2" layer="1" rot="R180"/>
<smd name="52" x="11.8" y="4.1" dx="0.45" dy="2" layer="1" rot="R180"/>
<wire x1="-3.1" y1="0" x2="12.85" y2="0" width="0.127" layer="51"/>
<wire x1="12.85" y1="0" x2="12.85" y2="3.25" width="0.127" layer="51"/>
<wire x1="-13.6" y1="4" x2="-19.5" y2="4" width="0.127" layer="51"/>
<wire x1="-12.85" y1="3.25" x2="-13.6" y2="4" width="0.127" layer="51" curve="90"/>
<wire x1="12.85" y1="3.25" x2="13.6" y2="4" width="0.127" layer="51" curve="-90"/>
<wire x1="-3.1" y1="3.25" x2="-3.85" y2="4" width="0.127" layer="51" curve="90"/>
<wire x1="-4.6" y1="3.25" x2="-3.85" y2="4" width="0.127" layer="51" curve="-90"/>
<wire x1="-12.85" y1="3.25" x2="-12.85" y2="0" width="0.127" layer="51"/>
<wire x1="-12.85" y1="0" x2="-4.6" y2="0" width="0.127" layer="51"/>
<wire x1="-15" y1="-5.1" x2="15" y2="-5.1" width="0.127" layer="51"/>
<wire x1="15" y1="-5.1" x2="15" y2="-3.5" width="0.127" layer="51"/>
<wire x1="15" y1="-3.5" x2="15" y2="3.5" width="0.127" layer="51"/>
<wire x1="15" y1="3.5" x2="15" y2="5.1" width="0.127" layer="51"/>
<wire x1="15" y1="5.1" x2="-15" y2="5.1" width="0.127" layer="51"/>
<wire x1="-15" y1="5.1" x2="-15" y2="3.5" width="0.127" layer="51"/>
<wire x1="-15" y1="3.5" x2="-15" y2="-3.5" width="0.127" layer="51"/>
<wire x1="-15" y1="-3.5" x2="-15" y2="-5.1" width="0.127" layer="51"/>
<wire x1="-15" y1="-3.5" x2="15" y2="-3.5" width="0.127" layer="51"/>
<wire x1="-15" y1="3.5" x2="15" y2="3.5" width="0.127" layer="51"/>
<wire x1="-15.8" y1="-2.5" x2="15.8" y2="-2.5" width="0.127" layer="51"/>
<pad name="P$1" x="16.5" y="8.5" drill="4.5" diameter="6.5" thermals="no"/>
<pad name="P$2" x="-16.5" y="8.5" drill="4.5" diameter="6.5" thermals="no"/>
<polygon width="0.127" layer="31">
<vertex x="-16.8" y="11.6"/>
<vertex x="-16.8" y="11.1" curve="-73.739933"/>
<vertex x="-17" y="10.8" curve="60.752764"/>
<vertex x="-18.8" y="8.9" curve="-80.286401"/>
<vertex x="-19.2" y="8.7" curve="6.546325"/>
<vertex x="-19.6" y="8.7" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-16.2" y="5.4"/>
<vertex x="-16.2" y="5.9" curve="-73.739933"/>
<vertex x="-16" y="6.2" curve="60.752764"/>
<vertex x="-14.2" y="8" curve="-80.286401"/>
<vertex x="-13.8" y="8.3" curve="6.546325"/>
<vertex x="-13.4" y="8.3" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-19.6" y="8.2"/>
<vertex x="-19.1" y="8.2" curve="-73.739933"/>
<vertex x="-18.8" y="8" curve="60.752764"/>
<vertex x="-17" y="6.2" curve="-80.286401"/>
<vertex x="-16.7" y="5.8" curve="6.546325"/>
<vertex x="-16.7" y="5.4" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-13.4" y="8.8"/>
<vertex x="-13.9" y="8.8" curve="-73.739933"/>
<vertex x="-14.2" y="9" curve="60.752764"/>
<vertex x="-16.1" y="10.8" curve="-80.286401"/>
<vertex x="-16.3" y="11.1" curve="6.546325"/>
<vertex x="-16.3" y="11.6" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="16.2" y="11.6"/>
<vertex x="16.2" y="11.1" curve="-73.739933"/>
<vertex x="16" y="10.8" curve="60.752764"/>
<vertex x="14.2" y="8.9" curve="-80.286401"/>
<vertex x="13.8" y="8.7" curve="6.546325"/>
<vertex x="13.4" y="8.7" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="16.8" y="5.4"/>
<vertex x="16.8" y="5.9" curve="-73.739933"/>
<vertex x="17" y="6.2" curve="60.752764"/>
<vertex x="18.8" y="8" curve="-80.286401"/>
<vertex x="19.2" y="8.3" curve="6.546325"/>
<vertex x="19.6" y="8.3" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="13.4" y="8.2"/>
<vertex x="13.9" y="8.2" curve="-73.739933"/>
<vertex x="14.2" y="8" curve="60.752764"/>
<vertex x="16" y="6.2" curve="-80.286401"/>
<vertex x="16.3" y="5.8" curve="6.546325"/>
<vertex x="16.3" y="5.4" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="19.6" y="8.8"/>
<vertex x="19.1" y="8.8" curve="-73.739933"/>
<vertex x="18.8" y="9" curve="60.752764"/>
<vertex x="16.9" y="10.8" curve="-80.286401"/>
<vertex x="16.7" y="11.1" curve="6.546325"/>
<vertex x="16.7" y="11.6" curve="-90"/>
</polygon>
<hole x="-12.5" y="0" drill="1.6"/>
<hole x="12.5" y="0" drill="1.1"/>
<smd name="P$3" x="14.65" y="-3.5" dx="2.3" dy="3.2" layer="1" rot="R180"/>
<smd name="P$4" x="-14.65" y="-3.5" dx="2.3" dy="3.2" layer="1" rot="R180"/>
<wire x1="-19.5" y1="4" x2="-20" y2="4.5" width="0.127" layer="51" curve="-90"/>
<wire x1="-20" y1="4.5" x2="-20" y2="34" width="0.127" layer="51"/>
<wire x1="-20" y1="34" x2="-19.5" y2="34.5" width="0.127" layer="51" curve="-90"/>
<wire x1="-19.5" y1="34.5" x2="19.5" y2="34.5" width="0.127" layer="51"/>
<wire x1="13.6" y1="4" x2="19.5" y2="4" width="0.127" layer="51"/>
<wire x1="19.5" y1="4" x2="20" y2="4.5" width="0.127" layer="51" curve="90"/>
<wire x1="20" y1="4.5" x2="20" y2="34" width="0.127" layer="51"/>
<wire x1="20" y1="34" x2="19.5" y2="34.5" width="0.127" layer="51" curve="90"/>
<wire x1="-19.5" y1="45" x2="19.5" y2="45" width="0.127" layer="51"/>
<wire x1="-20" y1="34" x2="-20" y2="44.5" width="0.127" layer="51"/>
<wire x1="-20" y1="44.5" x2="-19.5" y2="45" width="0.127" layer="51" curve="-90"/>
<wire x1="19.5" y1="45" x2="20" y2="44.5" width="0.127" layer="51" curve="-90"/>
<wire x1="20" y1="44.5" x2="20" y2="34" width="0.127" layer="51"/>
</package>
<package name="ATK-BFPP-BOTTOM-FX8-60">
<smd name="P$1" x="-8.6" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<pad name="P$61" x="-16.5" y="0" drill="4.5" diameter="6.5" thermals="no"/>
<pad name="P$62" x="16.4" y="0" drill="4.5" diameter="6.5" thermals="no"/>
<smd name="P$60" x="-8.6" y="2.85" dx="0.35" dy="1.7" layer="16"/>
<rectangle x1="-12.3" y1="-2.8" x2="12.3" y2="2.8" layer="52"/>
<hole x="-11.525" y="-1.8" drill="1.1"/>
<hole x="11.525" y="-1.8" drill="0.7"/>
<smd name="P$2" x="-8" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$3" x="-7.4" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$4" x="-6.8" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$5" x="-6.2" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$6" x="-5.6" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$7" x="-5" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$8" x="-4.4" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$9" x="-3.8" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$10" x="-3.2" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$11" x="-2.6" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$12" x="-2" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$13" x="-1.4" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$14" x="-0.8" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$15" x="-0.2" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$16" x="0.4" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$17" x="1" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$18" x="1.6" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$19" x="2.2" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$20" x="2.8" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$21" x="3.4" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$22" x="4" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$23" x="4.6" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$24" x="5.2" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$25" x="5.8" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$26" x="6.4" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$27" x="7" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$28" x="7.6" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$29" x="8.2" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$30" x="8.8" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$31" x="8.8" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$32" x="8.2" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$33" x="7.6" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$34" x="7" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$35" x="6.4" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$36" x="5.8" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$37" x="5.2" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$38" x="4.6" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$39" x="4" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$40" x="3.4" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$41" x="2.8" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$42" x="2.2" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$43" x="1.6" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$44" x="1" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$45" x="0.4" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$46" x="-0.2" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$47" x="-0.8" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$48" x="-1.4" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$49" x="-2" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$50" x="-2.6" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$51" x="-3.2" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$52" x="-3.8" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$53" x="-4.4" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$54" x="-5" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$55" x="-5.6" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$56" x="-6.2" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$57" x="-6.8" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$58" x="-7.4" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$59" x="-8" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<polygon width="0.127" layer="31">
<vertex x="-16.8" y="3.1"/>
<vertex x="-16.8" y="2.3" curve="90"/>
<vertex x="-18.8" y="0.3"/>
<vertex x="-19.6" y="0.3" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-19.6" y="-0.3"/>
<vertex x="-18.8" y="-0.3" curve="90"/>
<vertex x="-16.8" y="-2.3"/>
<vertex x="-16.8" y="-3.1" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-16.2" y="-3.1"/>
<vertex x="-16.2" y="-2.3" curve="90"/>
<vertex x="-14.2" y="-0.3"/>
<vertex x="-13.4" y="-0.3" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-13.4" y="0.3"/>
<vertex x="-14.2" y="0.3" curve="90"/>
<vertex x="-16.2" y="2.3"/>
<vertex x="-16.2" y="3.1" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="16.1" y="3.1"/>
<vertex x="16.1" y="2.3" curve="90"/>
<vertex x="14.1" y="0.3"/>
<vertex x="13.3" y="0.3" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="13.3" y="-0.3"/>
<vertex x="14.1" y="-0.3" curve="90"/>
<vertex x="16.1" y="-2.3"/>
<vertex x="16.1" y="-3.1" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="16.7" y="-3.1"/>
<vertex x="16.7" y="-2.3" curve="90"/>
<vertex x="18.7" y="-0.3"/>
<vertex x="19.5" y="-0.3" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="19.5" y="0.3"/>
<vertex x="18.7" y="0.3" curve="90"/>
<vertex x="16.7" y="2.3"/>
<vertex x="16.7" y="3.1" curve="-90"/>
</polygon>
</package>
<package name="ATK-BFPP-TOP-FX8-60">
<smd name="P$1" x="-8.6" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<pad name="P$61" x="-16.5" y="0" drill="4.5" diameter="6.5" thermals="no"/>
<pad name="P$62" x="16.4" y="0" drill="4.5" diameter="6.5" thermals="no"/>
<smd name="P$60" x="-8.6" y="2.7" dx="0.35" dy="1.7" layer="1"/>
<rectangle x1="-12.3" y1="-2.55" x2="12.3" y2="2.55" layer="51"/>
<hole x="-11.525" y="-1.8" drill="1.1"/>
<hole x="11.525" y="-1.8" drill="0.7"/>
<smd name="P$2" x="-8" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$3" x="-7.4" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$4" x="-6.8" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$5" x="-6.2" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$6" x="-5.6" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$7" x="-5" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$8" x="-4.4" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$9" x="-3.8" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$10" x="-3.2" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$11" x="-2.6" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$12" x="-2" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$13" x="-1.4" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$14" x="-0.8" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$15" x="-0.2" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$16" x="0.4" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$17" x="1" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$18" x="1.6" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$19" x="2.2" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$20" x="2.8" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$21" x="3.4" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$22" x="4" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$23" x="4.6" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$24" x="5.2" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$25" x="5.8" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$26" x="6.4" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$27" x="7" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$28" x="7.6" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$29" x="8.2" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$30" x="8.8" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$31" x="8.8" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$32" x="8.2" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$33" x="7.6" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$34" x="7" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$35" x="6.4" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$36" x="5.8" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$37" x="5.2" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$38" x="4.6" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$39" x="4" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$40" x="3.4" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$41" x="2.8" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$42" x="2.2" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$43" x="1.6" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$44" x="1" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$45" x="0.4" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$46" x="-0.2" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$47" x="-0.8" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$48" x="-1.4" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$49" x="-2" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$50" x="-2.6" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$51" x="-3.2" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$52" x="-3.8" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$53" x="-4.4" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$54" x="-5" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$55" x="-5.6" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$56" x="-6.2" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$57" x="-6.8" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$58" x="-7.4" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$59" x="-8" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<polygon width="0.127" layer="31">
<vertex x="-16.8" y="3.1"/>
<vertex x="-16.8" y="2.3" curve="90"/>
<vertex x="-18.8" y="0.3"/>
<vertex x="-19.6" y="0.3" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-19.6" y="-0.3"/>
<vertex x="-18.8" y="-0.3" curve="90"/>
<vertex x="-16.8" y="-2.3"/>
<vertex x="-16.8" y="-3.1" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-16.2" y="-3.1"/>
<vertex x="-16.2" y="-2.3" curve="90"/>
<vertex x="-14.2" y="-0.3"/>
<vertex x="-13.4" y="-0.3" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-13.4" y="0.3"/>
<vertex x="-14.2" y="0.3" curve="90"/>
<vertex x="-16.2" y="2.3"/>
<vertex x="-16.2" y="3.1" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="16.1" y="3.1"/>
<vertex x="16.1" y="2.3" curve="90"/>
<vertex x="14.1" y="0.3"/>
<vertex x="13.3" y="0.3" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="13.3" y="-0.3"/>
<vertex x="14.1" y="-0.3" curve="90"/>
<vertex x="16.1" y="-2.3"/>
<vertex x="16.1" y="-3.1" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="16.7" y="-3.1"/>
<vertex x="16.7" y="-2.3" curve="90"/>
<vertex x="18.7" y="-0.3"/>
<vertex x="19.5" y="-0.3" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="19.5" y="0.3"/>
<vertex x="18.7" y="0.3" curve="90"/>
<vertex x="16.7" y="2.3"/>
<vertex x="16.7" y="3.1" curve="-90"/>
</polygon>
<wire x1="-20" y1="-3.5" x2="-19.5" y2="-4" width="0.127" layer="51" curve="90"/>
<wire x1="-19.5" y1="-4" x2="19.5" y2="-4" width="0.127" layer="51"/>
<wire x1="19.5" y1="-4" x2="20" y2="-3.5" width="0.127" layer="51" curve="90"/>
<wire x1="-20" y1="-3.5" x2="-20" y2="29.5" width="0.127" layer="51"/>
<wire x1="-20" y1="29.5" x2="-19.5" y2="30" width="0.127" layer="51" curve="-90"/>
<wire x1="-19.5" y1="30" x2="19.5" y2="30" width="0.127" layer="51"/>
<wire x1="19.5" y1="30" x2="20" y2="29.5" width="0.127" layer="51" curve="-90"/>
<wire x1="20" y1="29.5" x2="20" y2="-3.5" width="0.127" layer="51"/>
<wire x1="-20" y1="29.5" x2="-20" y2="44.5" width="0.127" layer="51"/>
<wire x1="-20" y1="44.5" x2="-19.5" y2="45" width="0.127" layer="51" curve="-90"/>
<wire x1="-19.5" y1="45" x2="19.5" y2="45" width="0.127" layer="51"/>
<wire x1="19.5" y1="45" x2="20" y2="44.5" width="0.127" layer="51" curve="-90"/>
<wire x1="20" y1="44.5" x2="20" y2="29.5" width="0.127" layer="51"/>
<circle x="-20" y="5" radius="1.6" width="0.127" layer="51"/>
<circle x="-20" y="5" radius="2.75" width="0.127" layer="51"/>
<circle x="20" y="5" radius="1.6" width="0.127" layer="51"/>
<circle x="20" y="5" radius="2.75" width="0.127" layer="51"/>
</package>
<package name="ATK-BFPP-THRU-FX8-60">
<smd name="P$1" x="-8.6" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<pad name="P$61" x="-16.5" y="0" drill="4.5" diameter="6.5" thermals="no"/>
<pad name="P$62" x="16.5" y="0" drill="4.5" diameter="6.5" thermals="no"/>
<smd name="P$60" x="-8.6" y="2.7" dx="0.35" dy="1.7" layer="1"/>
<rectangle x1="-12.3" y1="-2.55" x2="12.3" y2="2.55" layer="51"/>
<hole x="-11.525" y="-1.8" drill="1.1"/>
<hole x="11.525" y="-1.8" drill="0.7"/>
<smd name="P$2" x="-8" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$3" x="-7.4" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$4" x="-6.8" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$5" x="-6.2" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$6" x="-5.6" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$7" x="-5" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$8" x="-4.4" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$9" x="-3.8" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$10" x="-3.2" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$11" x="-2.6" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$12" x="-2" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$13" x="-1.4" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$14" x="-0.8" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$15" x="-0.2" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$16" x="0.4" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$17" x="1" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$18" x="1.6" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$19" x="2.2" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$20" x="2.8" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$21" x="3.4" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$22" x="4" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$23" x="4.6" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$24" x="5.2" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$25" x="5.8" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$26" x="6.4" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$27" x="7" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$28" x="7.6" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$29" x="8.2" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$30" x="8.8" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$31" x="8.8" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$32" x="8.2" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$33" x="7.6" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$34" x="7" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$35" x="6.4" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$36" x="5.8" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$37" x="5.2" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$38" x="4.6" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$39" x="4" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$40" x="3.4" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$41" x="2.8" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$42" x="2.2" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$43" x="1.6" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$44" x="1" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$45" x="0.4" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$46" x="-0.2" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$47" x="-0.8" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$48" x="-1.4" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$49" x="-2" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$50" x="-2.6" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$51" x="-3.2" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$52" x="-3.8" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$53" x="-4.4" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$54" x="-5" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$55" x="-5.6" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$56" x="-6.2" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$57" x="-6.8" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$58" x="-7.4" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$59" x="-8" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<polygon width="0.127" layer="31">
<vertex x="-16.8" y="3.1"/>
<vertex x="-16.8" y="2.3" curve="90"/>
<vertex x="-18.8" y="0.3"/>
<vertex x="-19.6" y="0.3" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-19.6" y="-0.3"/>
<vertex x="-18.8" y="-0.3" curve="90"/>
<vertex x="-16.8" y="-2.3"/>
<vertex x="-16.8" y="-3.1" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-16.2" y="-3.1"/>
<vertex x="-16.2" y="-2.3" curve="90"/>
<vertex x="-14.2" y="-0.3"/>
<vertex x="-13.4" y="-0.3" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-13.4" y="0.3"/>
<vertex x="-14.2" y="0.3" curve="90"/>
<vertex x="-16.2" y="2.3"/>
<vertex x="-16.2" y="3.1" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="16.2" y="3.1"/>
<vertex x="16.2" y="2.3" curve="90"/>
<vertex x="14.2" y="0.3"/>
<vertex x="13.4" y="0.3" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="13.4" y="-0.3"/>
<vertex x="14.2" y="-0.3" curve="90"/>
<vertex x="16.2" y="-2.3"/>
<vertex x="16.2" y="-3.1" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="16.8" y="-3.1"/>
<vertex x="16.8" y="-2.3" curve="90"/>
<vertex x="18.8" y="-0.3"/>
<vertex x="19.6" y="-0.3" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="19.6" y="0.3"/>
<vertex x="18.8" y="0.3" curve="90"/>
<vertex x="16.8" y="2.3"/>
<vertex x="16.8" y="3.1" curve="-90"/>
</polygon>
<wire x1="-20" y1="-3.5" x2="-19.5" y2="-4" width="0.127" layer="51" curve="90"/>
<wire x1="-19.5" y1="-4" x2="19.5" y2="-4" width="0.127" layer="51"/>
<wire x1="19.5" y1="-4" x2="20" y2="-3.5" width="0.127" layer="51" curve="90"/>
<wire x1="-20" y1="-3.5" x2="-20" y2="29.5" width="0.127" layer="51"/>
<wire x1="-20" y1="29.5" x2="-19.5" y2="30" width="0.127" layer="51" curve="-90"/>
<wire x1="-19.5" y1="30" x2="19.5" y2="30" width="0.127" layer="51"/>
<wire x1="19.5" y1="30" x2="20" y2="29.5" width="0.127" layer="51" curve="-90"/>
<wire x1="20" y1="29.5" x2="20" y2="-3.5" width="0.127" layer="51"/>
<wire x1="-20" y1="29.5" x2="-20" y2="44.5" width="0.127" layer="51"/>
<wire x1="-20" y1="44.5" x2="-19.5" y2="45" width="0.127" layer="51" curve="-90"/>
<wire x1="-19.5" y1="45" x2="19.5" y2="45" width="0.127" layer="51"/>
<wire x1="19.5" y1="45" x2="20" y2="44.5" width="0.127" layer="51" curve="-90"/>
<wire x1="20" y1="44.5" x2="20" y2="29.5" width="0.127" layer="51"/>
<smd name="P$63" x="-8.6" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$64" x="-8" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$65" x="-7.4" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$66" x="-6.8" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$67" x="-6.2" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$68" x="-5.6" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$69" x="-5" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$70" x="-4.4" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$71" x="-3.8" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$72" x="-3.2" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$73" x="-2.6" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$74" x="-2" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$75" x="-1.4" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$76" x="-0.8" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$77" x="-0.2" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$78" x="0.4" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$79" x="1" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$80" x="1.6" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$81" x="2.2" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$82" x="2.8" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$83" x="3.4" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$84" x="4" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$85" x="4.6" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$86" x="5.2" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$87" x="5.8" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$88" x="6.4" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$89" x="7" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$90" x="7.6" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$91" x="8.2" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$92" x="8.8" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$93" x="8.8" y="2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$94" x="8.2" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$95" x="7.6" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$96" x="7" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$97" x="6.4" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$98" x="5.8" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$99" x="5.2" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$100" x="4.6" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$101" x="4" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$102" x="3.4" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$103" x="2.8" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$104" x="2.2" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$105" x="1.6" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$106" x="1" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$107" x="0.4" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$108" x="-0.2" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$109" x="-0.8" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$110" x="-1.4" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$111" x="-2" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$112" x="-2.6" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$113" x="-3.2" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$114" x="-3.8" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$115" x="-4.4" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$116" x="-5" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$117" x="-5.6" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$118" x="-6.2" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$119" x="-6.8" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$120" x="-7.4" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$121" x="-8" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$122" x="-8.6" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<pad name="P$123" x="-8.6" y="-3.25" drill="0.3" diameter="0.35" shape="square"/>
<pad name="P$124" x="-8" y="-2.65" drill="0.3" diameter="0.35" shape="square"/>
<pad name="P$125" x="-7.4" y="-3.25" drill="0.3" diameter="0.35" shape="square"/>
<pad name="P$126" x="-6.8" y="-2.65" drill="0.3" diameter="0.35" shape="square"/>
<pad name="P$127" x="-6.2" y="-3.25" drill="0.3" diameter="0.35" shape="square"/>
<pad name="P$128" x="-5.6" y="-2.65" drill="0.3" diameter="0.35" shape="square"/>
<pad name="P$129" x="-5" y="-3.25" drill="0.3" diameter="0.35" shape="square"/>
<pad name="P$130" x="-4.4" y="-2.65" drill="0.3" diameter="0.35" shape="square"/>
<pad name="P$131" x="-3.8" y="-3.25" drill="0.3" diameter="0.35" shape="square"/>
<pad name="P$132" x="-3.2" y="-2.65" drill="0.3" diameter="0.35" shape="square"/>
<pad name="P$133" x="-2.6" y="-3.25" drill="0.3" diameter="0.35" shape="square"/>
<pad name="P$134" x="-2" y="-2.65" drill="0.3" diameter="0.35" shape="square"/>
<pad name="P$135" x="-1.4" y="-3.25" drill="0.3" diameter="0.35" shape="square"/>
<pad name="P$136" x="-0.8" y="-2.65" drill="0.3" diameter="0.35" shape="square"/>
<pad name="P$137" x="-0.2" y="-3.25" drill="0.3" diameter="0.35" shape="square"/>
<pad name="P$138" x="0.4" y="-2.65" drill="0.3" diameter="0.35" shape="square"/>
<pad name="P$139" x="1" y="-3.25" drill="0.3" diameter="0.35" shape="square"/>
<pad name="P$140" x="1.6" y="-2.65" drill="0.3" diameter="0.35" shape="square"/>
<pad name="P$141" x="2.2" y="-3.25" drill="0.3" diameter="0.35" shape="square"/>
<pad name="P$142" x="2.8" y="-2.65" drill="0.3" diameter="0.35" shape="square"/>
<pad name="P$143" x="3.4" y="-3.25" drill="0.3" diameter="0.35" shape="square"/>
<pad name="P$144" x="4" y="-2.65" drill="0.3" diameter="0.35" shape="square"/>
<pad name="P$145" x="4.6" y="-3.25" drill="0.3" diameter="0.35" shape="square"/>
<pad name="P$146" x="5.2" y="-2.65" drill="0.3" diameter="0.35" shape="square"/>
<pad name="P$147" x="5.8" y="-3.25" drill="0.3" diameter="0.35" shape="square"/>
<pad name="P$148" x="6.4" y="-2.65" drill="0.3" diameter="0.35" shape="square"/>
<pad name="P$149" x="7" y="-3.25" drill="0.3" diameter="0.35" shape="square"/>
<pad name="P$150" x="7.6" y="-2.65" drill="0.3" diameter="0.35" shape="square"/>
<pad name="P$151" x="8.2" y="-3.25" drill="0.3" diameter="0.35" shape="square"/>
<pad name="P$152" x="8.8" y="-2.65" drill="0.3" diameter="0.35" shape="square"/>
<pad name="P$153" x="8.8" y="2.65" drill="0.3" diameter="0.35" shape="square" rot="R180"/>
<pad name="P$154" x="8.2" y="3.25" drill="0.3" diameter="0.35" shape="square" rot="R180"/>
<pad name="P$155" x="7.6" y="2.65" drill="0.3" diameter="0.35" shape="square" rot="R180"/>
<pad name="P$156" x="7" y="3.25" drill="0.3" diameter="0.35" shape="square" rot="R180"/>
<pad name="P$157" x="6.4" y="2.65" drill="0.3" diameter="0.35" shape="square" rot="R180"/>
<pad name="P$158" x="5.8" y="3.25" drill="0.3" diameter="0.35" shape="square" rot="R180"/>
<pad name="P$159" x="5.2" y="2.65" drill="0.3" diameter="0.35" shape="square" rot="R180"/>
<pad name="P$160" x="4.6" y="3.25" drill="0.3" diameter="0.35" shape="square" rot="R180"/>
<pad name="P$161" x="4" y="2.65" drill="0.3" diameter="0.35" shape="square" rot="R180"/>
<pad name="P$162" x="3.4" y="3.25" drill="0.3" diameter="0.35" shape="square" rot="R180"/>
<pad name="P$163" x="2.8" y="2.65" drill="0.3" diameter="0.35" shape="square" rot="R180"/>
<pad name="P$164" x="2.2" y="3.25" drill="0.3" diameter="0.35" shape="square" rot="R180"/>
<pad name="P$165" x="1.6" y="2.65" drill="0.3" diameter="0.35" shape="square" rot="R180"/>
<pad name="P$166" x="1" y="3.25" drill="0.3" diameter="0.35" shape="square" rot="R180"/>
<pad name="P$167" x="0.4" y="2.65" drill="0.3" diameter="0.35" shape="square" rot="R180"/>
<pad name="P$168" x="-0.2" y="3.25" drill="0.3" diameter="0.35" shape="square" rot="R180"/>
<pad name="P$169" x="-0.8" y="2.65" drill="0.3" diameter="0.35" shape="square" rot="R180"/>
<pad name="P$170" x="-1.4" y="3.25" drill="0.3" diameter="0.35" shape="square" rot="R180"/>
<pad name="P$171" x="-2" y="2.65" drill="0.3" diameter="0.35" shape="square" rot="R180"/>
<pad name="P$172" x="-2.6" y="3.25" drill="0.3" diameter="0.35" shape="square" rot="R180"/>
<pad name="P$173" x="-3.2" y="2.65" drill="0.3" diameter="0.35" shape="square" rot="R180"/>
<pad name="P$174" x="-3.8" y="3.25" drill="0.3" diameter="0.35" shape="square" rot="R180"/>
<pad name="P$175" x="-4.4" y="2.65" drill="0.3" diameter="0.35" shape="square" rot="R180"/>
<pad name="P$176" x="-5" y="3.25" drill="0.3" diameter="0.35" shape="square" rot="R180"/>
<pad name="P$177" x="-5.6" y="2.65" drill="0.3" diameter="0.35" shape="square" rot="R180"/>
<pad name="P$178" x="-6.2" y="3.25" drill="0.3" diameter="0.35" shape="square" rot="R180"/>
<pad name="P$179" x="-6.8" y="2.65" drill="0.3" diameter="0.35" shape="square" rot="R180"/>
<pad name="P$180" x="-7.4" y="3.25" drill="0.3" diameter="0.35" shape="square" rot="R180"/>
<pad name="P$181" x="-8" y="2.65" drill="0.3" diameter="0.35" shape="square" rot="R180"/>
<pad name="P$182" x="-8.6" y="3.25" drill="0.3" diameter="0.35" shape="square" rot="R180"/>
</package>
<package name="JST-6-SMD-HORI-1.0MM">
<description>&lt;h3&gt;JST SH Vertical 6-Pin SMT&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:6&lt;/li&gt;
&lt;li&gt;Pin pitch: 1 mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/GPS/EM406-SMDConnector-eSH.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;JST_6PIN_VERTICAL&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<circle x="-3.44" y="-5.16" radius="0.1047" width="0.4064" layer="21"/>
<wire x1="-2.9" y1="-0.1" x2="2.9" y2="-0.1" width="0.254" layer="21"/>
<wire x1="-4" y1="-2.1" x2="-4" y2="-4.4" width="0.254" layer="21"/>
<wire x1="3.1" y1="-4.4" x2="4" y2="-4.4" width="0.254" layer="21"/>
<wire x1="4" y1="-4.4" x2="4" y2="-2.1" width="0.254" layer="21"/>
<wire x1="-4" y1="-4.4" x2="-3.1" y2="-4.4" width="0.254" layer="21"/>
<smd name="1" x="-2.5" y="-4.775" dx="0.6" dy="1.55" layer="1"/>
<smd name="2" x="-1.5" y="-4.775" dx="0.6" dy="1.55" layer="1"/>
<smd name="3" x="-0.5" y="-4.775" dx="0.6" dy="1.55" layer="1"/>
<smd name="4" x="0.5" y="-4.775" dx="0.6" dy="1.55" layer="1"/>
<smd name="5" x="1.5" y="-4.775" dx="0.6" dy="1.55" layer="1"/>
<smd name="6" x="2.5" y="-4.775" dx="0.6" dy="1.55" layer="1"/>
<smd name="M1" x="-3.8" y="-0.9" dx="1.2" dy="1.8" layer="1"/>
<smd name="M2" x="3.8" y="-0.9" dx="1.2" dy="1.8" layer="1"/>
<text x="-1.524" y="0.54" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.905" y="-6.667" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="M3POWERCONN">
<pad name="P$1" x="0" y="0" drill="4.5" diameter="6.5" thermals="no"/>
<polygon width="0.127" layer="31">
<vertex x="-0.3" y="3.1"/>
<vertex x="-0.3" y="2.6" curve="-73.739933"/>
<vertex x="-0.5" y="2.3" curve="60.752764"/>
<vertex x="-2.3" y="0.4" curve="-80.286401"/>
<vertex x="-2.7" y="0.2" curve="6.546325"/>
<vertex x="-3.1" y="0.2" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="0.3" y="-3.1"/>
<vertex x="0.3" y="-2.6" curve="-73.739933"/>
<vertex x="0.5" y="-2.3" curve="60.752764"/>
<vertex x="2.3" y="-0.5" curve="-80.286401"/>
<vertex x="2.7" y="-0.2" curve="6.546325"/>
<vertex x="3.1" y="-0.2" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-3.1" y="-0.3"/>
<vertex x="-2.6" y="-0.3" curve="-73.739933"/>
<vertex x="-2.3" y="-0.5" curve="60.752764"/>
<vertex x="-0.5" y="-2.3" curve="-80.286401"/>
<vertex x="-0.2" y="-2.7" curve="6.546325"/>
<vertex x="-0.2" y="-3.1" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="3.1" y="0.3"/>
<vertex x="2.6" y="0.3" curve="-73.739933"/>
<vertex x="2.3" y="0.5" curve="60.752764"/>
<vertex x="0.4" y="2.3" curve="-80.286401"/>
<vertex x="0.2" y="2.6" curve="6.546325"/>
<vertex x="0.2" y="3.1" curve="-90"/>
</polygon>
</package>
</packages>
<symbols>
<symbol name="DOT">
<circle x="0" y="0" radius="2.54" width="0.254" layer="94"/>
</symbol>
<symbol name="ATK-BFPP">
<pin name="GND" x="-10.16" y="30.48" length="middle"/>
<pin name="+3V3" x="-10.16" y="15.24" length="middle"/>
<pin name="+5V" x="-10.16" y="7.62" length="middle"/>
<pin name="A0/AREF---PB0/ADC" x="-10.16" y="0" length="middle"/>
<pin name="A1/ADC---PB1/ADC" x="-10.16" y="-2.54" length="middle"/>
<pin name="A2/ADC/DAC---PB2/ADC/DAC" x="-10.16" y="-5.08" length="middle"/>
<pin name="A3/ADC/DAC---PB3/ADC/DAC" x="-10.16" y="-7.62" length="middle"/>
<pin name="A4/ADC---PB4/ADC" x="-10.16" y="-10.16" length="middle"/>
<pin name="A5/ADC---PB5/ADC" x="-10.16" y="-12.7" length="middle"/>
<pin name="A6/ADC---PB6/ADC/ADCOUT" x="-10.16" y="-15.24" length="middle"/>
<pin name="A7/ADC---PB7/ADC/ADCOUT" x="-10.16" y="-17.78" length="middle"/>
<pin name="C0/PWMALS---PC0/PWMALS/CSN/SDA" x="-10.16" y="-22.86" length="middle"/>
<pin name="C1/PWMAHS---PC1/PWMAHS/SCK/SCL" x="-10.16" y="-25.4" length="middle"/>
<pin name="C2/PWMBLS---PC2/PWMBLS/MISO/UARTRX" x="-10.16" y="-27.94" length="middle"/>
<pin name="C3/PWMBHS---PC3/PWMBHS/MOSI/UARTTX" x="-10.16" y="-30.48" length="middle"/>
<pin name="C4/PWMCLS---PC4/PWMCLS/CSN" x="-10.16" y="-33.02" length="middle"/>
<pin name="C5/PWMCHS---PC5/PWMCHS/SCK" x="-10.16" y="-35.56" length="middle"/>
<pin name="C6/PWMDLS---PC6/PWMDLS/MISO/UARTRX" x="-10.16" y="-38.1" length="middle"/>
<pin name="C7/PWMDHS---PC7/PWMDHS/MOSI/UARTTX" x="-10.16" y="-40.64" length="middle"/>
<pin name="D0/USART/CSN---PD0/PWMA/CSN" x="-10.16" y="-45.72" length="middle"/>
<pin name="D1/USART/CLK---PD1/PWMB/SCK" x="-10.16" y="-48.26" length="middle"/>
<pin name="D2/USART/RX---PD2/PWMC/MISO/UARTRX" x="-10.16" y="-50.8" length="middle"/>
<pin name="D3/USART/TX---PD3/PWMD/MOSI/UARTTX" x="-10.16" y="-53.34" length="middle"/>
<pin name="D4/PWMELS---PD4" x="-10.16" y="-58.42" length="middle"/>
<pin name="D5/PWMEHS---PD5" x="-10.16" y="-60.96" length="middle"/>
<pin name="E0/SDA---PE0" x="-10.16" y="-66.04" length="middle"/>
<pin name="E1/SCL---PE1" x="-10.16" y="-68.58" length="middle"/>
<pin name="E2/USARTRX---PE2" x="-10.16" y="-73.66" length="middle"/>
<pin name="E3/USARTTX---PE3" x="-10.16" y="-76.2" length="middle"/>
<pin name="E4/USART/CSN---PE4/PWMA/CSN" x="-10.16" y="-81.28" length="middle"/>
<pin name="E5/USART/SCK---PE5/PWMB/SCK" x="-10.16" y="-83.82" length="middle"/>
<pin name="E6/USART/RX---PE6/UARTRX/MISO" x="-10.16" y="-86.36" length="middle"/>
<pin name="E7/USART/TX---PE7/UARTTX/MOSI" x="-10.16" y="-88.9" length="middle"/>
<pin name="VCC" x="-10.16" y="22.86" length="middle"/>
<wire x1="-5.08" y1="33.02" x2="-5.08" y2="-91.44" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-91.44" x2="45.72" y2="-91.44" width="0.254" layer="94"/>
<wire x1="45.72" y1="-91.44" x2="45.72" y2="33.02" width="0.254" layer="94"/>
<wire x1="45.72" y1="33.02" x2="-5.08" y2="33.02" width="0.254" layer="94"/>
<wire x1="27.94" y1="-2.54" x2="27.94" y2="0" width="0.254" layer="94"/>
<wire x1="27.94" y1="-5.08" x2="27.94" y2="-7.62" width="0.254" layer="94"/>
<wire x1="27.94" y1="-10.16" x2="27.94" y2="-12.7" width="0.254" layer="94"/>
<wire x1="27.94" y1="-15.24" x2="27.94" y2="-17.78" width="0.254" layer="94"/>
<wire x1="27.94" y1="-12.7" x2="25.4" y2="-12.7" width="0.254" layer="94"/>
<wire x1="25.4" y1="-10.16" x2="27.94" y2="-10.16" width="0.254" layer="94"/>
<wire x1="27.94" y1="-7.62" x2="25.4" y2="-7.62" width="0.254" layer="94"/>
<wire x1="25.4" y1="-5.08" x2="27.94" y2="-5.08" width="0.254" layer="94"/>
<wire x1="27.94" y1="-2.54" x2="25.4" y2="-2.54" width="0.254" layer="94"/>
<wire x1="25.4" y1="0" x2="27.94" y2="0" width="0.254" layer="94"/>
<wire x1="27.94" y1="-15.24" x2="25.4" y2="-15.24" width="0.254" layer="94"/>
<wire x1="27.94" y1="-17.78" x2="25.4" y2="-17.78" width="0.254" layer="94"/>
</symbol>
<symbol name="CONN_06">
<description>&lt;h3&gt;6 Pin Connection&lt;/h3&gt;</description>
<wire x1="1.27" y1="-7.62" x2="-5.08" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="0" x2="0" y2="0" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="0" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="-5.08" x2="0" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="-5.08" y1="10.16" x2="-5.08" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-7.62" x2="1.27" y2="10.16" width="0.4064" layer="94"/>
<wire x1="-5.08" y1="10.16" x2="1.27" y2="10.16" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="5.08" x2="0" y2="5.08" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="0" y2="2.54" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="7.62" x2="0" y2="7.62" width="0.6096" layer="94"/>
<text x="-5.08" y="-9.906" size="1.778" layer="96" font="vector">&gt;VALUE</text>
<text x="-5.08" y="10.668" size="1.778" layer="95" font="vector">&gt;NAME</text>
<pin name="1" x="5.08" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="5.08" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="3" x="5.08" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="4" x="5.08" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="5" x="5.08" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="6" x="5.08" y="7.62" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="CONN_01">
<description>&lt;h3&gt;2 Pin Connection&lt;/h3&gt;</description>
<wire x1="3.81" y1="-2.54" x2="-2.54" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-2.54" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-2.54" x2="3.81" y2="2.54" width="0.4064" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="3.81" y2="2.54" width="0.4064" layer="94"/>
<text x="-2.54" y="-4.826" size="1.778" layer="96" font="vector">&gt;VALUE</text>
<text x="-2.54" y="3.048" size="1.778" layer="95" font="vector">&gt;NAME</text>
<pin name="2" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="FIDUCIAL" prefix="J">
<description>For use by pick and place machines to calibrate the vision/machine, 1mm
&lt;p&gt;By microbuilder.eu&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="DOT" x="0" y="0"/>
</gates>
<devices>
<device name="" package="FIDUCIAL_1MM">
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="RCT" package="FIDUCIAL_RECT_1MM">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ATK-BFPP" prefix="J">
<gates>
<gate name="G$1" symbol="ATK-BFPP" x="0" y="0"/>
</gates>
<devices>
<device name="MINIPCIE-MOTHER" package="MINI-PCIE-ATK-MOTHER">
<connects>
<connect gate="G$1" pin="+3V3" pad="15 16"/>
<connect gate="G$1" pin="+5V" pad="17 18"/>
<connect gate="G$1" pin="A0/AREF---PB0/ADC" pad="3"/>
<connect gate="G$1" pin="A1/ADC---PB1/ADC" pad="4"/>
<connect gate="G$1" pin="A2/ADC/DAC---PB2/ADC/DAC" pad="5"/>
<connect gate="G$1" pin="A3/ADC/DAC---PB3/ADC/DAC" pad="6"/>
<connect gate="G$1" pin="A4/ADC---PB4/ADC" pad="9"/>
<connect gate="G$1" pin="A5/ADC---PB5/ADC" pad="10"/>
<connect gate="G$1" pin="A6/ADC---PB6/ADC/ADCOUT" pad="11"/>
<connect gate="G$1" pin="A7/ADC---PB7/ADC/ADCOUT" pad="12"/>
<connect gate="G$1" pin="C0/PWMALS---PC0/PWMALS/CSN/SDA" pad="21"/>
<connect gate="G$1" pin="C1/PWMAHS---PC1/PWMAHS/SCK/SCL" pad="22"/>
<connect gate="G$1" pin="C2/PWMBLS---PC2/PWMBLS/MISO/UARTRX" pad="23"/>
<connect gate="G$1" pin="C3/PWMBHS---PC3/PWMBHS/MOSI/UARTTX" pad="24"/>
<connect gate="G$1" pin="C4/PWMCLS---PC4/PWMCLS/CSN" pad="27"/>
<connect gate="G$1" pin="C5/PWMCHS---PC5/PWMCHS/SCK" pad="28"/>
<connect gate="G$1" pin="C6/PWMDLS---PC6/PWMDLS/MISO/UARTRX" pad="29"/>
<connect gate="G$1" pin="C7/PWMDHS---PC7/PWMDHS/MOSI/UARTTX" pad="30"/>
<connect gate="G$1" pin="D0/USART/CSN---PD0/PWMA/CSN" pad="33"/>
<connect gate="G$1" pin="D1/USART/CLK---PD1/PWMB/SCK" pad="34"/>
<connect gate="G$1" pin="D2/USART/RX---PD2/PWMC/MISO/UARTRX" pad="35"/>
<connect gate="G$1" pin="D3/USART/TX---PD3/PWMD/MOSI/UARTTX" pad="36"/>
<connect gate="G$1" pin="D4/PWMELS---PD4" pad="39"/>
<connect gate="G$1" pin="D5/PWMEHS---PD5" pad="40"/>
<connect gate="G$1" pin="E0/SDA---PE0" pad="41"/>
<connect gate="G$1" pin="E1/SCL---PE1" pad="42"/>
<connect gate="G$1" pin="E2/USARTRX---PE2" pad="45"/>
<connect gate="G$1" pin="E3/USARTTX---PE3" pad="46"/>
<connect gate="G$1" pin="E4/USART/CSN---PE4/PWMA/CSN" pad="47"/>
<connect gate="G$1" pin="E5/USART/SCK---PE5/PWMB/SCK" pad="48"/>
<connect gate="G$1" pin="E6/USART/RX---PE6/UARTRX/MISO" pad="51"/>
<connect gate="G$1" pin="E7/USART/TX---PE7/UARTTX/MOSI" pad="52"/>
<connect gate="G$1" pin="GND" pad="1 2 7 8 13 14 19 20 25 26 31 32 37 38 43 44 49 50 P$1"/>
<connect gate="G$1" pin="VCC" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MINIPCIE-DAUGHTER" package="MINI-PCIE-ATK-DAUGHTER">
<connects>
<connect gate="G$1" pin="+3V3" pad="15 16"/>
<connect gate="G$1" pin="+5V" pad="17 18"/>
<connect gate="G$1" pin="A0/AREF---PB0/ADC" pad="3"/>
<connect gate="G$1" pin="A1/ADC---PB1/ADC" pad="4"/>
<connect gate="G$1" pin="A2/ADC/DAC---PB2/ADC/DAC" pad="5"/>
<connect gate="G$1" pin="A3/ADC/DAC---PB3/ADC/DAC" pad="6"/>
<connect gate="G$1" pin="A4/ADC---PB4/ADC" pad="9"/>
<connect gate="G$1" pin="A5/ADC---PB5/ADC" pad="10"/>
<connect gate="G$1" pin="A6/ADC---PB6/ADC/ADCOUT" pad="11"/>
<connect gate="G$1" pin="A7/ADC---PB7/ADC/ADCOUT" pad="12"/>
<connect gate="G$1" pin="C0/PWMALS---PC0/PWMALS/CSN/SDA" pad="21"/>
<connect gate="G$1" pin="C1/PWMAHS---PC1/PWMAHS/SCK/SCL" pad="22"/>
<connect gate="G$1" pin="C2/PWMBLS---PC2/PWMBLS/MISO/UARTRX" pad="23"/>
<connect gate="G$1" pin="C3/PWMBHS---PC3/PWMBHS/MOSI/UARTTX" pad="24"/>
<connect gate="G$1" pin="C4/PWMCLS---PC4/PWMCLS/CSN" pad="27"/>
<connect gate="G$1" pin="C5/PWMCHS---PC5/PWMCHS/SCK" pad="28"/>
<connect gate="G$1" pin="C6/PWMDLS---PC6/PWMDLS/MISO/UARTRX" pad="29"/>
<connect gate="G$1" pin="C7/PWMDHS---PC7/PWMDHS/MOSI/UARTTX" pad="30"/>
<connect gate="G$1" pin="D0/USART/CSN---PD0/PWMA/CSN" pad="33"/>
<connect gate="G$1" pin="D1/USART/CLK---PD1/PWMB/SCK" pad="34"/>
<connect gate="G$1" pin="D2/USART/RX---PD2/PWMC/MISO/UARTRX" pad="35"/>
<connect gate="G$1" pin="D3/USART/TX---PD3/PWMD/MOSI/UARTTX" pad="36"/>
<connect gate="G$1" pin="D4/PWMELS---PD4" pad="39"/>
<connect gate="G$1" pin="D5/PWMEHS---PD5" pad="40"/>
<connect gate="G$1" pin="E0/SDA---PE0" pad="41"/>
<connect gate="G$1" pin="E1/SCL---PE1" pad="42"/>
<connect gate="G$1" pin="E2/USARTRX---PE2" pad="45"/>
<connect gate="G$1" pin="E3/USARTTX---PE3" pad="46"/>
<connect gate="G$1" pin="E4/USART/CSN---PE4/PWMA/CSN" pad="47"/>
<connect gate="G$1" pin="E5/USART/SCK---PE5/PWMB/SCK" pad="48"/>
<connect gate="G$1" pin="E6/USART/RX---PE6/UARTRX/MISO" pad="51"/>
<connect gate="G$1" pin="E7/USART/TX---PE7/UARTTX/MOSI" pad="52"/>
<connect gate="G$1" pin="GND" pad="1 2 7 8 13 14 19 20 25 26 31 32 37 38 43 44 49 50 P$1"/>
<connect gate="G$1" pin="VCC" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="FX8-MOTHER" package="ATK-BFPP-BOTTOM-FX8-60">
<connects>
<connect gate="G$1" pin="+3V3" pad="P$28 P$29"/>
<connect gate="G$1" pin="+5V" pad="P$26 P$27"/>
<connect gate="G$1" pin="A0/AREF---PB0/ADC" pad="P$2"/>
<connect gate="G$1" pin="A1/ADC---PB1/ADC" pad="P$3"/>
<connect gate="G$1" pin="A2/ADC/DAC---PB2/ADC/DAC" pad="P$5"/>
<connect gate="G$1" pin="A3/ADC/DAC---PB3/ADC/DAC" pad="P$6"/>
<connect gate="G$1" pin="A4/ADC---PB4/ADC" pad="P$8"/>
<connect gate="G$1" pin="A5/ADC---PB5/ADC" pad="P$9"/>
<connect gate="G$1" pin="A6/ADC---PB6/ADC/ADCOUT" pad="P$11"/>
<connect gate="G$1" pin="A7/ADC---PB7/ADC/ADCOUT" pad="P$12"/>
<connect gate="G$1" pin="C0/PWMALS---PC0/PWMALS/CSN/SDA" pad="P$59"/>
<connect gate="G$1" pin="C1/PWMAHS---PC1/PWMAHS/SCK/SCL" pad="P$58"/>
<connect gate="G$1" pin="C2/PWMBLS---PC2/PWMBLS/MISO/UARTRX" pad="P$57"/>
<connect gate="G$1" pin="C3/PWMBHS---PC3/PWMBHS/MOSI/UARTTX" pad="P$56"/>
<connect gate="G$1" pin="C4/PWMCLS---PC4/PWMCLS/CSN" pad="P$55"/>
<connect gate="G$1" pin="C5/PWMCHS---PC5/PWMCHS/SCK" pad="P$54"/>
<connect gate="G$1" pin="C6/PWMDLS---PC6/PWMDLS/MISO/UARTRX" pad="P$53"/>
<connect gate="G$1" pin="C7/PWMDHS---PC7/PWMDHS/MOSI/UARTTX" pad="P$52"/>
<connect gate="G$1" pin="D0/USART/CSN---PD0/PWMA/CSN" pad="P$49"/>
<connect gate="G$1" pin="D1/USART/CLK---PD1/PWMB/SCK" pad="P$48"/>
<connect gate="G$1" pin="D2/USART/RX---PD2/PWMC/MISO/UARTRX" pad="P$46"/>
<connect gate="G$1" pin="D3/USART/TX---PD3/PWMD/MOSI/UARTTX" pad="P$45"/>
<connect gate="G$1" pin="D4/PWMELS---PD4" pad="P$15"/>
<connect gate="G$1" pin="D5/PWMEHS---PD5" pad="P$16"/>
<connect gate="G$1" pin="E0/SDA---PE0" pad="P$42"/>
<connect gate="G$1" pin="E1/SCL---PE1" pad="P$40"/>
<connect gate="G$1" pin="E2/USARTRX---PE2" pad="P$19"/>
<connect gate="G$1" pin="E3/USARTTX---PE3" pad="P$20"/>
<connect gate="G$1" pin="E4/USART/CSN---PE4/PWMA/CSN" pad="P$37"/>
<connect gate="G$1" pin="E5/USART/SCK---PE5/PWMB/SCK" pad="P$36"/>
<connect gate="G$1" pin="E6/USART/RX---PE6/UARTRX/MISO" pad="P$34"/>
<connect gate="G$1" pin="E7/USART/TX---PE7/UARTTX/MOSI" pad="P$33"/>
<connect gate="G$1" pin="GND" pad="P$1 P$4 P$7 P$10 P$13 P$14 P$17 P$18 P$21 P$30 P$31 P$32 P$35 P$38 P$39 P$41 P$43 P$44 P$47 P$50 P$51 P$60 P$62"/>
<connect gate="G$1" pin="VCC" pad="P$61"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="FX8-DAUGHTER" package="ATK-BFPP-TOP-FX8-60">
<connects>
<connect gate="G$1" pin="+3V3" pad="P$28 P$29"/>
<connect gate="G$1" pin="+5V" pad="P$26 P$27"/>
<connect gate="G$1" pin="A0/AREF---PB0/ADC" pad="P$2"/>
<connect gate="G$1" pin="A1/ADC---PB1/ADC" pad="P$3"/>
<connect gate="G$1" pin="A2/ADC/DAC---PB2/ADC/DAC" pad="P$5"/>
<connect gate="G$1" pin="A3/ADC/DAC---PB3/ADC/DAC" pad="P$6"/>
<connect gate="G$1" pin="A4/ADC---PB4/ADC" pad="P$8"/>
<connect gate="G$1" pin="A5/ADC---PB5/ADC" pad="P$9"/>
<connect gate="G$1" pin="A6/ADC---PB6/ADC/ADCOUT" pad="P$11"/>
<connect gate="G$1" pin="A7/ADC---PB7/ADC/ADCOUT" pad="P$12"/>
<connect gate="G$1" pin="C0/PWMALS---PC0/PWMALS/CSN/SDA" pad="P$59"/>
<connect gate="G$1" pin="C1/PWMAHS---PC1/PWMAHS/SCK/SCL" pad="P$58"/>
<connect gate="G$1" pin="C2/PWMBLS---PC2/PWMBLS/MISO/UARTRX" pad="P$57"/>
<connect gate="G$1" pin="C3/PWMBHS---PC3/PWMBHS/MOSI/UARTTX" pad="P$56"/>
<connect gate="G$1" pin="C4/PWMCLS---PC4/PWMCLS/CSN" pad="P$55"/>
<connect gate="G$1" pin="C5/PWMCHS---PC5/PWMCHS/SCK" pad="P$54"/>
<connect gate="G$1" pin="C6/PWMDLS---PC6/PWMDLS/MISO/UARTRX" pad="P$53"/>
<connect gate="G$1" pin="C7/PWMDHS---PC7/PWMDHS/MOSI/UARTTX" pad="P$52"/>
<connect gate="G$1" pin="D0/USART/CSN---PD0/PWMA/CSN" pad="P$49"/>
<connect gate="G$1" pin="D1/USART/CLK---PD1/PWMB/SCK" pad="P$48"/>
<connect gate="G$1" pin="D2/USART/RX---PD2/PWMC/MISO/UARTRX" pad="P$46"/>
<connect gate="G$1" pin="D3/USART/TX---PD3/PWMD/MOSI/UARTTX" pad="P$45"/>
<connect gate="G$1" pin="D4/PWMELS---PD4" pad="P$15"/>
<connect gate="G$1" pin="D5/PWMEHS---PD5" pad="P$16"/>
<connect gate="G$1" pin="E0/SDA---PE0" pad="P$42"/>
<connect gate="G$1" pin="E1/SCL---PE1" pad="P$40"/>
<connect gate="G$1" pin="E2/USARTRX---PE2" pad="P$19"/>
<connect gate="G$1" pin="E3/USARTTX---PE3" pad="P$20"/>
<connect gate="G$1" pin="E4/USART/CSN---PE4/PWMA/CSN" pad="P$37"/>
<connect gate="G$1" pin="E5/USART/SCK---PE5/PWMB/SCK" pad="P$36"/>
<connect gate="G$1" pin="E6/USART/RX---PE6/UARTRX/MISO" pad="P$34"/>
<connect gate="G$1" pin="E7/USART/TX---PE7/UARTTX/MOSI" pad="P$33"/>
<connect gate="G$1" pin="GND" pad="P$1 P$4 P$7 P$10 P$13 P$14 P$17 P$18 P$21 P$30 P$31 P$32 P$35 P$38 P$39 P$41 P$43 P$44 P$47 P$50 P$51 P$60 P$62"/>
<connect gate="G$1" pin="VCC" pad="P$61"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="FX8-THRU" package="ATK-BFPP-THRU-FX8-60">
<connects>
<connect gate="G$1" pin="+3V3" pad="P$28 P$29 P$90 P$91 P$150 P$151"/>
<connect gate="G$1" pin="+5V" pad="P$26 P$27 P$88 P$89 P$148 P$149"/>
<connect gate="G$1" pin="A0/AREF---PB0/ADC" pad="P$2 P$64 P$124"/>
<connect gate="G$1" pin="A1/ADC---PB1/ADC" pad="P$3 P$65 P$125"/>
<connect gate="G$1" pin="A2/ADC/DAC---PB2/ADC/DAC" pad="P$5 P$67 P$127"/>
<connect gate="G$1" pin="A3/ADC/DAC---PB3/ADC/DAC" pad="P$6 P$68 P$128"/>
<connect gate="G$1" pin="A4/ADC---PB4/ADC" pad="P$8 P$70 P$130"/>
<connect gate="G$1" pin="A5/ADC---PB5/ADC" pad="P$9 P$71 P$131"/>
<connect gate="G$1" pin="A6/ADC---PB6/ADC/ADCOUT" pad="P$11 P$73 P$133"/>
<connect gate="G$1" pin="A7/ADC---PB7/ADC/ADCOUT" pad="P$12 P$74 P$134"/>
<connect gate="G$1" pin="C0/PWMALS---PC0/PWMALS/CSN/SDA" pad="P$59 P$121 P$181"/>
<connect gate="G$1" pin="C1/PWMAHS---PC1/PWMAHS/SCK/SCL" pad="P$58 P$120 P$180"/>
<connect gate="G$1" pin="C2/PWMBLS---PC2/PWMBLS/MISO/UARTRX" pad="P$57 P$119 P$179"/>
<connect gate="G$1" pin="C3/PWMBHS---PC3/PWMBHS/MOSI/UARTTX" pad="P$56 P$118 P$178"/>
<connect gate="G$1" pin="C4/PWMCLS---PC4/PWMCLS/CSN" pad="P$55 P$117 P$177"/>
<connect gate="G$1" pin="C5/PWMCHS---PC5/PWMCHS/SCK" pad="P$54 P$116 P$176"/>
<connect gate="G$1" pin="C6/PWMDLS---PC6/PWMDLS/MISO/UARTRX" pad="P$53 P$115 P$175"/>
<connect gate="G$1" pin="C7/PWMDHS---PC7/PWMDHS/MOSI/UARTTX" pad="P$52 P$114 P$174"/>
<connect gate="G$1" pin="D0/USART/CSN---PD0/PWMA/CSN" pad="P$49 P$111 P$171"/>
<connect gate="G$1" pin="D1/USART/CLK---PD1/PWMB/SCK" pad="P$48 P$110 P$170"/>
<connect gate="G$1" pin="D2/USART/RX---PD2/PWMC/MISO/UARTRX" pad="P$46 P$108 P$168"/>
<connect gate="G$1" pin="D3/USART/TX---PD3/PWMD/MOSI/UARTTX" pad="P$45 P$107 P$167"/>
<connect gate="G$1" pin="D4/PWMELS---PD4" pad="P$15 P$77 P$137"/>
<connect gate="G$1" pin="D5/PWMEHS---PD5" pad="P$16 P$78 P$138"/>
<connect gate="G$1" pin="E0/SDA---PE0" pad="P$42 P$104 P$164"/>
<connect gate="G$1" pin="E1/SCL---PE1" pad="P$40 P$102 P$162"/>
<connect gate="G$1" pin="E2/USARTRX---PE2" pad="P$19 P$81 P$141"/>
<connect gate="G$1" pin="E3/USARTTX---PE3" pad="P$20 P$82 P$142"/>
<connect gate="G$1" pin="E4/USART/CSN---PE4/PWMA/CSN" pad="P$37 P$99 P$159"/>
<connect gate="G$1" pin="E5/USART/SCK---PE5/PWMB/SCK" pad="P$36 P$98 P$158"/>
<connect gate="G$1" pin="E6/USART/RX---PE6/UARTRX/MISO" pad="P$34 P$96 P$156"/>
<connect gate="G$1" pin="E7/USART/TX---PE7/UARTTX/MOSI" pad="P$33 P$95 P$155"/>
<connect gate="G$1" pin="GND" pad="P$1 P$4 P$7 P$10 P$13 P$14 P$17 P$18 P$21 P$30 P$31 P$32 P$35 P$38 P$39 P$41 P$43 P$44 P$47 P$50 P$51 P$60 P$62 P$63 P$66 P$69 P$72 P$75 P$76 P$79 P$80 P$83 P$92 P$93 P$94 P$97 P$100 P$101 P$103 P$105 P$106 P$109 P$112 P$113 P$122 P$123 P$126 P$129 P$132 P$135 P$136 P$139 P$140 P$143 P$152 P$153 P$154 P$157 P$160 P$161 P$163 P$165 P$166 P$169 P$172 P$173 P$182"/>
<connect gate="G$1" pin="VCC" pad="P$61"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="JST_6_PIN_HORIZONTAL" prefix="J">
<description>&lt;h3&gt;JST 6 pin horizontal connector&lt;/h3&gt;
JST-SH type.

&lt;p&gt;&lt;/p&gt;
&lt;b&gt;Here is the connector we sell at SparkFun:&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="link"&gt;name&lt;/a&gt; (XXX-00000)&lt;/li&gt;
&lt;li&gt;&lt;a href="http://www.sparkfun.com/datasheets/GPS/EM406-SMDConnector-eSH.pdf"&gt;Datasheet&lt;/a&gt;
&lt;/ul&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt;It was used on these SparkFun products:&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="link"&gt;name&lt;/a&gt; (XXX-00000)&lt;/li&gt;
&lt;li&gt;&lt;a href="link"&gt;name&lt;/a&gt; (XXX-00000)&lt;/li&gt;
&lt;/ul&gt;</description>
<gates>
<gate name="A" symbol="CONN_06" x="0" y="0"/>
</gates>
<devices>
<device name="" package="JST-6-SMD-HORI-1.0MM">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="5" pad="5"/>
<connect gate="A" pin="6" pad="6"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="XXX-00000" constant="no"/>
<attribute name="VALUE" value="BM06B-SRSS-TB" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="M3POWERCONN" prefix="J">
<gates>
<gate name="G$1" symbol="CONN_01" x="0" y="0"/>
</gates>
<devices>
<device name="" package="M3POWERCONN">
<connects>
<connect gate="G$1" pin="2" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="passives">
<packages>
<package name="0805-DIODE">
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.889" y="1.397" size="0.3048" layer="25">&gt;NAME</text>
<text x="-1.016" y="-1.143" size="0.3048" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<wire x1="-0.1778" y1="0.4318" x2="0.1778" y2="0" width="0.127" layer="21"/>
<wire x1="0.1778" y1="0" x2="-0.1778" y2="-0.4318" width="0.127" layer="21"/>
<wire x1="-0.1778" y1="0.4318" x2="-0.1778" y2="-0.4318" width="0.127" layer="21"/>
</package>
<package name="SOD123">
<description>&lt;b&gt;SMALL OUTLINE DIODE&lt;/b&gt;</description>
<wire x1="-2.973" y1="0.983" x2="2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-0.983" x2="-2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-0.983" x2="-2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="0.983" x2="2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.321" y1="0.787" x2="1.321" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-1.321" y1="-0.787" x2="1.321" y2="-0.787" width="0.1016" layer="51"/>
<wire x1="-1.321" y1="-0.787" x2="-1.321" y2="0.787" width="0.1016" layer="51"/>
<wire x1="1.321" y1="-0.787" x2="1.321" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-1" y1="0" x2="0" y2="0.5" width="0.2032" layer="51"/>
<wire x1="0" y1="0.5" x2="0" y2="-0.5" width="0.2032" layer="51"/>
<wire x1="0" y1="-0.5" x2="-1" y2="0" width="0.2032" layer="51"/>
<wire x1="-1" y1="0.5" x2="-1" y2="0" width="0.2032" layer="51"/>
<wire x1="-1" y1="0" x2="-1" y2="-0.5" width="0.2032" layer="51"/>
<smd name="CATHODE" x="-1.7" y="0" dx="1.6" dy="0.8" layer="1"/>
<smd name="ANODE" x="1.7" y="0" dx="1.6" dy="0.8" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.9558" y1="-0.3048" x2="-1.3716" y2="0.3048" layer="51" rot="R180"/>
<rectangle x1="1.3716" y1="-0.3048" x2="1.9558" y2="0.3048" layer="51" rot="R180"/>
<rectangle x1="-0.4001" y1="-0.7" x2="0.4001" y2="0.7" layer="35"/>
<wire x1="-2.667" y1="0.889" x2="-2.667" y2="-0.889" width="0.127" layer="21"/>
<wire x1="-2.921" y1="0.889" x2="-2.921" y2="-0.889" width="0.127" layer="21"/>
<wire x1="-2.921" y1="-0.889" x2="2.794" y2="-0.889" width="0.127" layer="21"/>
<wire x1="2.794" y1="-0.889" x2="2.794" y2="0.889" width="0.127" layer="21"/>
<wire x1="2.794" y1="0.889" x2="-2.921" y2="0.889" width="0.127" layer="21"/>
</package>
<package name="SOD-123HE">
<smd name="P$1" x="0.8" y="0" dx="2.4" dy="1.4" layer="1"/>
<smd name="P$2" x="-1.55" y="0" dx="0.9" dy="1.4" layer="1"/>
<wire x1="-1.4" y1="-0.9" x2="0" y2="-0.9" width="0.127" layer="51"/>
<wire x1="0" y1="-0.9" x2="0.9" y2="-0.9" width="0.127" layer="51"/>
<wire x1="0.9" y1="-0.9" x2="1.4" y2="-0.9" width="0.127" layer="51"/>
<wire x1="1.4" y1="-0.9" x2="1.4" y2="0.9" width="0.127" layer="51"/>
<wire x1="1.4" y1="0.9" x2="0.9" y2="0.9" width="0.127" layer="51"/>
<wire x1="0.9" y1="0.9" x2="0" y2="0.9" width="0.127" layer="51"/>
<wire x1="0" y1="0.9" x2="-1.4" y2="0.9" width="0.127" layer="51"/>
<wire x1="-1.4" y1="0.9" x2="-1.4" y2="-0.9" width="0.127" layer="51"/>
<wire x1="0.9" y1="0.9" x2="0.9" y2="0" width="0.127" layer="51"/>
<wire x1="0.9" y1="0" x2="0.9" y2="-0.9" width="0.127" layer="51"/>
<wire x1="0.9" y1="0" x2="0" y2="0.9" width="0.127" layer="51"/>
<wire x1="0" y1="0.9" x2="0" y2="-0.9" width="0.127" layer="51"/>
<wire x1="0" y1="-0.9" x2="0.9" y2="0" width="0.127" layer="51"/>
<wire x1="1.4" y1="0.9" x2="0.5" y2="0.9" width="0.127" layer="21"/>
<wire x1="1.4" y1="-0.9" x2="0.5" y2="-0.9" width="0.127" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="D">
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-1.27" width="0.254" layer="94"/>
<text x="2.54" y="0.4826" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-2.3114" size="1.778" layer="96">&gt;VALUE</text>
<pin name="A" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
<pin name="C" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="DIODE" prefix="D" uservalue="yes">
<description>&lt;B&gt;DIODE&lt;/B&gt;&lt;p&gt;
high speed (Philips)</description>
<gates>
<gate name="G$1" symbol="D" x="0" y="0"/>
</gates>
<devices>
<device name="SOD123" package="SOD123">
<connects>
<connect gate="G$1" pin="A" pad="ANODE"/>
<connect gate="G$1" pin="C" pad="CATHODE"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="DIODE" package="0805-DIODE">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="C" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOD123HE" package="SOD-123HE">
<connects>
<connect gate="G$1" pin="A" pad="P$2"/>
<connect gate="G$1" pin="C" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="P+6" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="GND20" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="R17" library="borkedlabs-passives" deviceset="RESISTOR" device="0805-RES" value="2R2"/>
<part name="R18" library="borkedlabs-passives" deviceset="RESISTOR" device="0805-RES" value="2R2"/>
<part name="P+7" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="GND23" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="R24" library="borkedlabs-passives" deviceset="RESISTOR" device="0805-RES" value="2R2"/>
<part name="R25" library="borkedlabs-passives" deviceset="RESISTOR" device="0805-RES" value="2R2"/>
<part name="R21" library="borkedlabs-passives" deviceset="RESISTOR" device="0805-RES" value="5K6"/>
<part name="R20" library="borkedlabs-passives" deviceset="RESISTOR" device="0805-RES" value="39K"/>
<part name="GND21" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="R23" library="borkedlabs-passives" deviceset="RESISTOR" device="0805-RES" value="5K6"/>
<part name="R22" library="borkedlabs-passives" deviceset="RESISTOR" device="0805-RES" value="39K"/>
<part name="GND22" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="P+10" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="GND27" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="R31" library="borkedlabs-passives" deviceset="RESISTOR" device="0805-RES" value="2R2"/>
<part name="R32" library="borkedlabs-passives" deviceset="RESISTOR" device="0805-RES" value="2R2"/>
<part name="R28" library="borkedlabs-passives" deviceset="RESISTOR" device="0805-RES" value="5K6"/>
<part name="R27" library="borkedlabs-passives" deviceset="RESISTOR" device="0805-RES" value="39K"/>
<part name="GND24" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="U5" library="power" deviceset="DRV8302" device=""/>
<part name="GND15" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="C28" library="borkedlabs-passives" deviceset="CAP" device="0603-CAP" value="0.1uF"/>
<part name="C29" library="borkedlabs-passives" deviceset="CAP" device="0603-CAP" value="0.1uF"/>
<part name="C30" library="borkedlabs-passives" deviceset="CAP" device="0603-CAP" value="0.1uF"/>
<part name="C26" library="borkedlabs-passives" deviceset="CAP" device="0805" value="1nF"/>
<part name="C27" library="borkedlabs-passives" deviceset="CAP" device="0805" value="1nF"/>
<part name="R12" library="borkedlabs-passives" deviceset="RESISTOR" device="0805-RES" value="R100"/>
<part name="R13" library="borkedlabs-passives" deviceset="RESISTOR" device="0805-RES" value="R100"/>
<part name="R14" library="borkedlabs-passives" deviceset="RESISTOR" device="0805-RES" value="R100"/>
<part name="R15" library="borkedlabs-passives" deviceset="RESISTOR" device="0805-RES" value="R100"/>
<part name="C22" library="borkedlabs-passives" deviceset="CAP" device="0603-CAP" value="0.1uF"/>
<part name="C23" library="borkedlabs-passives" deviceset="CAP" device="1206" value="10uF"/>
<part name="C20" library="borkedlabs-passives" deviceset="CAP" device="1206" value="10uF"/>
<part name="C18" library="borkedlabs-passives" deviceset="CAP" device="0805" value="22nF"/>
<part name="GND13" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="C15" library="borkedlabs-passives" deviceset="CAP" device="1206" value="10uF"/>
<part name="GND10" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="C17" library="borkedlabs-passives" deviceset="CAP" device="1206" value="10uF"/>
<part name="GND11" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="R10" library="borkedlabs-passives" deviceset="RESISTOR" device="0805-RES" value="10k"/>
<part name="GND12" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND17" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="R9" library="borkedlabs-passives" deviceset="RESISTOR" device="0805-RES" value="10k"/>
<part name="P+4" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="+3V38" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="P+8" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="GND25" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="+3V37" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="C31" library="borkedlabs-passives" deviceset="CAP" device="0805" value="10nF"/>
<part name="C33" library="borkedlabs-passives" deviceset="CAP" device="0805" value="10nF"/>
<part name="C37" library="borkedlabs-passives" deviceset="CAP" device="0805" value="10nF"/>
<part name="R30" library="borkedlabs-passives" deviceset="RESISTOR" device="0805-RES" value="5K6"/>
<part name="R29" library="borkedlabs-passives" deviceset="RESISTOR" device="0805-RES" value="39K"/>
<part name="GND26" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="C38" library="borkedlabs-passives" deviceset="CAP" device="0805" value="10nF"/>
<part name="P+9" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="C10" library="borkedlabs-passives" deviceset="CAP" device="0603-CAP" value="0.1uF"/>
<part name="+3V35" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="GND8" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="S2" library="fab" deviceset="SLIDE-SWITCH" device="SMT" value="SLIDE-SWITCHSMT"/>
<part name="R8" library="borkedlabs-passives" deviceset="RESISTOR" device="0805-RES" value="10k"/>
<part name="GND7" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="+3V312" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="+3V313" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="+3V314" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="+3V315" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="D4" library="passives" deviceset="DIODE" device="DIODE" value="1A/23V/620mV"/>
<part name="D5" library="passives" deviceset="DIODE" device="DIODE" value="1A/23V/620mV"/>
<part name="D6" library="passives" deviceset="DIODE" device="DIODE" value="1A/23V/620mV"/>
<part name="D7" library="passives" deviceset="DIODE" device="DIODE" value="1A/23V/620mV"/>
<part name="R11" library="borkedlabs-passives" deviceset="RESISTOR" device="0805-RES" value="10k"/>
<part name="GND16" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="C25" library="borkedlabs-passives" deviceset="CAP" device="0603-CAP" value="0.1uF"/>
<part name="R19" library="power" deviceset="R_SHUNT" device="2512"/>
<part name="R26" library="power" deviceset="R_SHUNT" device="2512"/>
<part name="C24" library="borkedlabs-passives" deviceset="CAP" device="1206" value="10uF"/>
<part name="C32" library="borkedlabs-passives" deviceset="CAP" device="1206" value="10uF"/>
<part name="C34" library="borkedlabs-passives" deviceset="CAP" device="1206" value="10uF"/>
<part name="C35" library="borkedlabs-passives" deviceset="CAP" device="1206" value="10uF"/>
<part name="J22" library="connector" deviceset="FIDUCIAL" device="RCT" value="FIDUCIALRCT"/>
<part name="J1" library="connector" deviceset="FIDUCIAL" device="RCT" value="FIDUCIALRCT"/>
<part name="P+5" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="GND18" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="J10" library="SparkFun-Connectors" deviceset="CONN_02" device="1X02_NO_SILK"/>
<part name="Q1" library="power" deviceset="NFET-TPW4R008NH" device=""/>
<part name="Q2" library="power" deviceset="NFET-TPW4R008NH" device=""/>
<part name="Q6" library="power" deviceset="NFET-TPW4R008NH" device=""/>
<part name="Q5" library="power" deviceset="NFET-TPW4R008NH" device=""/>
<part name="Q10" library="power" deviceset="NFET-TPW4R008NH" device=""/>
<part name="Q9" library="power" deviceset="NFET-TPW4R008NH" device=""/>
<part name="C16" library="borkedlabs-passives" deviceset="CAP" device="0603-CAP" value="0.1uF"/>
<part name="J21" library="power" deviceset="SL-LINK" device=""/>
<part name="Q4" library="power" deviceset="NFET-TPW4R008NH" device=""/>
<part name="Q3" library="power" deviceset="NFET-TPW4R008NH" device=""/>
<part name="Q7" library="power" deviceset="NFET-TPW4R008NH" device=""/>
<part name="Q8" library="power" deviceset="NFET-TPW4R008NH" device=""/>
<part name="Q11" library="power" deviceset="NFET-TPW4R008NH" device=""/>
<part name="Q12" library="power" deviceset="NFET-TPW4R008NH" device=""/>
<part name="C36" library="borkedlabs-passives" deviceset="CAP" device="1206" value="10uF"/>
<part name="J24" library="connector" deviceset="ATK-BFPP" device="FX8-DAUGHTER"/>
<part name="J2" library="connector" deviceset="JST_6_PIN_HORIZONTAL" device="" value="BM06B-SRSS-TB"/>
<part name="GND1" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="P+1" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="GND2" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="+3V1" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="J3" library="power" deviceset="PWRPAD" device="M3"/>
<part name="J4" library="power" deviceset="PWRPAD" device="M3"/>
<part name="J5" library="power" deviceset="PWRPAD" device="M3"/>
<part name="J6" library="power" deviceset="PWRPAD" device="M3"/>
<part name="+3V2" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="C1" library="borkedlabs-passives" deviceset="CAP" device="0603-CAP" value="0.1uF"/>
<part name="C2" library="borkedlabs-passives" deviceset="CAP-POL" device="-6.6X6.6" value="150uF"/>
<part name="C3" library="borkedlabs-passives" deviceset="CAP-POL" device="-6.6X6.6" value="150uF"/>
<part name="C4" library="borkedlabs-passives" deviceset="CAP-POL" device="-6.6X6.6" value="150uF"/>
<part name="C5" library="borkedlabs-passives" deviceset="CAP-POL" device="-6.6X6.6" value="150uF"/>
<part name="C7" library="borkedlabs-passives" deviceset="CAP" device="1206" value="10uF"/>
<part name="C8" library="borkedlabs-passives" deviceset="CAP" device="1206" value="10uF"/>
<part name="C9" library="borkedlabs-passives" deviceset="CAP" device="1206" value="10uF"/>
<part name="C11" library="borkedlabs-passives" deviceset="CAP" device="1206" value="10uF"/>
<part name="C12" library="borkedlabs-passives" deviceset="CAP" device="1206" value="10uF"/>
<part name="C14" library="borkedlabs-passives" deviceset="CAP" device="1206" value="10uF"/>
<part name="C19" library="borkedlabs-passives" deviceset="CAP" device="1206" value="10uF"/>
<part name="C21" library="borkedlabs-passives" deviceset="CAP" device="1206" value="10uF"/>
<part name="C39" library="borkedlabs-passives" deviceset="CAP" device="1206" value="10uF"/>
<part name="C40" library="borkedlabs-passives" deviceset="CAP" device="1206" value="10uF"/>
<part name="J7" library="connector" deviceset="M3POWERCONN" device=""/>
<part name="J8" library="connector" deviceset="M3POWERCONN" device=""/>
<part name="J9" library="connector" deviceset="M3POWERCONN" device=""/>
</parts>
<sheets>
<sheet>
<plain>
<text x="525.78" y="99.06" size="1.778" layer="97">hack for eagle trace routing specific gnd</text>
</plain>
<instances>
<instance part="P+6" gate="VCC" x="383.54" y="160.02" smashed="yes">
<attribute name="VALUE" x="381" y="157.48" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND20" gate="1" x="383.54" y="83.82" smashed="yes">
<attribute name="VALUE" x="381" y="81.28" size="1.778" layer="96"/>
</instance>
<instance part="R17" gate="G$1" x="368.3" y="137.16" smashed="yes">
<attribute name="NAME" x="364.49" y="138.6586" size="1.778" layer="95"/>
<attribute name="VALUE" x="364.49" y="133.858" size="1.778" layer="96"/>
<attribute name="PRECISION" x="364.49" y="130.302" size="1.27" layer="97"/>
<attribute name="PACKAGE" x="364.49" y="132.08" size="1.27" layer="97"/>
</instance>
<instance part="R18" gate="G$1" x="368.3" y="116.84" smashed="yes">
<attribute name="NAME" x="364.49" y="118.3386" size="1.778" layer="95"/>
<attribute name="VALUE" x="364.49" y="113.538" size="1.778" layer="96"/>
<attribute name="PRECISION" x="364.49" y="109.982" size="1.27" layer="97"/>
<attribute name="PACKAGE" x="364.49" y="111.76" size="1.27" layer="97"/>
</instance>
<instance part="P+7" gate="VCC" x="477.52" y="160.02" smashed="yes">
<attribute name="VALUE" x="474.98" y="157.48" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND23" gate="1" x="477.52" y="83.82" smashed="yes">
<attribute name="VALUE" x="474.98" y="81.28" size="1.778" layer="96"/>
</instance>
<instance part="R24" gate="G$1" x="462.28" y="137.16" smashed="yes">
<attribute name="NAME" x="458.47" y="138.6586" size="1.778" layer="95"/>
<attribute name="VALUE" x="458.47" y="133.858" size="1.778" layer="96"/>
<attribute name="PRECISION" x="458.47" y="130.302" size="1.27" layer="97"/>
<attribute name="PACKAGE" x="458.47" y="132.08" size="1.27" layer="97"/>
</instance>
<instance part="R25" gate="G$1" x="462.28" y="116.84" smashed="yes">
<attribute name="NAME" x="458.47" y="118.3386" size="1.778" layer="95"/>
<attribute name="VALUE" x="458.47" y="113.538" size="1.778" layer="96"/>
<attribute name="PRECISION" x="458.47" y="109.982" size="1.27" layer="97"/>
<attribute name="PACKAGE" x="458.47" y="111.76" size="1.27" layer="97"/>
</instance>
<instance part="R21" gate="G$1" x="383.54" y="12.7" smashed="yes" rot="R90">
<attribute name="NAME" x="382.0414" y="8.89" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="386.842" y="8.89" size="1.778" layer="96" rot="R90"/>
<attribute name="PRECISION" x="390.398" y="8.89" size="1.27" layer="97" rot="R90"/>
<attribute name="PACKAGE" x="388.62" y="8.89" size="1.27" layer="97" rot="R90"/>
</instance>
<instance part="R20" gate="G$1" x="383.54" y="27.94" smashed="yes" rot="R90">
<attribute name="NAME" x="382.0414" y="24.13" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="386.842" y="24.13" size="1.778" layer="96" rot="R90"/>
<attribute name="PRECISION" x="390.398" y="24.13" size="1.27" layer="97" rot="R90"/>
<attribute name="PACKAGE" x="388.62" y="24.13" size="1.27" layer="97" rot="R90"/>
</instance>
<instance part="GND21" gate="1" x="383.54" y="0" smashed="yes">
<attribute name="VALUE" x="381" y="-2.54" size="1.778" layer="96"/>
</instance>
<instance part="R23" gate="G$1" x="436.88" y="12.7" smashed="yes" rot="R90">
<attribute name="NAME" x="435.3814" y="8.89" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="440.182" y="8.89" size="1.778" layer="96" rot="R90"/>
<attribute name="PRECISION" x="443.738" y="8.89" size="1.27" layer="97" rot="R90"/>
<attribute name="PACKAGE" x="441.96" y="8.89" size="1.27" layer="97" rot="R90"/>
</instance>
<instance part="R22" gate="G$1" x="436.88" y="27.94" smashed="yes" rot="R90">
<attribute name="NAME" x="435.3814" y="24.13" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="440.182" y="24.13" size="1.778" layer="96" rot="R90"/>
<attribute name="PRECISION" x="443.738" y="24.13" size="1.27" layer="97" rot="R90"/>
<attribute name="PACKAGE" x="441.96" y="24.13" size="1.27" layer="97" rot="R90"/>
</instance>
<instance part="GND22" gate="1" x="436.88" y="0" smashed="yes">
<attribute name="VALUE" x="434.34" y="-2.54" size="1.778" layer="96"/>
</instance>
<instance part="P+10" gate="VCC" x="574.04" y="160.02" smashed="yes">
<attribute name="VALUE" x="571.5" y="157.48" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND27" gate="1" x="574.04" y="78.74" smashed="yes">
<attribute name="VALUE" x="571.5" y="76.2" size="1.778" layer="96"/>
</instance>
<instance part="R31" gate="G$1" x="558.8" y="137.16" smashed="yes">
<attribute name="NAME" x="554.99" y="138.6586" size="1.778" layer="95"/>
<attribute name="VALUE" x="554.99" y="133.858" size="1.778" layer="96"/>
<attribute name="PRECISION" x="554.99" y="130.302" size="1.27" layer="97"/>
<attribute name="PACKAGE" x="554.99" y="132.08" size="1.27" layer="97"/>
</instance>
<instance part="R32" gate="G$1" x="558.8" y="116.84" smashed="yes">
<attribute name="NAME" x="554.99" y="118.3386" size="1.778" layer="95"/>
<attribute name="VALUE" x="554.99" y="113.538" size="1.778" layer="96"/>
<attribute name="PRECISION" x="554.99" y="109.982" size="1.27" layer="97"/>
<attribute name="PACKAGE" x="554.99" y="111.76" size="1.27" layer="97"/>
</instance>
<instance part="R28" gate="G$1" x="490.22" y="12.7" smashed="yes" rot="R90">
<attribute name="NAME" x="488.7214" y="8.89" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="493.522" y="8.89" size="1.778" layer="96" rot="R90"/>
<attribute name="PRECISION" x="497.078" y="8.89" size="1.27" layer="97" rot="R90"/>
<attribute name="PACKAGE" x="495.3" y="8.89" size="1.27" layer="97" rot="R90"/>
</instance>
<instance part="R27" gate="G$1" x="490.22" y="27.94" smashed="yes" rot="R90">
<attribute name="NAME" x="488.7214" y="24.13" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="493.522" y="24.13" size="1.778" layer="96" rot="R90"/>
<attribute name="PRECISION" x="497.078" y="24.13" size="1.27" layer="97" rot="R90"/>
<attribute name="PACKAGE" x="495.3" y="24.13" size="1.27" layer="97" rot="R90"/>
</instance>
<instance part="GND24" gate="1" x="490.22" y="0" smashed="yes">
<attribute name="VALUE" x="487.68" y="-2.54" size="1.778" layer="96"/>
</instance>
<instance part="U5" gate="G$1" x="213.36" y="93.98" smashed="yes"/>
<instance part="GND15" gate="1" x="213.36" y="10.16" smashed="yes">
<attribute name="VALUE" x="210.82" y="7.62" size="1.778" layer="96"/>
</instance>
<instance part="C28" gate="G$1" x="256.54" y="101.6" smashed="yes">
<attribute name="NAME" x="258.064" y="104.521" size="1.778" layer="95"/>
<attribute name="VALUE" x="258.064" y="99.441" size="1.778" layer="96"/>
<attribute name="PACKAGE" x="258.064" y="97.536" size="1.27" layer="97"/>
<attribute name="VOLTAGE" x="258.064" y="95.758" size="1.27" layer="97"/>
<attribute name="TYPE" x="258.064" y="93.98" size="1.27" layer="97"/>
</instance>
<instance part="C29" gate="G$1" x="256.54" y="88.9" smashed="yes">
<attribute name="NAME" x="258.064" y="91.821" size="1.778" layer="95"/>
<attribute name="VALUE" x="258.064" y="86.741" size="1.778" layer="96"/>
<attribute name="PACKAGE" x="258.064" y="84.836" size="1.27" layer="97"/>
<attribute name="VOLTAGE" x="258.064" y="83.058" size="1.27" layer="97"/>
<attribute name="TYPE" x="258.064" y="81.28" size="1.27" layer="97"/>
</instance>
<instance part="C30" gate="G$1" x="256.54" y="76.2" smashed="yes">
<attribute name="NAME" x="258.064" y="79.121" size="1.778" layer="95"/>
<attribute name="VALUE" x="258.064" y="74.041" size="1.778" layer="96"/>
<attribute name="PACKAGE" x="258.064" y="72.136" size="1.27" layer="97"/>
<attribute name="VOLTAGE" x="258.064" y="70.358" size="1.27" layer="97"/>
<attribute name="TYPE" x="258.064" y="68.58" size="1.27" layer="97"/>
</instance>
<instance part="C26" gate="G$1" x="248.92" y="63.5" smashed="yes">
<attribute name="NAME" x="250.444" y="66.421" size="1.778" layer="95"/>
<attribute name="VALUE" x="250.444" y="61.341" size="1.778" layer="96"/>
<attribute name="PACKAGE" x="250.444" y="59.436" size="1.27" layer="97"/>
<attribute name="VOLTAGE" x="250.444" y="57.658" size="1.27" layer="97"/>
<attribute name="TYPE" x="250.444" y="55.88" size="1.27" layer="97"/>
</instance>
<instance part="C27" gate="G$1" x="248.92" y="48.26" smashed="yes">
<attribute name="NAME" x="250.444" y="51.181" size="1.778" layer="95"/>
<attribute name="VALUE" x="250.444" y="46.101" size="1.778" layer="96"/>
<attribute name="PACKAGE" x="250.444" y="44.196" size="1.27" layer="97"/>
<attribute name="VOLTAGE" x="250.444" y="42.418" size="1.27" layer="97"/>
<attribute name="TYPE" x="250.444" y="40.64" size="1.27" layer="97"/>
</instance>
<instance part="R12" gate="G$1" x="264.16" y="68.58" smashed="yes" rot="R180">
<attribute name="NAME" x="267.97" y="67.0814" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="267.97" y="71.882" size="1.778" layer="96" rot="R180"/>
<attribute name="PRECISION" x="267.97" y="75.438" size="1.27" layer="97" rot="R180"/>
<attribute name="PACKAGE" x="267.97" y="73.66" size="1.27" layer="97" rot="R180"/>
</instance>
<instance part="R13" gate="G$1" x="264.16" y="60.96" smashed="yes" rot="R180">
<attribute name="NAME" x="267.97" y="59.4614" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="267.97" y="64.262" size="1.778" layer="96" rot="R180"/>
<attribute name="PRECISION" x="267.97" y="67.818" size="1.27" layer="97" rot="R180"/>
<attribute name="PACKAGE" x="267.97" y="66.04" size="1.27" layer="97" rot="R180"/>
</instance>
<instance part="R14" gate="G$1" x="264.16" y="53.34" smashed="yes" rot="R180">
<attribute name="NAME" x="267.97" y="51.8414" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="267.97" y="56.642" size="1.778" layer="96" rot="R180"/>
<attribute name="PRECISION" x="267.97" y="60.198" size="1.27" layer="97" rot="R180"/>
<attribute name="PACKAGE" x="267.97" y="58.42" size="1.27" layer="97" rot="R180"/>
</instance>
<instance part="R15" gate="G$1" x="264.16" y="45.72" smashed="yes" rot="R180">
<attribute name="NAME" x="267.97" y="44.2214" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="267.97" y="49.022" size="1.778" layer="96" rot="R180"/>
<attribute name="PRECISION" x="267.97" y="52.578" size="1.27" layer="97" rot="R180"/>
<attribute name="PACKAGE" x="267.97" y="50.8" size="1.27" layer="97" rot="R180"/>
</instance>
<instance part="C22" gate="G$1" x="228.6" y="48.26" smashed="yes" rot="R270">
<attribute name="NAME" x="231.521" y="46.736" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="226.441" y="46.736" size="1.778" layer="96" rot="R270"/>
<attribute name="PACKAGE" x="224.536" y="46.736" size="1.27" layer="97" rot="R270"/>
<attribute name="VOLTAGE" x="222.758" y="46.736" size="1.27" layer="97" rot="R270"/>
<attribute name="TYPE" x="220.98" y="46.736" size="1.27" layer="97" rot="R270"/>
</instance>
<instance part="C23" gate="G$1" x="228.6" y="30.48" smashed="yes" rot="R270">
<attribute name="NAME" x="231.521" y="28.956" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="226.441" y="28.956" size="1.778" layer="96" rot="R270"/>
<attribute name="PACKAGE" x="224.536" y="28.956" size="1.27" layer="97" rot="R270"/>
<attribute name="VOLTAGE" x="222.758" y="28.956" size="1.27" layer="97" rot="R270"/>
<attribute name="TYPE" x="220.98" y="28.956" size="1.27" layer="97" rot="R270"/>
</instance>
<instance part="C20" gate="G$1" x="187.96" y="58.42" smashed="yes" rot="R180">
<attribute name="NAME" x="186.436" y="55.499" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="186.436" y="60.579" size="1.778" layer="96" rot="R180"/>
<attribute name="PACKAGE" x="186.436" y="62.484" size="1.27" layer="97" rot="R180"/>
<attribute name="VOLTAGE" x="186.436" y="64.262" size="1.27" layer="97" rot="R180"/>
<attribute name="TYPE" x="186.436" y="66.04" size="1.27" layer="97" rot="R180"/>
</instance>
<instance part="C18" gate="G$1" x="170.18" y="93.98" smashed="yes" rot="R270">
<attribute name="NAME" x="173.101" y="92.456" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="168.021" y="92.456" size="1.778" layer="96" rot="R270"/>
<attribute name="PACKAGE" x="166.116" y="92.456" size="1.27" layer="97" rot="R270"/>
<attribute name="VOLTAGE" x="164.338" y="92.456" size="1.27" layer="97" rot="R270"/>
<attribute name="TYPE" x="162.56" y="92.456" size="1.27" layer="97" rot="R270"/>
</instance>
<instance part="GND13" gate="1" x="147.32" y="53.34" smashed="yes" rot="R270">
<attribute name="VALUE" x="144.78" y="55.88" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="C15" gate="G$1" x="152.4" y="93.98" smashed="yes" rot="R180">
<attribute name="NAME" x="150.876" y="91.059" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="150.876" y="96.139" size="1.778" layer="96" rot="R180"/>
<attribute name="PACKAGE" x="150.876" y="98.044" size="1.27" layer="97" rot="R180"/>
<attribute name="VOLTAGE" x="150.876" y="99.822" size="1.27" layer="97" rot="R180"/>
<attribute name="TYPE" x="150.876" y="101.6" size="1.27" layer="97" rot="R180"/>
</instance>
<instance part="GND10" gate="1" x="142.24" y="88.9" smashed="yes" rot="R270">
<attribute name="VALUE" x="139.7" y="91.44" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="C17" gate="G$1" x="162.56" y="68.58" smashed="yes" rot="R180">
<attribute name="NAME" x="161.036" y="65.659" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="161.036" y="70.739" size="1.778" layer="96" rot="R180"/>
<attribute name="PACKAGE" x="161.036" y="72.644" size="1.27" layer="97" rot="R180"/>
<attribute name="VOLTAGE" x="161.036" y="74.422" size="1.27" layer="97" rot="R180"/>
<attribute name="TYPE" x="161.036" y="76.2" size="1.27" layer="97" rot="R180"/>
</instance>
<instance part="GND11" gate="1" x="142.24" y="63.5" smashed="yes" rot="R270">
<attribute name="VALUE" x="139.7" y="66.04" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="R10" gate="G$1" x="170.18" y="111.76" smashed="yes" rot="R180">
<attribute name="NAME" x="173.99" y="110.2614" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="173.99" y="115.062" size="1.778" layer="96" rot="R180"/>
<attribute name="PRECISION" x="173.99" y="118.618" size="1.27" layer="97" rot="R180"/>
<attribute name="PACKAGE" x="173.99" y="116.84" size="1.27" layer="97" rot="R180"/>
</instance>
<instance part="GND12" gate="1" x="147.32" y="111.76" smashed="yes" rot="R270">
<attribute name="VALUE" x="144.78" y="114.3" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="GND17" gate="1" x="259.08" y="114.3" smashed="yes" rot="R90">
<attribute name="VALUE" x="261.62" y="111.76" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R9" gate="G$1" x="162.56" y="106.68" smashed="yes" rot="R180">
<attribute name="NAME" x="166.37" y="105.1814" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="166.37" y="109.982" size="1.778" layer="96" rot="R180"/>
<attribute name="PRECISION" x="166.37" y="113.538" size="1.27" layer="97" rot="R180"/>
<attribute name="PACKAGE" x="166.37" y="111.76" size="1.27" layer="97" rot="R180"/>
</instance>
<instance part="P+4" gate="VCC" x="233.68" y="10.16" smashed="yes" rot="R180">
<attribute name="VALUE" x="236.22" y="12.7" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="+3V38" gate="G$1" x="170.18" y="68.58" smashed="yes" rot="R90">
<attribute name="VALUE" x="175.26" y="66.04" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="P+8" gate="VCC" x="594.36" y="205.74" smashed="yes" rot="R270">
<attribute name="VALUE" x="591.82" y="208.28" size="1.778" layer="96"/>
</instance>
<instance part="GND25" gate="1" x="594.36" y="198.12" smashed="yes" rot="R90">
<attribute name="VALUE" x="596.9" y="195.58" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="+3V37" gate="G$1" x="139.7" y="106.68" smashed="yes" rot="R90">
<attribute name="VALUE" x="144.78" y="104.14" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="C31" gate="G$1" x="391.16" y="10.16" smashed="yes">
<attribute name="NAME" x="392.684" y="13.081" size="1.778" layer="95"/>
<attribute name="VALUE" x="392.684" y="8.001" size="1.778" layer="96"/>
<attribute name="PACKAGE" x="392.684" y="6.096" size="1.27" layer="97"/>
<attribute name="VOLTAGE" x="392.684" y="4.318" size="1.27" layer="97"/>
<attribute name="TYPE" x="392.684" y="2.54" size="1.27" layer="97"/>
</instance>
<instance part="C33" gate="G$1" x="444.5" y="10.16" smashed="yes">
<attribute name="NAME" x="446.024" y="13.081" size="1.778" layer="95"/>
<attribute name="VALUE" x="446.024" y="8.001" size="1.778" layer="96"/>
<attribute name="PACKAGE" x="446.024" y="6.096" size="1.27" layer="97"/>
<attribute name="VOLTAGE" x="446.024" y="4.318" size="1.27" layer="97"/>
<attribute name="TYPE" x="446.024" y="2.54" size="1.27" layer="97"/>
</instance>
<instance part="C37" gate="G$1" x="497.84" y="10.16" smashed="yes">
<attribute name="NAME" x="499.364" y="13.081" size="1.778" layer="95"/>
<attribute name="VALUE" x="499.364" y="8.001" size="1.778" layer="96"/>
<attribute name="PACKAGE" x="499.364" y="6.096" size="1.27" layer="97"/>
<attribute name="VOLTAGE" x="499.364" y="4.318" size="1.27" layer="97"/>
<attribute name="TYPE" x="499.364" y="2.54" size="1.27" layer="97"/>
</instance>
<instance part="R30" gate="G$1" x="543.56" y="12.7" smashed="yes" rot="R90">
<attribute name="NAME" x="542.0614" y="8.89" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="546.862" y="8.89" size="1.778" layer="96" rot="R90"/>
<attribute name="PRECISION" x="550.418" y="8.89" size="1.27" layer="97" rot="R90"/>
<attribute name="PACKAGE" x="548.64" y="8.89" size="1.27" layer="97" rot="R90"/>
</instance>
<instance part="R29" gate="G$1" x="543.56" y="27.94" smashed="yes" rot="R90">
<attribute name="NAME" x="542.0614" y="24.13" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="546.862" y="24.13" size="1.778" layer="96" rot="R90"/>
<attribute name="PRECISION" x="550.418" y="24.13" size="1.27" layer="97" rot="R90"/>
<attribute name="PACKAGE" x="548.64" y="24.13" size="1.27" layer="97" rot="R90"/>
</instance>
<instance part="GND26" gate="1" x="543.56" y="0" smashed="yes">
<attribute name="VALUE" x="541.02" y="-2.54" size="1.778" layer="96"/>
</instance>
<instance part="C38" gate="G$1" x="551.18" y="10.16" smashed="yes">
<attribute name="NAME" x="552.704" y="13.081" size="1.778" layer="95"/>
<attribute name="VALUE" x="552.704" y="8.001" size="1.778" layer="96"/>
<attribute name="PACKAGE" x="552.704" y="6.096" size="1.27" layer="97"/>
<attribute name="VOLTAGE" x="552.704" y="4.318" size="1.27" layer="97"/>
<attribute name="TYPE" x="552.704" y="2.54" size="1.27" layer="97"/>
</instance>
<instance part="P+9" gate="VCC" x="543.56" y="40.64" smashed="yes">
<attribute name="VALUE" x="541.02" y="38.1" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="C10" gate="G$1" x="-25.4" y="137.16" smashed="yes">
<attribute name="NAME" x="-23.876" y="140.081" size="1.778" layer="95"/>
<attribute name="VALUE" x="-23.876" y="135.001" size="1.778" layer="96"/>
<attribute name="PACKAGE" x="-23.876" y="133.096" size="1.27" layer="97"/>
<attribute name="VOLTAGE" x="-23.876" y="131.318" size="1.27" layer="97"/>
<attribute name="TYPE" x="-23.876" y="129.54" size="1.27" layer="97"/>
</instance>
<instance part="+3V35" gate="G$1" x="-38.1" y="142.24" smashed="yes" rot="R90">
<attribute name="VALUE" x="-33.02" y="139.7" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="GND8" gate="1" x="-25.4" y="127" smashed="yes">
<attribute name="VALUE" x="-27.94" y="124.46" size="1.778" layer="96"/>
</instance>
<instance part="S2" gate="G$1" x="50.8" y="193.04" smashed="yes" rot="R180">
<attribute name="NAME" x="54.864" y="188.722" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="55.626" y="199.136" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="R8" gate="G$1" x="88.9" y="193.04" smashed="yes" rot="R180">
<attribute name="NAME" x="92.71" y="191.5414" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="92.71" y="196.342" size="1.778" layer="96" rot="R180"/>
<attribute name="PRECISION" x="92.71" y="199.898" size="1.27" layer="97" rot="R180"/>
<attribute name="PACKAGE" x="92.71" y="198.12" size="1.27" layer="97" rot="R180"/>
</instance>
<instance part="GND7" gate="1" x="99.06" y="193.04" smashed="yes" rot="R90">
<attribute name="VALUE" x="101.6" y="190.5" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="+3V312" gate="G$1" x="391.16" y="35.56" smashed="yes">
<attribute name="VALUE" x="388.62" y="30.48" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="+3V313" gate="G$1" x="444.5" y="35.56" smashed="yes">
<attribute name="VALUE" x="441.96" y="30.48" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="+3V314" gate="G$1" x="497.84" y="35.56" smashed="yes">
<attribute name="VALUE" x="495.3" y="30.48" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="+3V315" gate="G$1" x="551.18" y="40.64" smashed="yes">
<attribute name="VALUE" x="548.64" y="35.56" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="D4" gate="G$1" x="391.16" y="22.86" smashed="yes" rot="R90">
<attribute name="NAME" x="390.6774" y="25.4" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="393.4714" y="25.4" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="D5" gate="G$1" x="444.5" y="22.86" smashed="yes" rot="R90">
<attribute name="NAME" x="444.0174" y="25.4" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="446.8114" y="25.4" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="D6" gate="G$1" x="497.84" y="22.86" smashed="yes" rot="R90">
<attribute name="NAME" x="497.3574" y="25.4" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="500.1514" y="25.4" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="D7" gate="G$1" x="551.18" y="22.86" smashed="yes" rot="R90">
<attribute name="NAME" x="550.6974" y="25.4" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="553.4914" y="25.4" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R11" gate="G$1" x="248.92" y="124.46" smashed="yes" rot="R180">
<attribute name="NAME" x="252.73" y="122.9614" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="252.73" y="127.762" size="1.778" layer="96" rot="R180"/>
<attribute name="PRECISION" x="252.73" y="131.318" size="1.27" layer="97" rot="R180"/>
<attribute name="PACKAGE" x="252.73" y="129.54" size="1.27" layer="97" rot="R180"/>
</instance>
<instance part="GND16" gate="1" x="259.08" y="124.46" smashed="yes" rot="R90">
<attribute name="VALUE" x="261.62" y="121.92" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="C25" gate="G$1" x="243.84" y="114.3" smashed="yes" rot="R90">
<attribute name="NAME" x="240.919" y="115.824" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="245.999" y="115.824" size="1.778" layer="96" rot="R90"/>
<attribute name="PACKAGE" x="247.904" y="115.824" size="1.27" layer="97" rot="R90"/>
<attribute name="VOLTAGE" x="249.682" y="115.824" size="1.27" layer="97" rot="R90"/>
<attribute name="TYPE" x="251.46" y="115.824" size="1.27" layer="97" rot="R90"/>
</instance>
<instance part="R19" gate="G$1" x="383.54" y="96.52" smashed="yes" rot="R90">
<attribute name="NAME" x="382.1684" y="91.948" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="387.985" y="91.694" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R26" gate="G$1" x="477.52" y="96.52" smashed="yes" rot="R90">
<attribute name="NAME" x="476.1484" y="91.948" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="481.965" y="91.694" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="C24" gate="G$1" x="228.6" y="20.32" smashed="yes" rot="R270">
<attribute name="NAME" x="231.521" y="18.796" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="226.441" y="18.796" size="1.778" layer="96" rot="R270"/>
<attribute name="PACKAGE" x="224.536" y="18.796" size="1.27" layer="97" rot="R270"/>
<attribute name="VOLTAGE" x="222.758" y="18.796" size="1.27" layer="97" rot="R270"/>
<attribute name="TYPE" x="220.98" y="18.796" size="1.27" layer="97" rot="R270"/>
</instance>
<instance part="C32" gate="G$1" x="436.88" y="200.66" smashed="yes">
<attribute name="NAME" x="438.404" y="203.581" size="1.778" layer="95"/>
<attribute name="VALUE" x="438.404" y="198.501" size="1.778" layer="96"/>
<attribute name="PACKAGE" x="438.404" y="196.596" size="1.27" layer="97"/>
<attribute name="VOLTAGE" x="438.404" y="194.818" size="1.27" layer="97"/>
<attribute name="TYPE" x="438.404" y="193.04" size="1.27" layer="97"/>
</instance>
<instance part="C34" gate="G$1" x="447.04" y="200.66" smashed="yes">
<attribute name="NAME" x="448.564" y="203.581" size="1.778" layer="95"/>
<attribute name="VALUE" x="448.564" y="198.501" size="1.778" layer="96"/>
<attribute name="PACKAGE" x="448.564" y="196.596" size="1.27" layer="97"/>
<attribute name="VOLTAGE" x="448.564" y="194.818" size="1.27" layer="97"/>
<attribute name="TYPE" x="448.564" y="193.04" size="1.27" layer="97"/>
</instance>
<instance part="C35" gate="G$1" x="457.2" y="200.66" smashed="yes">
<attribute name="NAME" x="458.724" y="203.581" size="1.778" layer="95"/>
<attribute name="VALUE" x="458.724" y="198.501" size="1.778" layer="96"/>
<attribute name="PACKAGE" x="458.724" y="196.596" size="1.27" layer="97"/>
<attribute name="VOLTAGE" x="458.724" y="194.818" size="1.27" layer="97"/>
<attribute name="TYPE" x="458.724" y="193.04" size="1.27" layer="97"/>
</instance>
<instance part="J22" gate="G$1" x="591.82" y="269.24" smashed="yes"/>
<instance part="J1" gate="G$1" x="0" y="0" smashed="yes"/>
<instance part="P+5" gate="VCC" x="314.96" y="175.26" smashed="yes">
<attribute name="VALUE" x="312.42" y="172.72" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND18" gate="1" x="314.96" y="162.56" smashed="yes">
<attribute name="VALUE" x="312.42" y="160.02" size="1.778" layer="96"/>
</instance>
<instance part="J10" gate="G$1" x="325.12" y="170.18" smashed="yes" rot="R180">
<attribute name="VALUE" x="327.66" y="175.006" size="1.778" layer="96" font="vector" rot="R180"/>
<attribute name="NAME" x="327.66" y="164.592" size="1.778" layer="95" font="vector" rot="R180"/>
</instance>
<instance part="Q1" gate="1" x="381" y="139.7" smashed="yes">
<attribute name="VALUE" x="369.57" y="143.51" size="1.778" layer="96" rot="MR180"/>
<attribute name="NAME" x="369.57" y="140.97" size="1.778" layer="95" rot="MR180"/>
</instance>
<instance part="Q2" gate="1" x="381" y="119.38" smashed="yes">
<attribute name="VALUE" x="369.57" y="123.19" size="1.778" layer="96" rot="MR180"/>
<attribute name="NAME" x="369.57" y="120.65" size="1.778" layer="95" rot="MR180"/>
</instance>
<instance part="Q6" gate="1" x="474.98" y="119.38" smashed="yes">
<attribute name="VALUE" x="463.55" y="123.19" size="1.778" layer="96" rot="MR180"/>
<attribute name="NAME" x="463.55" y="120.65" size="1.778" layer="95" rot="MR180"/>
</instance>
<instance part="Q5" gate="1" x="474.98" y="139.7" smashed="yes">
<attribute name="VALUE" x="463.55" y="143.51" size="1.778" layer="96" rot="MR180"/>
<attribute name="NAME" x="463.55" y="140.97" size="1.778" layer="95" rot="MR180"/>
</instance>
<instance part="Q10" gate="1" x="571.5" y="119.38" smashed="yes">
<attribute name="VALUE" x="560.07" y="123.19" size="1.778" layer="96" rot="MR180"/>
<attribute name="NAME" x="560.07" y="120.65" size="1.778" layer="95" rot="MR180"/>
</instance>
<instance part="Q9" gate="1" x="571.5" y="139.7" smashed="yes">
<attribute name="VALUE" x="560.07" y="143.51" size="1.778" layer="96" rot="MR180"/>
<attribute name="NAME" x="560.07" y="140.97" size="1.778" layer="95" rot="MR180"/>
</instance>
<instance part="C16" gate="G$1" x="162.56" y="93.98" smashed="yes" rot="R180">
<attribute name="NAME" x="161.036" y="91.059" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="161.036" y="96.139" size="1.778" layer="96" rot="R180"/>
<attribute name="PACKAGE" x="161.036" y="98.044" size="1.27" layer="97" rot="R180"/>
<attribute name="VOLTAGE" x="161.036" y="99.822" size="1.27" layer="97" rot="R180"/>
<attribute name="TYPE" x="161.036" y="101.6" size="1.27" layer="97" rot="R180"/>
</instance>
<instance part="J21" gate="1" x="561.34" y="106.68" smashed="yes"/>
<instance part="Q4" gate="1" x="393.7" y="119.38" smashed="yes">
<attribute name="VALUE" x="382.27" y="123.19" size="1.778" layer="96" rot="MR180"/>
<attribute name="NAME" x="382.27" y="120.65" size="1.778" layer="95" rot="MR180"/>
</instance>
<instance part="Q3" gate="1" x="393.7" y="139.7" smashed="yes">
<attribute name="VALUE" x="382.27" y="143.51" size="1.778" layer="96" rot="MR180"/>
<attribute name="NAME" x="382.27" y="140.97" size="1.778" layer="95" rot="MR180"/>
</instance>
<instance part="Q7" gate="1" x="492.76" y="139.7" smashed="yes">
<attribute name="VALUE" x="481.33" y="143.51" size="1.778" layer="96" rot="MR180"/>
<attribute name="NAME" x="481.33" y="140.97" size="1.778" layer="95" rot="MR180"/>
</instance>
<instance part="Q8" gate="1" x="492.76" y="119.38" smashed="yes">
<attribute name="VALUE" x="481.33" y="123.19" size="1.778" layer="96" rot="MR180"/>
<attribute name="NAME" x="481.33" y="120.65" size="1.778" layer="95" rot="MR180"/>
</instance>
<instance part="Q11" gate="1" x="589.28" y="139.7" smashed="yes">
<attribute name="VALUE" x="577.85" y="143.51" size="1.778" layer="96" rot="MR180"/>
<attribute name="NAME" x="577.85" y="140.97" size="1.778" layer="95" rot="MR180"/>
</instance>
<instance part="Q12" gate="1" x="589.28" y="119.38" smashed="yes">
<attribute name="VALUE" x="577.85" y="123.19" size="1.778" layer="96" rot="MR180"/>
<attribute name="NAME" x="577.85" y="120.65" size="1.778" layer="95" rot="MR180"/>
</instance>
<instance part="C36" gate="G$1" x="467.36" y="200.66" smashed="yes">
<attribute name="NAME" x="468.884" y="203.581" size="1.778" layer="95"/>
<attribute name="VALUE" x="468.884" y="198.501" size="1.778" layer="96"/>
<attribute name="PACKAGE" x="468.884" y="196.596" size="1.27" layer="97"/>
<attribute name="VOLTAGE" x="468.884" y="194.818" size="1.27" layer="97"/>
<attribute name="TYPE" x="468.884" y="193.04" size="1.27" layer="97"/>
</instance>
<instance part="J24" gate="G$1" x="12.7" y="142.24" smashed="yes"/>
<instance part="J2" gate="A" x="220.98" y="152.4" smashed="yes" rot="R180">
<attribute name="VALUE" x="226.06" y="162.306" size="1.778" layer="96" font="vector" rot="R180"/>
<attribute name="NAME" x="226.06" y="141.732" size="1.778" layer="95" font="vector" rot="R180"/>
</instance>
<instance part="GND1" gate="1" x="205.74" y="167.64" smashed="yes" rot="R180">
<attribute name="VALUE" x="208.28" y="170.18" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="P+1" gate="VCC" x="-15.24" y="165.1" smashed="yes" rot="R90">
<attribute name="VALUE" x="-12.7" y="162.56" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="GND2" gate="1" x="-15.24" y="172.72" smashed="yes" rot="R270">
<attribute name="VALUE" x="-17.78" y="175.26" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="+3V1" gate="G$1" x="-15.24" y="157.48" smashed="yes" rot="R90">
<attribute name="VALUE" x="-10.16" y="154.94" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="J3" gate="G$1" x="213.36" y="190.5" smashed="yes"/>
<instance part="J4" gate="G$1" x="213.36" y="185.42" smashed="yes"/>
<instance part="J5" gate="G$1" x="193.04" y="190.5" smashed="yes" rot="R180"/>
<instance part="J6" gate="G$1" x="193.04" y="185.42" smashed="yes" rot="R180"/>
<instance part="+3V2" gate="G$1" x="185.42" y="154.94" smashed="yes" rot="R90">
<attribute name="VALUE" x="190.5" y="152.4" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="C1" gate="G$1" x="205.74" y="160.02" smashed="yes" rot="R180">
<attribute name="NAME" x="204.216" y="157.099" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="204.216" y="162.179" size="1.778" layer="96" rot="R180"/>
<attribute name="PACKAGE" x="204.216" y="164.084" size="1.27" layer="97" rot="R180"/>
<attribute name="VOLTAGE" x="204.216" y="165.862" size="1.27" layer="97" rot="R180"/>
<attribute name="TYPE" x="204.216" y="167.64" size="1.27" layer="97" rot="R180"/>
</instance>
<instance part="C2" gate="G$1" x="426.72" y="203.2" smashed="yes">
<attribute name="NAME" x="427.736" y="203.835" size="1.778" layer="95"/>
<attribute name="VALUE" x="427.736" y="199.009" size="1.778" layer="96"/>
</instance>
<instance part="C3" gate="G$1" x="416.56" y="203.2" smashed="yes">
<attribute name="NAME" x="417.576" y="203.835" size="1.778" layer="95"/>
<attribute name="VALUE" x="417.576" y="199.009" size="1.778" layer="96"/>
</instance>
<instance part="C4" gate="G$1" x="406.4" y="203.2" smashed="yes">
<attribute name="NAME" x="407.416" y="203.835" size="1.778" layer="95"/>
<attribute name="VALUE" x="407.416" y="199.009" size="1.778" layer="96"/>
</instance>
<instance part="C5" gate="G$1" x="396.24" y="203.2" smashed="yes">
<attribute name="NAME" x="397.256" y="203.835" size="1.778" layer="95"/>
<attribute name="VALUE" x="397.256" y="199.009" size="1.778" layer="96"/>
</instance>
<instance part="C7" gate="G$1" x="487.68" y="200.66" smashed="yes">
<attribute name="NAME" x="489.204" y="203.581" size="1.778" layer="95"/>
<attribute name="VALUE" x="489.204" y="198.501" size="1.778" layer="96"/>
<attribute name="PACKAGE" x="489.204" y="196.596" size="1.27" layer="97"/>
<attribute name="VOLTAGE" x="489.204" y="194.818" size="1.27" layer="97"/>
<attribute name="TYPE" x="489.204" y="193.04" size="1.27" layer="97"/>
</instance>
<instance part="C8" gate="G$1" x="497.84" y="200.66" smashed="yes">
<attribute name="NAME" x="499.364" y="203.581" size="1.778" layer="95"/>
<attribute name="VALUE" x="499.364" y="198.501" size="1.778" layer="96"/>
<attribute name="PACKAGE" x="499.364" y="196.596" size="1.27" layer="97"/>
<attribute name="VOLTAGE" x="499.364" y="194.818" size="1.27" layer="97"/>
<attribute name="TYPE" x="499.364" y="193.04" size="1.27" layer="97"/>
</instance>
<instance part="C9" gate="G$1" x="508" y="200.66" smashed="yes">
<attribute name="NAME" x="509.524" y="203.581" size="1.778" layer="95"/>
<attribute name="VALUE" x="509.524" y="198.501" size="1.778" layer="96"/>
<attribute name="PACKAGE" x="509.524" y="196.596" size="1.27" layer="97"/>
<attribute name="VOLTAGE" x="509.524" y="194.818" size="1.27" layer="97"/>
<attribute name="TYPE" x="509.524" y="193.04" size="1.27" layer="97"/>
</instance>
<instance part="C11" gate="G$1" x="518.16" y="200.66" smashed="yes">
<attribute name="NAME" x="519.684" y="203.581" size="1.778" layer="95"/>
<attribute name="VALUE" x="519.684" y="198.501" size="1.778" layer="96"/>
<attribute name="PACKAGE" x="519.684" y="196.596" size="1.27" layer="97"/>
<attribute name="VOLTAGE" x="519.684" y="194.818" size="1.27" layer="97"/>
<attribute name="TYPE" x="519.684" y="193.04" size="1.27" layer="97"/>
</instance>
<instance part="C12" gate="G$1" x="528.32" y="200.66" smashed="yes">
<attribute name="NAME" x="529.844" y="203.581" size="1.778" layer="95"/>
<attribute name="VALUE" x="529.844" y="198.501" size="1.778" layer="96"/>
<attribute name="PACKAGE" x="529.844" y="196.596" size="1.27" layer="97"/>
<attribute name="VOLTAGE" x="529.844" y="194.818" size="1.27" layer="97"/>
<attribute name="TYPE" x="529.844" y="193.04" size="1.27" layer="97"/>
</instance>
<instance part="C14" gate="G$1" x="548.64" y="200.66" smashed="yes">
<attribute name="NAME" x="550.164" y="203.581" size="1.778" layer="95"/>
<attribute name="VALUE" x="550.164" y="198.501" size="1.778" layer="96"/>
<attribute name="PACKAGE" x="550.164" y="196.596" size="1.27" layer="97"/>
<attribute name="VOLTAGE" x="550.164" y="194.818" size="1.27" layer="97"/>
<attribute name="TYPE" x="550.164" y="193.04" size="1.27" layer="97"/>
</instance>
<instance part="C19" gate="G$1" x="558.8" y="200.66" smashed="yes">
<attribute name="NAME" x="560.324" y="203.581" size="1.778" layer="95"/>
<attribute name="VALUE" x="560.324" y="198.501" size="1.778" layer="96"/>
<attribute name="PACKAGE" x="560.324" y="196.596" size="1.27" layer="97"/>
<attribute name="VOLTAGE" x="560.324" y="194.818" size="1.27" layer="97"/>
<attribute name="TYPE" x="560.324" y="193.04" size="1.27" layer="97"/>
</instance>
<instance part="C21" gate="G$1" x="568.96" y="200.66" smashed="yes">
<attribute name="NAME" x="570.484" y="203.581" size="1.778" layer="95"/>
<attribute name="VALUE" x="570.484" y="198.501" size="1.778" layer="96"/>
<attribute name="PACKAGE" x="570.484" y="196.596" size="1.27" layer="97"/>
<attribute name="VOLTAGE" x="570.484" y="194.818" size="1.27" layer="97"/>
<attribute name="TYPE" x="570.484" y="193.04" size="1.27" layer="97"/>
</instance>
<instance part="C39" gate="G$1" x="579.12" y="200.66" smashed="yes">
<attribute name="NAME" x="580.644" y="203.581" size="1.778" layer="95"/>
<attribute name="VALUE" x="580.644" y="198.501" size="1.778" layer="96"/>
<attribute name="PACKAGE" x="580.644" y="196.596" size="1.27" layer="97"/>
<attribute name="VOLTAGE" x="580.644" y="194.818" size="1.27" layer="97"/>
<attribute name="TYPE" x="580.644" y="193.04" size="1.27" layer="97"/>
</instance>
<instance part="C40" gate="G$1" x="589.28" y="200.66" smashed="yes">
<attribute name="NAME" x="590.804" y="203.581" size="1.778" layer="95"/>
<attribute name="VALUE" x="590.804" y="198.501" size="1.778" layer="96"/>
<attribute name="PACKAGE" x="590.804" y="196.596" size="1.27" layer="97"/>
<attribute name="VOLTAGE" x="590.804" y="194.818" size="1.27" layer="97"/>
<attribute name="TYPE" x="590.804" y="193.04" size="1.27" layer="97"/>
</instance>
<instance part="J7" gate="G$1" x="424.18" y="127" smashed="yes" rot="R180">
<attribute name="VALUE" x="426.72" y="131.826" size="1.778" layer="96" font="vector" rot="R180"/>
<attribute name="NAME" x="426.72" y="123.952" size="1.778" layer="95" font="vector" rot="R180"/>
</instance>
<instance part="J8" gate="G$1" x="518.16" y="127" smashed="yes" rot="R180">
<attribute name="VALUE" x="520.7" y="131.826" size="1.778" layer="96" font="vector" rot="R180"/>
<attribute name="NAME" x="520.7" y="123.952" size="1.778" layer="95" font="vector" rot="R180"/>
</instance>
<instance part="J9" gate="G$1" x="617.22" y="127" smashed="yes" rot="R180">
<attribute name="VALUE" x="619.76" y="131.826" size="1.778" layer="96" font="vector" rot="R180"/>
<attribute name="NAME" x="619.76" y="123.952" size="1.778" layer="95" font="vector" rot="R180"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="R21" gate="G$1" pin="1"/>
<pinref part="GND21" gate="1" pin="GND"/>
<wire x1="383.54" y1="7.62" x2="383.54" y2="5.08" width="0.1524" layer="91"/>
<pinref part="C31" gate="G$1" pin="2"/>
<wire x1="383.54" y1="5.08" x2="383.54" y2="2.54" width="0.1524" layer="91"/>
<wire x1="383.54" y1="5.08" x2="391.16" y2="5.08" width="0.1524" layer="91"/>
<wire x1="391.16" y1="5.08" x2="391.16" y2="7.62" width="0.1524" layer="91"/>
<junction x="383.54" y="5.08"/>
</segment>
<segment>
<pinref part="R23" gate="G$1" pin="1"/>
<pinref part="GND22" gate="1" pin="GND"/>
<wire x1="436.88" y1="7.62" x2="436.88" y2="5.08" width="0.1524" layer="91"/>
<pinref part="C33" gate="G$1" pin="2"/>
<wire x1="436.88" y1="5.08" x2="436.88" y2="2.54" width="0.1524" layer="91"/>
<wire x1="436.88" y1="5.08" x2="444.5" y2="5.08" width="0.1524" layer="91"/>
<wire x1="444.5" y1="5.08" x2="444.5" y2="7.62" width="0.1524" layer="91"/>
<junction x="436.88" y="5.08"/>
</segment>
<segment>
<pinref part="R28" gate="G$1" pin="1"/>
<pinref part="GND24" gate="1" pin="GND"/>
<wire x1="490.22" y1="7.62" x2="490.22" y2="5.08" width="0.1524" layer="91"/>
<pinref part="C37" gate="G$1" pin="2"/>
<wire x1="490.22" y1="5.08" x2="490.22" y2="2.54" width="0.1524" layer="91"/>
<wire x1="497.84" y1="7.62" x2="497.84" y2="5.08" width="0.1524" layer="91"/>
<wire x1="497.84" y1="5.08" x2="490.22" y2="5.08" width="0.1524" layer="91"/>
<junction x="490.22" y="5.08"/>
</segment>
<segment>
<pinref part="GND15" gate="1" pin="GND"/>
<pinref part="U5" gate="G$1" pin="GNDPAD"/>
<pinref part="C22" gate="G$1" pin="2"/>
<wire x1="213.36" y1="12.7" x2="213.36" y2="20.32" width="0.1524" layer="91"/>
<wire x1="213.36" y1="20.32" x2="213.36" y2="30.48" width="0.1524" layer="91"/>
<wire x1="213.36" y1="30.48" x2="213.36" y2="48.26" width="0.1524" layer="91"/>
<wire x1="213.36" y1="48.26" x2="213.36" y2="50.8" width="0.1524" layer="91"/>
<wire x1="226.06" y1="48.26" x2="213.36" y2="48.26" width="0.1524" layer="91"/>
<junction x="213.36" y="48.26"/>
<pinref part="C23" gate="G$1" pin="2"/>
<wire x1="226.06" y1="30.48" x2="213.36" y2="30.48" width="0.1524" layer="91"/>
<junction x="213.36" y="30.48"/>
<pinref part="C24" gate="G$1" pin="2"/>
<wire x1="226.06" y1="20.32" x2="213.36" y2="20.32" width="0.1524" layer="91"/>
<junction x="213.36" y="20.32"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="AGND"/>
<wire x1="195.58" y1="58.42" x2="193.04" y2="58.42" width="0.1524" layer="91"/>
<wire x1="193.04" y1="58.42" x2="193.04" y2="53.34" width="0.1524" layer="91"/>
<pinref part="C20" gate="G$1" pin="1"/>
<wire x1="193.04" y1="53.34" x2="187.96" y2="53.34" width="0.1524" layer="91"/>
<pinref part="GND13" gate="1" pin="GND"/>
<wire x1="187.96" y1="53.34" x2="149.86" y2="53.34" width="0.1524" layer="91"/>
<junction x="187.96" y="53.34"/>
</segment>
<segment>
<pinref part="R10" gate="G$1" pin="2"/>
<pinref part="GND12" gate="1" pin="GND"/>
<wire x1="165.1" y1="111.76" x2="149.86" y2="111.76" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND25" gate="1" pin="GND"/>
<pinref part="C32" gate="G$1" pin="2"/>
<wire x1="436.88" y1="198.12" x2="447.04" y2="198.12" width="0.1524" layer="91"/>
<pinref part="C34" gate="G$1" pin="2"/>
<wire x1="447.04" y1="198.12" x2="457.2" y2="198.12" width="0.1524" layer="91"/>
<junction x="447.04" y="198.12"/>
<pinref part="C35" gate="G$1" pin="2"/>
<wire x1="457.2" y1="198.12" x2="467.36" y2="198.12" width="0.1524" layer="91"/>
<junction x="457.2" y="198.12"/>
<wire x1="467.36" y1="198.12" x2="487.68" y2="198.12" width="0.1524" layer="91"/>
<wire x1="487.68" y1="198.12" x2="497.84" y2="198.12" width="0.1524" layer="91"/>
<wire x1="497.84" y1="198.12" x2="508" y2="198.12" width="0.1524" layer="91"/>
<wire x1="508" y1="198.12" x2="518.16" y2="198.12" width="0.1524" layer="91"/>
<wire x1="518.16" y1="198.12" x2="528.32" y2="198.12" width="0.1524" layer="91"/>
<wire x1="528.32" y1="198.12" x2="548.64" y2="198.12" width="0.1524" layer="91"/>
<wire x1="548.64" y1="198.12" x2="558.8" y2="198.12" width="0.1524" layer="91"/>
<wire x1="558.8" y1="198.12" x2="568.96" y2="198.12" width="0.1524" layer="91"/>
<wire x1="568.96" y1="198.12" x2="579.12" y2="198.12" width="0.1524" layer="91"/>
<wire x1="579.12" y1="198.12" x2="589.28" y2="198.12" width="0.1524" layer="91"/>
<wire x1="589.28" y1="198.12" x2="591.82" y2="198.12" width="0.1524" layer="91"/>
<wire x1="406.4" y1="198.12" x2="416.56" y2="198.12" width="0.1524" layer="91"/>
<junction x="436.88" y="198.12"/>
<pinref part="C36" gate="G$1" pin="2"/>
<junction x="467.36" y="198.12"/>
<pinref part="C2" gate="G$1" pin="-"/>
<wire x1="416.56" y1="198.12" x2="426.72" y2="198.12" width="0.1524" layer="91"/>
<wire x1="426.72" y1="198.12" x2="436.88" y2="198.12" width="0.1524" layer="91"/>
<junction x="426.72" y="198.12"/>
<pinref part="C3" gate="G$1" pin="-"/>
<junction x="416.56" y="198.12"/>
<pinref part="C4" gate="G$1" pin="-"/>
<pinref part="C5" gate="G$1" pin="-"/>
<wire x1="406.4" y1="198.12" x2="396.24" y2="198.12" width="0.1524" layer="91"/>
<junction x="406.4" y="198.12"/>
<pinref part="C7" gate="G$1" pin="2"/>
<junction x="487.68" y="198.12"/>
<pinref part="C8" gate="G$1" pin="2"/>
<junction x="497.84" y="198.12"/>
<pinref part="C9" gate="G$1" pin="2"/>
<junction x="508" y="198.12"/>
<pinref part="C11" gate="G$1" pin="2"/>
<junction x="518.16" y="198.12"/>
<pinref part="C12" gate="G$1" pin="2"/>
<junction x="528.32" y="198.12"/>
<pinref part="C14" gate="G$1" pin="2"/>
<junction x="548.64" y="198.12"/>
<pinref part="C19" gate="G$1" pin="2"/>
<junction x="558.8" y="198.12"/>
<pinref part="C21" gate="G$1" pin="2"/>
<junction x="568.96" y="198.12"/>
<pinref part="C39" gate="G$1" pin="2"/>
<junction x="579.12" y="198.12"/>
<pinref part="C40" gate="G$1" pin="2"/>
<junction x="589.28" y="198.12"/>
</segment>
<segment>
<pinref part="C15" gate="G$1" pin="1"/>
<wire x1="162.56" y1="88.9" x2="152.4" y2="88.9" width="0.1524" layer="91"/>
<wire x1="152.4" y1="88.9" x2="144.78" y2="88.9" width="0.1524" layer="91"/>
<junction x="152.4" y="88.9"/>
<pinref part="GND10" gate="1" pin="GND"/>
<pinref part="C16" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="C17" gate="G$1" pin="1"/>
<wire x1="162.56" y1="63.5" x2="144.78" y2="63.5" width="0.1524" layer="91"/>
<pinref part="GND11" gate="1" pin="GND"/>
<label x="144.78" y="63.5" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R30" gate="G$1" pin="1"/>
<pinref part="GND26" gate="1" pin="GND"/>
<wire x1="543.56" y1="7.62" x2="543.56" y2="5.08" width="0.1524" layer="91"/>
<pinref part="C38" gate="G$1" pin="2"/>
<wire x1="543.56" y1="5.08" x2="543.56" y2="2.54" width="0.1524" layer="91"/>
<wire x1="551.18" y1="7.62" x2="551.18" y2="5.08" width="0.1524" layer="91"/>
<wire x1="551.18" y1="5.08" x2="543.56" y2="5.08" width="0.1524" layer="91"/>
<junction x="543.56" y="5.08"/>
</segment>
<segment>
<pinref part="C10" gate="G$1" pin="2"/>
<pinref part="GND8" gate="1" pin="GND"/>
<wire x1="-25.4" y1="129.54" x2="-25.4" y2="134.62" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND16" gate="1" pin="GND"/>
<pinref part="R11" gate="G$1" pin="1"/>
<wire x1="256.54" y1="124.46" x2="254" y2="124.46" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C25" gate="G$1" pin="2"/>
<pinref part="GND17" gate="1" pin="GND"/>
<wire x1="246.38" y1="114.3" x2="256.54" y2="114.3" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND18" gate="1" pin="GND"/>
<wire x1="314.96" y1="165.1" x2="314.96" y2="167.64" width="0.1524" layer="91"/>
<pinref part="J10" gate="G$1" pin="2"/>
<wire x1="314.96" y1="167.64" x2="317.5" y2="167.64" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R8" gate="G$1" pin="1"/>
<pinref part="GND7" gate="1" pin="GND"/>
<wire x1="96.52" y1="193.04" x2="93.98" y2="193.04" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="Q10" gate="1" pin="S"/>
<pinref part="J21" gate="1" pin="P$2"/>
<wire x1="574.04" y1="114.3" x2="574.04" y2="106.68" width="0.1524" layer="91"/>
<pinref part="GND27" gate="1" pin="GND"/>
<wire x1="574.04" y1="106.68" x2="574.04" y2="81.28" width="0.1524" layer="91"/>
<junction x="574.04" y="106.68"/>
<pinref part="Q12" gate="1" pin="S"/>
<wire x1="591.82" y1="114.3" x2="574.04" y2="114.3" width="0.1524" layer="91"/>
<junction x="574.04" y="114.3"/>
</segment>
<segment>
<pinref part="GND20" gate="1" pin="GND"/>
<pinref part="R19" gate="G$1" pin="B"/>
<wire x1="383.54" y1="86.36" x2="383.54" y2="88.9" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND23" gate="1" pin="GND"/>
<pinref part="R26" gate="G$1" pin="B"/>
<wire x1="477.52" y1="86.36" x2="477.52" y2="88.9" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J24" gate="G$1" pin="GND"/>
<pinref part="GND2" gate="1" pin="GND"/>
<wire x1="-12.7" y1="172.72" x2="2.54" y2="172.72" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J2" gate="A" pin="1"/>
<wire x1="215.9" y1="157.48" x2="210.82" y2="157.48" width="0.1524" layer="91"/>
<wire x1="210.82" y1="157.48" x2="210.82" y2="162.56" width="0.1524" layer="91"/>
<pinref part="C1" gate="G$1" pin="2"/>
<wire x1="210.82" y1="162.56" x2="205.74" y2="162.56" width="0.1524" layer="91"/>
<wire x1="205.74" y1="162.56" x2="205.74" y2="165.1" width="0.1524" layer="91"/>
<junction x="205.74" y="162.56"/>
<pinref part="GND1" gate="1" pin="GND"/>
</segment>
</net>
<net name="+3V3" class="0">
<segment>
<pinref part="+3V38" gate="G$1" pin="+3V3"/>
<pinref part="U5" gate="G$1" pin="REF"/>
<wire x1="172.72" y1="68.58" x2="195.58" y2="68.58" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="+3V315" gate="G$1" pin="+3V3"/>
<wire x1="551.18" y1="25.4" x2="551.18" y2="38.1" width="0.1524" layer="91"/>
<pinref part="D7" gate="G$1" pin="C"/>
</segment>
<segment>
<pinref part="+3V314" gate="G$1" pin="+3V3"/>
<wire x1="497.84" y1="33.02" x2="497.84" y2="25.4" width="0.1524" layer="91"/>
<pinref part="D6" gate="G$1" pin="C"/>
</segment>
<segment>
<pinref part="+3V313" gate="G$1" pin="+3V3"/>
<wire x1="444.5" y1="33.02" x2="444.5" y2="25.4" width="0.1524" layer="91"/>
<pinref part="D5" gate="G$1" pin="C"/>
</segment>
<segment>
<pinref part="+3V312" gate="G$1" pin="+3V3"/>
<wire x1="391.16" y1="33.02" x2="391.16" y2="25.4" width="0.1524" layer="91"/>
<pinref part="D4" gate="G$1" pin="C"/>
</segment>
<segment>
<pinref part="R9" gate="G$1" pin="2"/>
<pinref part="+3V37" gate="G$1" pin="+3V3"/>
<wire x1="157.48" y1="106.68" x2="142.24" y2="106.68" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C10" gate="G$1" pin="1"/>
<wire x1="-35.56" y1="142.24" x2="-25.4" y2="142.24" width="0.1524" layer="91"/>
<pinref part="+3V35" gate="G$1" pin="+3V3"/>
<pinref part="J24" gate="G$1" pin="A0/AREF---PB0/ADC"/>
<wire x1="-25.4" y1="142.24" x2="2.54" y2="142.24" width="0.1524" layer="91"/>
<junction x="-25.4" y="142.24"/>
</segment>
<segment>
<pinref part="J24" gate="G$1" pin="+3V3"/>
<pinref part="+3V1" gate="G$1" pin="+3V3"/>
<wire x1="-12.7" y1="157.48" x2="2.54" y2="157.48" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="+3V2" gate="G$1" pin="+3V3"/>
<pinref part="C1" gate="G$1" pin="1"/>
<wire x1="187.96" y1="154.94" x2="205.74" y2="154.94" width="0.1524" layer="91"/>
<pinref part="J2" gate="A" pin="2"/>
<wire x1="205.74" y1="154.94" x2="215.9" y2="154.94" width="0.1524" layer="91"/>
<junction x="205.74" y="154.94"/>
</segment>
</net>
<net name="V-U" class="0">
<segment>
<pinref part="R21" gate="G$1" pin="2"/>
<pinref part="R20" gate="G$1" pin="1"/>
<wire x1="383.54" y1="17.78" x2="383.54" y2="20.32" width="0.1524" layer="91"/>
<wire x1="383.54" y1="20.32" x2="383.54" y2="22.86" width="0.1524" layer="91"/>
<wire x1="383.54" y1="20.32" x2="353.06" y2="20.32" width="0.1524" layer="91"/>
<junction x="383.54" y="20.32"/>
<label x="353.06" y="20.32" size="1.778" layer="95"/>
<pinref part="C31" gate="G$1" pin="1"/>
<wire x1="383.54" y1="20.32" x2="391.16" y2="20.32" width="0.1524" layer="91"/>
<wire x1="391.16" y1="20.32" x2="391.16" y2="15.24" width="0.1524" layer="91"/>
<pinref part="D4" gate="G$1" pin="A"/>
<junction x="391.16" y="20.32"/>
</segment>
<segment>
<wire x1="-15.24" y1="132.08" x2="2.54" y2="132.08" width="0.1524" layer="91"/>
<label x="-12.7" y="132.08" size="1.778" layer="95"/>
<pinref part="J24" gate="G$1" pin="A4/ADC---PB4/ADC"/>
</segment>
</net>
<net name="V-V" class="0">
<segment>
<pinref part="R23" gate="G$1" pin="2"/>
<pinref part="R22" gate="G$1" pin="1"/>
<wire x1="436.88" y1="17.78" x2="436.88" y2="20.32" width="0.1524" layer="91"/>
<wire x1="436.88" y1="20.32" x2="436.88" y2="22.86" width="0.1524" layer="91"/>
<wire x1="436.88" y1="20.32" x2="406.4" y2="20.32" width="0.1524" layer="91"/>
<junction x="436.88" y="20.32"/>
<label x="406.4" y="20.32" size="1.778" layer="95"/>
<pinref part="C33" gate="G$1" pin="1"/>
<wire x1="436.88" y1="20.32" x2="444.5" y2="20.32" width="0.1524" layer="91"/>
<wire x1="444.5" y1="20.32" x2="444.5" y2="15.24" width="0.1524" layer="91"/>
<pinref part="D5" gate="G$1" pin="A"/>
<junction x="444.5" y="20.32"/>
</segment>
<segment>
<wire x1="-15.24" y1="129.54" x2="2.54" y2="129.54" width="0.1524" layer="91"/>
<label x="-12.7" y="129.54" size="1.778" layer="95"/>
<pinref part="J24" gate="G$1" pin="A5/ADC---PB5/ADC"/>
</segment>
</net>
<net name="V-VCC" class="0">
<segment>
<pinref part="R30" gate="G$1" pin="2"/>
<pinref part="R29" gate="G$1" pin="1"/>
<wire x1="543.56" y1="17.78" x2="543.56" y2="20.32" width="0.1524" layer="91"/>
<wire x1="543.56" y1="20.32" x2="543.56" y2="22.86" width="0.1524" layer="91"/>
<wire x1="543.56" y1="20.32" x2="513.08" y2="20.32" width="0.1524" layer="91"/>
<junction x="543.56" y="20.32"/>
<label x="513.08" y="20.32" size="1.778" layer="95"/>
<wire x1="543.56" y1="20.32" x2="551.18" y2="20.32" width="0.1524" layer="91"/>
<pinref part="C38" gate="G$1" pin="1"/>
<wire x1="551.18" y1="20.32" x2="551.18" y2="15.24" width="0.1524" layer="91"/>
<pinref part="D7" gate="G$1" pin="A"/>
<junction x="551.18" y="20.32"/>
</segment>
<segment>
<wire x1="-15.24" y1="124.46" x2="2.54" y2="124.46" width="0.1524" layer="91"/>
<label x="-12.7" y="124.46" size="1.778" layer="95"/>
<pinref part="J24" gate="G$1" pin="A7/ADC---PB7/ADC/ADCOUT"/>
</segment>
</net>
<net name="PHASE-U" class="0">
<segment>
<pinref part="R20" gate="G$1" pin="2"/>
<wire x1="383.54" y1="33.02" x2="383.54" y2="38.1" width="0.1524" layer="91"/>
<wire x1="383.54" y1="38.1" x2="393.7" y2="38.1" width="0.1524" layer="91"/>
<label x="388.62" y="38.1" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="SH_A"/>
<label x="233.68" y="101.6" size="1.778" layer="95"/>
<wire x1="251.46" y1="101.6" x2="231.14" y2="101.6" width="0.1524" layer="91"/>
<pinref part="C28" gate="G$1" pin="2"/>
<wire x1="256.54" y1="99.06" x2="251.46" y2="99.06" width="0.1524" layer="91"/>
<wire x1="251.46" y1="99.06" x2="251.46" y2="101.6" width="0.1524" layer="91"/>
</segment>
<segment>
<label x="353.06" y="127" size="1.778" layer="95"/>
<wire x1="383.54" y1="134.62" x2="383.54" y2="127" width="0.1524" layer="91"/>
<wire x1="383.54" y1="127" x2="383.54" y2="124.46" width="0.1524" layer="91"/>
<junction x="383.54" y="127"/>
<wire x1="383.54" y1="127" x2="396.24" y2="127" width="0.1524" layer="91"/>
<label x="401.32" y="127" size="1.778" layer="95"/>
<pinref part="Q1" gate="1" pin="S"/>
<pinref part="Q2" gate="1" pin="D"/>
<pinref part="Q3" gate="1" pin="S"/>
<wire x1="396.24" y1="127" x2="416.56" y2="127" width="0.1524" layer="91"/>
<wire x1="396.24" y1="134.62" x2="396.24" y2="127" width="0.1524" layer="91"/>
<junction x="396.24" y="127"/>
<pinref part="Q4" gate="1" pin="D"/>
<wire x1="396.24" y1="124.46" x2="396.24" y2="127" width="0.1524" layer="91"/>
<wire x1="383.54" y1="127" x2="353.06" y2="127" width="0.1524" layer="91"/>
<pinref part="J7" gate="G$1" pin="2"/>
</segment>
</net>
<net name="VCC" class="0">
<segment>
<pinref part="P+7" gate="VCC" pin="VCC"/>
<wire x1="477.52" y1="144.78" x2="477.52" y2="157.48" width="0.1524" layer="91"/>
<pinref part="Q5" gate="1" pin="D"/>
<pinref part="Q7" gate="1" pin="D"/>
<wire x1="495.3" y1="144.78" x2="477.52" y2="144.78" width="0.1524" layer="91"/>
<junction x="477.52" y="144.78"/>
</segment>
<segment>
<pinref part="P+10" gate="VCC" pin="VCC"/>
<wire x1="574.04" y1="144.78" x2="574.04" y2="157.48" width="0.1524" layer="91"/>
<pinref part="Q9" gate="1" pin="D"/>
<pinref part="Q11" gate="1" pin="D"/>
<wire x1="591.82" y1="144.78" x2="574.04" y2="144.78" width="0.1524" layer="91"/>
<junction x="574.04" y="144.78"/>
</segment>
<segment>
<pinref part="P+6" gate="VCC" pin="VCC"/>
<pinref part="Q1" gate="1" pin="D"/>
<wire x1="383.54" y1="144.78" x2="383.54" y2="157.48" width="0.1524" layer="91"/>
<pinref part="Q3" gate="1" pin="D"/>
<wire x1="396.24" y1="144.78" x2="383.54" y2="144.78" width="0.1524" layer="91"/>
<junction x="383.54" y="144.78"/>
</segment>
<segment>
<pinref part="P+8" gate="VCC" pin="VCC"/>
<pinref part="C32" gate="G$1" pin="1"/>
<wire x1="436.88" y1="205.74" x2="447.04" y2="205.74" width="0.1524" layer="91"/>
<pinref part="C34" gate="G$1" pin="1"/>
<wire x1="447.04" y1="205.74" x2="457.2" y2="205.74" width="0.1524" layer="91"/>
<junction x="447.04" y="205.74"/>
<pinref part="C35" gate="G$1" pin="1"/>
<wire x1="457.2" y1="205.74" x2="467.36" y2="205.74" width="0.1524" layer="91"/>
<junction x="457.2" y="205.74"/>
<wire x1="467.36" y1="205.74" x2="487.68" y2="205.74" width="0.1524" layer="91"/>
<wire x1="487.68" y1="205.74" x2="497.84" y2="205.74" width="0.1524" layer="91"/>
<wire x1="497.84" y1="205.74" x2="508" y2="205.74" width="0.1524" layer="91"/>
<wire x1="508" y1="205.74" x2="518.16" y2="205.74" width="0.1524" layer="91"/>
<wire x1="518.16" y1="205.74" x2="528.32" y2="205.74" width="0.1524" layer="91"/>
<wire x1="528.32" y1="205.74" x2="548.64" y2="205.74" width="0.1524" layer="91"/>
<wire x1="548.64" y1="205.74" x2="558.8" y2="205.74" width="0.1524" layer="91"/>
<wire x1="558.8" y1="205.74" x2="568.96" y2="205.74" width="0.1524" layer="91"/>
<wire x1="568.96" y1="205.74" x2="579.12" y2="205.74" width="0.1524" layer="91"/>
<wire x1="579.12" y1="205.74" x2="589.28" y2="205.74" width="0.1524" layer="91"/>
<wire x1="589.28" y1="205.74" x2="591.82" y2="205.74" width="0.1524" layer="91"/>
<wire x1="406.4" y1="205.74" x2="416.56" y2="205.74" width="0.1524" layer="91"/>
<junction x="436.88" y="205.74"/>
<pinref part="C36" gate="G$1" pin="1"/>
<junction x="467.36" y="205.74"/>
<pinref part="C2" gate="G$1" pin="+"/>
<wire x1="416.56" y1="205.74" x2="426.72" y2="205.74" width="0.1524" layer="91"/>
<wire x1="426.72" y1="205.74" x2="436.88" y2="205.74" width="0.1524" layer="91"/>
<junction x="426.72" y="205.74"/>
<pinref part="C3" gate="G$1" pin="+"/>
<junction x="416.56" y="205.74"/>
<pinref part="C4" gate="G$1" pin="+"/>
<pinref part="C5" gate="G$1" pin="+"/>
<wire x1="406.4" y1="205.74" x2="396.24" y2="205.74" width="0.1524" layer="91"/>
<junction x="406.4" y="205.74"/>
<pinref part="C7" gate="G$1" pin="1"/>
<junction x="487.68" y="205.74"/>
<pinref part="C8" gate="G$1" pin="1"/>
<junction x="497.84" y="205.74"/>
<pinref part="C9" gate="G$1" pin="1"/>
<junction x="508" y="205.74"/>
<pinref part="C11" gate="G$1" pin="1"/>
<junction x="518.16" y="205.74"/>
<pinref part="C12" gate="G$1" pin="1"/>
<junction x="528.32" y="205.74"/>
<pinref part="C14" gate="G$1" pin="1"/>
<junction x="548.64" y="205.74"/>
<pinref part="C19" gate="G$1" pin="1"/>
<junction x="558.8" y="205.74"/>
<pinref part="C21" gate="G$1" pin="1"/>
<junction x="568.96" y="205.74"/>
<pinref part="C39" gate="G$1" pin="1"/>
<junction x="579.12" y="205.74"/>
<pinref part="C40" gate="G$1" pin="1"/>
<junction x="589.28" y="205.74"/>
</segment>
<segment>
<pinref part="R29" gate="G$1" pin="2"/>
<wire x1="543.56" y1="33.02" x2="543.56" y2="38.1" width="0.1524" layer="91"/>
<pinref part="P+9" gate="VCC" pin="VCC"/>
</segment>
<segment>
<pinref part="J10" gate="G$1" pin="1"/>
<wire x1="317.5" y1="170.18" x2="314.96" y2="170.18" width="0.1524" layer="91"/>
<pinref part="P+5" gate="VCC" pin="VCC"/>
<wire x1="314.96" y1="170.18" x2="314.96" y2="172.72" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C23" gate="G$1" pin="1"/>
<pinref part="C22" gate="G$1" pin="1"/>
<wire x1="233.68" y1="30.48" x2="233.68" y2="48.26" width="0.1524" layer="91"/>
<wire x1="233.68" y1="48.26" x2="233.68" y2="58.42" width="0.1524" layer="91"/>
<junction x="233.68" y="48.26"/>
<pinref part="U5" gate="G$1" pin="PVDD1"/>
<wire x1="233.68" y1="58.42" x2="231.14" y2="58.42" width="0.1524" layer="91"/>
<wire x1="233.68" y1="12.7" x2="233.68" y2="20.32" width="0.1524" layer="91"/>
<junction x="233.68" y="30.48"/>
<pinref part="P+4" gate="VCC" pin="VCC"/>
<pinref part="C24" gate="G$1" pin="1"/>
<wire x1="233.68" y1="20.32" x2="233.68" y2="30.48" width="0.1524" layer="91"/>
<junction x="233.68" y="20.32"/>
</segment>
<segment>
<pinref part="J24" gate="G$1" pin="VCC"/>
<pinref part="P+1" gate="VCC" pin="VCC"/>
<wire x1="-12.7" y1="165.1" x2="2.54" y2="165.1" width="0.1524" layer="91"/>
</segment>
</net>
<net name="DRV-HI-U" class="0">
<segment>
<pinref part="R17" gate="G$1" pin="1"/>
<wire x1="363.22" y1="137.16" x2="353.06" y2="137.16" width="0.1524" layer="91"/>
<label x="353.06" y="137.16" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="GH_A"/>
<wire x1="231.14" y1="104.14" x2="241.3" y2="104.14" width="0.1524" layer="91"/>
<label x="233.68" y="104.14" size="1.778" layer="95"/>
</segment>
</net>
<net name="DRV-LO-U" class="0">
<segment>
<pinref part="R18" gate="G$1" pin="1"/>
<wire x1="363.22" y1="116.84" x2="353.06" y2="116.84" width="0.1524" layer="91"/>
<label x="353.06" y="116.84" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="GL_A"/>
<wire x1="231.14" y1="99.06" x2="241.3" y2="99.06" width="0.1524" layer="91"/>
<label x="233.68" y="99.06" size="1.778" layer="95"/>
</segment>
</net>
<net name="SL-V" class="0">
<segment>
<wire x1="477.52" y1="109.22" x2="447.04" y2="109.22" width="0.1524" layer="91"/>
<label x="447.04" y="109.22" size="1.778" layer="95"/>
<wire x1="477.52" y1="109.22" x2="477.52" y2="114.3" width="0.1524" layer="91"/>
<pinref part="Q6" gate="1" pin="S"/>
<pinref part="R26" gate="G$1" pin="A"/>
<wire x1="477.52" y1="101.6" x2="477.52" y2="109.22" width="0.1524" layer="91"/>
<junction x="477.52" y="109.22"/>
<pinref part="Q8" gate="1" pin="S"/>
<wire x1="495.3" y1="114.3" x2="477.52" y2="114.3" width="0.1524" layer="91"/>
<junction x="477.52" y="114.3"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="SL_B"/>
<wire x1="231.14" y1="83.82" x2="241.3" y2="83.82" width="0.1524" layer="91"/>
<label x="233.68" y="83.82" size="1.778" layer="95"/>
</segment>
</net>
<net name="SHNT-V-HI" class="0">
<segment>
<pinref part="R14" gate="G$1" pin="1"/>
<wire x1="269.24" y1="53.34" x2="276.86" y2="53.34" width="0.1524" layer="91"/>
<label x="269.24" y="53.34" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R26" gate="G$1" pin="SA"/>
<wire x1="480.06" y1="101.6" x2="495.3" y2="101.6" width="0.1524" layer="91"/>
<label x="482.6" y="101.6" size="1.778" layer="95"/>
</segment>
</net>
<net name="SHNT-V-LO" class="0">
<segment>
<pinref part="R15" gate="G$1" pin="1"/>
<wire x1="269.24" y1="45.72" x2="276.86" y2="45.72" width="0.1524" layer="91"/>
<label x="269.24" y="45.72" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R26" gate="G$1" pin="SB"/>
<wire x1="480.06" y1="88.9" x2="495.3" y2="88.9" width="0.1524" layer="91"/>
<label x="482.6" y="88.9" size="1.778" layer="95"/>
</segment>
</net>
<net name="PHASE-V" class="0">
<segment>
<pinref part="R22" gate="G$1" pin="2"/>
<wire x1="436.88" y1="33.02" x2="436.88" y2="38.1" width="0.1524" layer="91"/>
<wire x1="436.88" y1="38.1" x2="447.04" y2="38.1" width="0.1524" layer="91"/>
<label x="441.96" y="38.1" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="SH_B"/>
<label x="233.68" y="88.9" size="1.778" layer="95"/>
<wire x1="251.46" y1="88.9" x2="231.14" y2="88.9" width="0.1524" layer="91"/>
<pinref part="C29" gate="G$1" pin="2"/>
<wire x1="256.54" y1="86.36" x2="251.46" y2="86.36" width="0.1524" layer="91"/>
<wire x1="251.46" y1="86.36" x2="251.46" y2="88.9" width="0.1524" layer="91"/>
</segment>
<segment>
<label x="495.3" y="127" size="1.778" layer="95"/>
<wire x1="495.3" y1="127" x2="510.54" y2="127" width="0.1524" layer="91"/>
<wire x1="477.52" y1="127" x2="447.04" y2="127" width="0.1524" layer="91"/>
<label x="447.04" y="127" size="1.778" layer="95"/>
<wire x1="495.3" y1="127" x2="477.52" y2="127" width="0.1524" layer="91"/>
<wire x1="477.52" y1="124.46" x2="477.52" y2="127" width="0.1524" layer="91"/>
<junction x="477.52" y="127"/>
<wire x1="477.52" y1="127" x2="477.52" y2="134.62" width="0.1524" layer="91"/>
<pinref part="Q6" gate="1" pin="D"/>
<pinref part="Q5" gate="1" pin="S"/>
<pinref part="Q7" gate="1" pin="S"/>
<wire x1="495.3" y1="134.62" x2="495.3" y2="127" width="0.1524" layer="91"/>
<junction x="495.3" y="127"/>
<pinref part="Q8" gate="1" pin="D"/>
<wire x1="495.3" y1="124.46" x2="495.3" y2="127" width="0.1524" layer="91"/>
<pinref part="J8" gate="G$1" pin="2"/>
</segment>
</net>
<net name="DRV-LO-V" class="0">
<segment>
<pinref part="R25" gate="G$1" pin="1"/>
<wire x1="457.2" y1="116.84" x2="447.04" y2="116.84" width="0.1524" layer="91"/>
<label x="447.04" y="116.84" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="GL_B"/>
<wire x1="231.14" y1="86.36" x2="241.3" y2="86.36" width="0.1524" layer="91"/>
<label x="233.68" y="86.36" size="1.778" layer="95"/>
</segment>
</net>
<net name="DRV-HI-V" class="0">
<segment>
<pinref part="R24" gate="G$1" pin="1"/>
<wire x1="457.2" y1="137.16" x2="447.04" y2="137.16" width="0.1524" layer="91"/>
<label x="447.04" y="137.16" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="GH_B"/>
<wire x1="231.14" y1="91.44" x2="241.3" y2="91.44" width="0.1524" layer="91"/>
<label x="233.68" y="91.44" size="1.778" layer="95"/>
</segment>
</net>
<net name="DRV-HI-W" class="0">
<segment>
<pinref part="R31" gate="G$1" pin="1"/>
<wire x1="553.72" y1="137.16" x2="543.56" y2="137.16" width="0.1524" layer="91"/>
<label x="543.56" y="137.16" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="GH_C"/>
<wire x1="231.14" y1="78.74" x2="241.3" y2="78.74" width="0.1524" layer="91"/>
<label x="233.68" y="78.74" size="1.778" layer="95"/>
</segment>
</net>
<net name="PHASE-W" class="0">
<segment>
<pinref part="R27" gate="G$1" pin="2"/>
<wire x1="490.22" y1="33.02" x2="490.22" y2="38.1" width="0.1524" layer="91"/>
<wire x1="490.22" y1="38.1" x2="500.38" y2="38.1" width="0.1524" layer="91"/>
<label x="495.3" y="38.1" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="SH_C"/>
<label x="233.68" y="76.2" size="1.778" layer="95"/>
<wire x1="251.46" y1="76.2" x2="231.14" y2="76.2" width="0.1524" layer="91"/>
<pinref part="C30" gate="G$1" pin="2"/>
<wire x1="256.54" y1="73.66" x2="251.46" y2="73.66" width="0.1524" layer="91"/>
<wire x1="251.46" y1="73.66" x2="251.46" y2="76.2" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="574.04" y1="127" x2="543.56" y2="127" width="0.1524" layer="91"/>
<label x="543.56" y="127" size="1.778" layer="95"/>
<wire x1="574.04" y1="124.46" x2="574.04" y2="127" width="0.1524" layer="91"/>
<wire x1="574.04" y1="127" x2="574.04" y2="134.62" width="0.1524" layer="91"/>
<junction x="574.04" y="127"/>
<label x="591.82" y="127" size="1.778" layer="95"/>
<wire x1="591.82" y1="127" x2="609.6" y2="127" width="0.1524" layer="91"/>
<wire x1="574.04" y1="127" x2="591.82" y2="127" width="0.1524" layer="91"/>
<pinref part="Q10" gate="1" pin="D"/>
<pinref part="Q9" gate="1" pin="S"/>
<pinref part="Q12" gate="1" pin="D"/>
<wire x1="591.82" y1="124.46" x2="591.82" y2="127" width="0.1524" layer="91"/>
<junction x="591.82" y="127"/>
<pinref part="Q11" gate="1" pin="S"/>
<wire x1="591.82" y1="134.62" x2="591.82" y2="127" width="0.1524" layer="91"/>
<pinref part="J9" gate="G$1" pin="2"/>
</segment>
</net>
<net name="DRV-LO-W" class="0">
<segment>
<pinref part="R32" gate="G$1" pin="1"/>
<wire x1="553.72" y1="116.84" x2="543.56" y2="116.84" width="0.1524" layer="91"/>
<label x="543.56" y="116.84" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="GL_C"/>
<wire x1="231.14" y1="73.66" x2="241.3" y2="73.66" width="0.1524" layer="91"/>
<label x="233.68" y="73.66" size="1.778" layer="95"/>
</segment>
</net>
<net name="SL-W" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="SL_C"/>
<wire x1="231.14" y1="71.12" x2="241.3" y2="71.12" width="0.1524" layer="91"/>
<label x="233.68" y="71.12" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J21" gate="1" pin="P$1"/>
<wire x1="548.64" y1="106.68" x2="530.86" y2="106.68" width="0.1524" layer="91"/>
<label x="530.86" y="106.68" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="BST_C"/>
<pinref part="C30" gate="G$1" pin="1"/>
<wire x1="231.14" y1="81.28" x2="256.54" y2="81.28" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="BST_B"/>
<pinref part="C29" gate="G$1" pin="1"/>
<wire x1="231.14" y1="93.98" x2="256.54" y2="93.98" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="BST_A"/>
<pinref part="C28" gate="G$1" pin="1"/>
<wire x1="231.14" y1="106.68" x2="256.54" y2="106.68" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="SN1"/>
<pinref part="C26" gate="G$1" pin="1"/>
<wire x1="231.14" y1="68.58" x2="248.92" y2="68.58" width="0.1524" layer="91"/>
<pinref part="R12" gate="G$1" pin="2"/>
<wire x1="248.92" y1="68.58" x2="259.08" y2="68.58" width="0.1524" layer="91"/>
<junction x="248.92" y="68.58"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="SP1"/>
<wire x1="231.14" y1="66.04" x2="243.84" y2="66.04" width="0.1524" layer="91"/>
<wire x1="243.84" y1="66.04" x2="243.84" y2="60.96" width="0.1524" layer="91"/>
<pinref part="C26" gate="G$1" pin="2"/>
<wire x1="243.84" y1="60.96" x2="248.92" y2="60.96" width="0.1524" layer="91"/>
<pinref part="R13" gate="G$1" pin="2"/>
<wire x1="248.92" y1="60.96" x2="259.08" y2="60.96" width="0.1524" layer="91"/>
<junction x="248.92" y="60.96"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="SN2"/>
<wire x1="231.14" y1="63.5" x2="241.3" y2="63.5" width="0.1524" layer="91"/>
<wire x1="241.3" y1="63.5" x2="241.3" y2="53.34" width="0.1524" layer="91"/>
<pinref part="C27" gate="G$1" pin="1"/>
<wire x1="241.3" y1="53.34" x2="248.92" y2="53.34" width="0.1524" layer="91"/>
<pinref part="R14" gate="G$1" pin="2"/>
<wire x1="248.92" y1="53.34" x2="259.08" y2="53.34" width="0.1524" layer="91"/>
<junction x="248.92" y="53.34"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="SP2"/>
<wire x1="231.14" y1="60.96" x2="238.76" y2="60.96" width="0.1524" layer="91"/>
<wire x1="238.76" y1="60.96" x2="238.76" y2="45.72" width="0.1524" layer="91"/>
<pinref part="C27" gate="G$1" pin="2"/>
<wire x1="238.76" y1="45.72" x2="248.92" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R15" gate="G$1" pin="2"/>
<wire x1="248.92" y1="45.72" x2="259.08" y2="45.72" width="0.1524" layer="91"/>
<junction x="248.92" y="45.72"/>
</segment>
</net>
<net name="CP1" class="0">
<segment>
<pinref part="C18" gate="G$1" pin="1"/>
<pinref part="U5" gate="G$1" pin="CP1"/>
<wire x1="175.26" y1="93.98" x2="195.58" y2="93.98" width="0.1524" layer="91"/>
</segment>
</net>
<net name="CP2" class="0">
<segment>
<pinref part="C18" gate="G$1" pin="2"/>
<wire x1="167.64" y1="93.98" x2="165.1" y2="93.98" width="0.1524" layer="91"/>
<wire x1="165.1" y1="93.98" x2="165.1" y2="91.44" width="0.1524" layer="91"/>
<pinref part="U5" gate="G$1" pin="CP2"/>
<wire x1="165.1" y1="91.44" x2="195.58" y2="91.44" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="GVDD"/>
<wire x1="195.58" y1="96.52" x2="162.56" y2="96.52" width="0.1524" layer="91"/>
<pinref part="C15" gate="G$1" pin="2"/>
<wire x1="162.56" y1="96.52" x2="152.4" y2="96.52" width="0.1524" layer="91"/>
<pinref part="C16" gate="G$1" pin="2"/>
<junction x="162.56" y="96.52"/>
</segment>
</net>
<net name="N$20" class="0">
<segment>
<pinref part="C17" gate="G$1" pin="2"/>
<pinref part="U5" gate="G$1" pin="DVDD"/>
<wire x1="195.58" y1="71.12" x2="162.56" y2="71.12" width="0.1524" layer="91"/>
<pinref part="U5" gate="G$1" pin="OC_ADJ"/>
<wire x1="195.58" y1="101.6" x2="137.16" y2="101.6" width="0.1524" layer="91"/>
<wire x1="137.16" y1="101.6" x2="137.16" y2="71.12" width="0.1524" layer="91"/>
<wire x1="137.16" y1="71.12" x2="162.56" y2="71.12" width="0.1524" layer="91"/>
<junction x="162.56" y="71.12"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="AVDD"/>
<pinref part="C20" gate="G$1" pin="2"/>
<wire x1="195.58" y1="60.96" x2="187.96" y2="60.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$21" class="0">
<segment>
<pinref part="R10" gate="G$1" pin="1"/>
<pinref part="U5" gate="G$1" pin="DTC"/>
<wire x1="175.26" y1="111.76" x2="195.58" y2="111.76" width="0.1524" layer="91"/>
</segment>
</net>
<net name="DRV_OCTW" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="NOCTW"/>
<wire x1="177.8" y1="116.84" x2="195.58" y2="116.84" width="0.1524" layer="91"/>
<label x="177.8" y="116.84" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="-15.24" y1="93.98" x2="2.54" y2="93.98" width="0.1524" layer="91"/>
<label x="-12.7" y="93.98" size="1.778" layer="95"/>
<pinref part="J24" gate="G$1" pin="D1/USART/CLK---PD1/PWMB/SCK"/>
</segment>
</net>
<net name="V-W" class="0">
<segment>
<pinref part="R28" gate="G$1" pin="2"/>
<pinref part="R27" gate="G$1" pin="1"/>
<wire x1="490.22" y1="17.78" x2="490.22" y2="20.32" width="0.1524" layer="91"/>
<wire x1="490.22" y1="20.32" x2="490.22" y2="22.86" width="0.1524" layer="91"/>
<wire x1="490.22" y1="20.32" x2="459.74" y2="20.32" width="0.1524" layer="91"/>
<junction x="490.22" y="20.32"/>
<label x="459.74" y="20.32" size="1.778" layer="95"/>
<wire x1="490.22" y1="20.32" x2="497.84" y2="20.32" width="0.1524" layer="91"/>
<pinref part="C37" gate="G$1" pin="1"/>
<wire x1="497.84" y1="20.32" x2="497.84" y2="15.24" width="0.1524" layer="91"/>
<pinref part="D6" gate="G$1" pin="A"/>
<junction x="497.84" y="20.32"/>
</segment>
<segment>
<wire x1="-15.24" y1="127" x2="2.54" y2="127" width="0.1524" layer="91"/>
<label x="-12.7" y="127" size="1.778" layer="95"/>
<pinref part="J24" gate="G$1" pin="A6/ADC---PB6/ADC/ADCOUT"/>
</segment>
</net>
<net name="N$25" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="M_OC"/>
<pinref part="R9" gate="G$1" pin="1"/>
<wire x1="195.58" y1="106.68" x2="167.64" y2="106.68" width="0.1524" layer="91"/>
</segment>
</net>
<net name="I-V" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="SO1"/>
<wire x1="195.58" y1="66.04" x2="177.8" y2="66.04" width="0.1524" layer="91"/>
<label x="177.8" y="66.04" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="-15.24" y1="137.16" x2="2.54" y2="137.16" width="0.1524" layer="91"/>
<label x="-12.7" y="137.16" size="1.778" layer="95"/>
<pinref part="J24" gate="G$1" pin="A2/ADC/DAC---PB2/ADC/DAC"/>
</segment>
</net>
<net name="I-W" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="SO2"/>
<wire x1="195.58" y1="63.5" x2="177.8" y2="63.5" width="0.1524" layer="91"/>
<label x="177.8" y="63.5" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="-15.24" y1="134.62" x2="2.54" y2="134.62" width="0.1524" layer="91"/>
<label x="-12.7" y="134.62" size="1.778" layer="95"/>
<pinref part="J24" gate="G$1" pin="A3/ADC/DAC---PB3/ADC/DAC"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="R11" gate="G$1" pin="2"/>
<pinref part="U5" gate="G$1" pin="EN_BUCK"/>
<wire x1="243.84" y1="124.46" x2="231.14" y2="124.46" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$19" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="BIAS"/>
<wire x1="231.14" y1="109.22" x2="236.22" y2="109.22" width="0.1524" layer="91"/>
<wire x1="236.22" y1="109.22" x2="236.22" y2="114.3" width="0.1524" layer="91"/>
<pinref part="C25" gate="G$1" pin="1"/>
<wire x1="236.22" y1="114.3" x2="238.76" y2="114.3" width="0.1524" layer="91"/>
</segment>
</net>
<net name="DRV_FAULT" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="NFAULT"/>
<wire x1="177.8" y1="114.3" x2="195.58" y2="114.3" width="0.1524" layer="91"/>
<label x="177.8" y="114.3" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="-15.24" y1="91.44" x2="2.54" y2="91.44" width="0.1524" layer="91"/>
<label x="-12.7" y="91.44" size="1.778" layer="95"/>
<pinref part="J24" gate="G$1" pin="D2/USART/RX---PD2/PWMC/MISO/UARTRX"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="R18" gate="G$1" pin="2"/>
<wire x1="373.38" y1="116.84" x2="378.46" y2="116.84" width="0.1524" layer="91"/>
<pinref part="Q2" gate="1" pin="G"/>
<pinref part="Q4" gate="1" pin="G"/>
<wire x1="378.46" y1="116.84" x2="391.16" y2="116.84" width="0.1524" layer="91"/>
<junction x="378.46" y="116.84"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="R17" gate="G$1" pin="2"/>
<wire x1="378.46" y1="137.16" x2="373.38" y2="137.16" width="0.1524" layer="91"/>
<pinref part="Q1" gate="1" pin="G"/>
<pinref part="Q3" gate="1" pin="G"/>
<wire x1="378.46" y1="137.16" x2="391.16" y2="137.16" width="0.1524" layer="91"/>
<junction x="378.46" y="137.16"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="R24" gate="G$1" pin="2"/>
<wire x1="467.36" y1="137.16" x2="472.44" y2="137.16" width="0.1524" layer="91"/>
<pinref part="Q5" gate="1" pin="G"/>
<pinref part="Q7" gate="1" pin="G"/>
<wire x1="472.44" y1="137.16" x2="490.22" y2="137.16" width="0.1524" layer="91"/>
<junction x="472.44" y="137.16"/>
</segment>
</net>
<net name="N$24" class="0">
<segment>
<pinref part="R25" gate="G$1" pin="2"/>
<wire x1="467.36" y1="116.84" x2="472.44" y2="116.84" width="0.1524" layer="91"/>
<pinref part="Q6" gate="1" pin="G"/>
<pinref part="Q8" gate="1" pin="G"/>
<wire x1="472.44" y1="116.84" x2="490.22" y2="116.84" width="0.1524" layer="91"/>
<junction x="472.44" y="116.84"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="R31" gate="G$1" pin="2"/>
<wire x1="563.88" y1="137.16" x2="568.96" y2="137.16" width="0.1524" layer="91"/>
<pinref part="Q9" gate="1" pin="G"/>
<pinref part="Q11" gate="1" pin="G"/>
<wire x1="568.96" y1="137.16" x2="586.74" y2="137.16" width="0.1524" layer="91"/>
<junction x="568.96" y="137.16"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="R32" gate="G$1" pin="2"/>
<wire x1="568.96" y1="116.84" x2="563.88" y2="116.84" width="0.1524" layer="91"/>
<pinref part="Q10" gate="1" pin="G"/>
<pinref part="Q12" gate="1" pin="G"/>
<wire x1="586.74" y1="116.84" x2="568.96" y2="116.84" width="0.1524" layer="91"/>
<junction x="568.96" y="116.84"/>
</segment>
</net>
<net name="ENC_MISO" class="0">
<segment>
<wire x1="-15.24" y1="55.88" x2="2.54" y2="55.88" width="0.1524" layer="91"/>
<label x="-12.7" y="55.88" size="1.778" layer="95"/>
<pinref part="J24" gate="G$1" pin="E6/USART/RX---PE6/UARTRX/MISO"/>
</segment>
<segment>
<wire x1="198.12" y1="152.4" x2="215.9" y2="152.4" width="0.1524" layer="91"/>
<label x="200.66" y="152.4" size="1.778" layer="95"/>
<pinref part="J2" gate="A" pin="3"/>
</segment>
</net>
<net name="ENC_SCK" class="0">
<segment>
<wire x1="-15.24" y1="58.42" x2="2.54" y2="58.42" width="0.1524" layer="91"/>
<label x="-12.7" y="58.42" size="1.778" layer="95"/>
<pinref part="J24" gate="G$1" pin="E5/USART/SCK---PE5/PWMB/SCK"/>
</segment>
<segment>
<wire x1="198.12" y1="149.86" x2="215.9" y2="149.86" width="0.1524" layer="91"/>
<label x="200.66" y="149.86" size="1.778" layer="95"/>
<pinref part="J2" gate="A" pin="4"/>
</segment>
</net>
<net name="ENC_CSN" class="0">
<segment>
<wire x1="-15.24" y1="60.96" x2="2.54" y2="60.96" width="0.1524" layer="91"/>
<label x="-12.7" y="60.96" size="1.778" layer="95"/>
<pinref part="J24" gate="G$1" pin="E4/USART/CSN---PE4/PWMA/CSN"/>
</segment>
<segment>
<wire x1="198.12" y1="147.32" x2="215.9" y2="147.32" width="0.1524" layer="91"/>
<label x="200.66" y="147.32" size="1.778" layer="95"/>
<pinref part="J2" gate="A" pin="5"/>
</segment>
</net>
<net name="ENC_MOSI" class="0">
<segment>
<wire x1="-15.24" y1="53.34" x2="2.54" y2="53.34" width="0.1524" layer="91"/>
<label x="-12.7" y="53.34" size="1.778" layer="95"/>
<pinref part="J24" gate="G$1" pin="E7/USART/TX---PE7/UARTTX/MOSI"/>
</segment>
<segment>
<wire x1="198.12" y1="144.78" x2="215.9" y2="144.78" width="0.1524" layer="91"/>
<label x="200.66" y="144.78" size="1.778" layer="95"/>
<pinref part="J2" gate="A" pin="6"/>
</segment>
</net>
<net name="DRV_EN" class="0">
<segment>
<pinref part="S2" gate="G$1" pin="3"/>
<wire x1="45.72" y1="195.58" x2="30.48" y2="195.58" width="0.1524" layer="91"/>
<label x="30.48" y="195.58" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="-15.24" y1="96.52" x2="2.54" y2="96.52" width="0.1524" layer="91"/>
<label x="-12.7" y="96.52" size="1.778" layer="95"/>
<pinref part="J24" gate="G$1" pin="D0/USART/CSN---PD0/PWMA/CSN"/>
</segment>
</net>
<net name="DRV_EN_DRV" class="0">
<segment>
<pinref part="S2" gate="G$1" pin="2"/>
<wire x1="55.88" y1="193.04" x2="83.82" y2="193.04" width="0.1524" layer="91"/>
<label x="66.04" y="193.04" size="1.778" layer="95"/>
<pinref part="R8" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="EN_GATE"/>
<wire x1="195.58" y1="88.9" x2="177.8" y2="88.9" width="0.1524" layer="91"/>
<label x="177.8" y="88.9" size="1.778" layer="95"/>
</segment>
</net>
<net name="PWMHIU" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="INH_A"/>
<wire x1="195.58" y1="86.36" x2="177.8" y2="86.36" width="0.1524" layer="91"/>
<label x="177.8" y="86.36" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="-15.24" y1="106.68" x2="2.54" y2="106.68" width="0.1524" layer="91"/>
<label x="-12.7" y="106.68" size="1.778" layer="95"/>
<pinref part="J24" gate="G$1" pin="C5/PWMCHS---PC5/PWMCHS/SCK"/>
</segment>
</net>
<net name="PWMLOU" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="INL_A"/>
<wire x1="195.58" y1="83.82" x2="177.8" y2="83.82" width="0.1524" layer="91"/>
<label x="177.8" y="83.82" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="-15.24" y1="109.22" x2="2.54" y2="109.22" width="0.1524" layer="91"/>
<label x="-12.7" y="109.22" size="1.778" layer="95"/>
<pinref part="J24" gate="G$1" pin="C4/PWMCLS---PC4/PWMCLS/CSN"/>
</segment>
</net>
<net name="PWMHIV" class="0">
<segment>
<wire x1="-15.24" y1="111.76" x2="2.54" y2="111.76" width="0.1524" layer="91"/>
<label x="-12.7" y="111.76" size="1.778" layer="95"/>
<pinref part="J24" gate="G$1" pin="C3/PWMBHS---PC3/PWMBHS/MOSI/UARTTX"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="INH_B"/>
<wire x1="195.58" y1="81.28" x2="177.8" y2="81.28" width="0.1524" layer="91"/>
<label x="177.8" y="81.28" size="1.778" layer="95"/>
</segment>
</net>
<net name="PWMHIW" class="0">
<segment>
<wire x1="-15.24" y1="116.84" x2="2.54" y2="116.84" width="0.1524" layer="91"/>
<label x="-12.7" y="116.84" size="1.778" layer="95"/>
<pinref part="J24" gate="G$1" pin="C1/PWMAHS---PC1/PWMAHS/SCK/SCL"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="INH_C"/>
<wire x1="195.58" y1="76.2" x2="177.8" y2="76.2" width="0.1524" layer="91"/>
<label x="177.8" y="76.2" size="1.778" layer="95"/>
</segment>
</net>
<net name="PWMLOV" class="0">
<segment>
<wire x1="-15.24" y1="114.3" x2="2.54" y2="114.3" width="0.1524" layer="91"/>
<label x="-12.7" y="114.3" size="1.778" layer="95"/>
<pinref part="J24" gate="G$1" pin="C2/PWMBLS---PC2/PWMBLS/MISO/UARTRX"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="INL_B"/>
<wire x1="195.58" y1="78.74" x2="177.8" y2="78.74" width="0.1524" layer="91"/>
<label x="177.8" y="78.74" size="1.778" layer="95"/>
</segment>
</net>
<net name="PWMLOW" class="0">
<segment>
<wire x1="-15.24" y1="119.38" x2="2.54" y2="119.38" width="0.1524" layer="91"/>
<label x="-12.7" y="119.38" size="1.778" layer="95"/>
<pinref part="J24" gate="G$1" pin="C0/PWMALS---PC0/PWMALS/CSN/SDA"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="INL_C"/>
<wire x1="195.58" y1="73.66" x2="177.8" y2="73.66" width="0.1524" layer="91"/>
<label x="177.8" y="73.66" size="1.778" layer="95"/>
</segment>
</net>
<net name="RTD" class="0">
<segment>
<wire x1="-15.24" y1="139.7" x2="2.54" y2="139.7" width="0.1524" layer="91"/>
<label x="-12.7" y="139.7" size="1.778" layer="95"/>
<pinref part="J24" gate="G$1" pin="A1/ADC---PB1/ADC"/>
</segment>
</net>
<net name="SHNT-U-HI" class="0">
<segment>
<pinref part="R12" gate="G$1" pin="1"/>
<wire x1="269.24" y1="68.58" x2="276.86" y2="68.58" width="0.1524" layer="91"/>
<label x="269.24" y="68.58" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R19" gate="G$1" pin="SA"/>
<wire x1="386.08" y1="101.6" x2="403.86" y2="101.6" width="0.1524" layer="91"/>
<label x="388.62" y="101.6" size="1.778" layer="95"/>
</segment>
</net>
<net name="SHNT-U-LO" class="0">
<segment>
<pinref part="R13" gate="G$1" pin="1"/>
<wire x1="269.24" y1="60.96" x2="276.86" y2="60.96" width="0.1524" layer="91"/>
<label x="269.24" y="60.96" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R19" gate="G$1" pin="SB"/>
<wire x1="386.08" y1="88.9" x2="403.86" y2="88.9" width="0.1524" layer="91"/>
<label x="388.62" y="88.9" size="1.778" layer="95"/>
</segment>
</net>
<net name="SL-U" class="0">
<segment>
<pinref part="Q2" gate="1" pin="S"/>
<wire x1="383.54" y1="109.22" x2="383.54" y2="114.3" width="0.1524" layer="91"/>
<wire x1="383.54" y1="109.22" x2="353.06" y2="109.22" width="0.1524" layer="91"/>
<label x="353.06" y="109.22" size="1.778" layer="95"/>
<pinref part="R19" gate="G$1" pin="A"/>
<wire x1="383.54" y1="101.6" x2="383.54" y2="109.22" width="0.1524" layer="91"/>
<junction x="383.54" y="109.22"/>
<pinref part="Q4" gate="1" pin="S"/>
<wire x1="396.24" y1="114.3" x2="383.54" y2="114.3" width="0.1524" layer="91"/>
<junction x="383.54" y="114.3"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="SL_A"/>
<wire x1="231.14" y1="96.52" x2="241.3" y2="96.52" width="0.1524" layer="91"/>
<label x="233.68" y="96.52" size="1.778" layer="95"/>
</segment>
</net>
<net name="DC_CAL" class="0">
<segment>
<wire x1="-15.24" y1="73.66" x2="2.54" y2="73.66" width="0.1524" layer="91"/>
<label x="-12.7" y="73.66" size="1.778" layer="95"/>
<pinref part="J24" gate="G$1" pin="E1/SCL---PE1"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="DC_CAL"/>
<wire x1="195.58" y1="99.06" x2="177.8" y2="99.06" width="0.1524" layer="91"/>
<label x="177.8" y="99.06" size="1.778" layer="95"/>
</segment>
</net>
<net name="M_GAIN" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="GAIN"/>
<wire x1="195.58" y1="104.14" x2="177.8" y2="104.14" width="0.1524" layer="91"/>
<label x="177.8" y="104.14" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="-15.24" y1="76.2" x2="2.54" y2="76.2" width="0.1524" layer="91"/>
<label x="-12.7" y="76.2" size="1.778" layer="95"/>
<pinref part="J24" gate="G$1" pin="E0/SDA---PE0"/>
</segment>
</net>
<net name="M_PWM" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="M_PWM"/>
<wire x1="195.58" y1="109.22" x2="177.8" y2="109.22" width="0.1524" layer="91"/>
<label x="177.8" y="109.22" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="-15.24" y1="88.9" x2="2.54" y2="88.9" width="0.1524" layer="91"/>
<label x="-12.7" y="88.9" size="1.778" layer="95"/>
<pinref part="J24" gate="G$1" pin="D3/USART/TX---PD3/PWMD/MOSI/UARTTX"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
