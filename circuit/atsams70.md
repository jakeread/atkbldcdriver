# Circuit Documentation for v0.1

Using the ATSAMS70

Warning! Train-of-though documentation!

#### FET Selection

Using 2x20 pin header on ATSAMS70 Switch - 

I want to use these fancy DirectFets because I know they can handle really big currents in astoundingly small packages - aiming at a 2kW spindle here. The PN<sup>1</sup> I found is IRF6648TRPBF, switches 60v at 86a - this is rated by the silicon<sup>2</sup>, so won't really get that close, but it's a big number nonetheless - and even at ~ 40% of that I'm running near 2KW (also, power is switched with 6 - so load is a bit split up, though the picture is not so simple - i.e. during moments of the phase only one is 'full on' on the top - or bottom - side of the phases.)

I also found an eagle library for this package, nice. 

These are apparently a pain to solder - you need reflow. I'm OK with that, as I think reflow ovens are not hard to build... I know it makes it a little bit un-fabbable, but it makes me stoked about this project. So here we are.

Now - an aside, to introduce you to [Ben Katz](http://build-its-inprogress.blogspot.com/search/label/Motor%20Control) a researcher at MIT's Biomimetic Robotics Lab, who is an expert in motor control, and keeps this great blog about it. Also, this is [Benjamin Vedder](http://vedder.se/2015/01/vesc-open-source-esc/)<sup>3</sup> - who's project is hugely popular in the Electric Longboard community. Niche. Great software, great board design.

#### Layout

I start by laying out the Three Half-Bridges that drive the Motor. The FET I am using actually has multiple pins per connection - so I pulled those out on the symbol to make it clearer what I needed to connect. Here's my schematic for the gates:

![gates schematic](https://gitlab.cba.mit.edu/jakeread/mkbldcdriver/raw/master/images/schematic-gates.jpg)

A few notes - I'm using Labels instead of directly 'routing' on the schematic. This helps keep things diagrammatic... And not messy. You'll notice my Shunt symbols have separate legs for sensing - you'll get that when you see the layout. I also pull out a pin from the low-side of the gates that the gate-driver uses to sense the voltage there (actually not ~ exactly ground). I also have a few voltage dividers setup on phases, which is useful when doing closed-loop sensorless BLDC or FOC control. Not something I imagine I'll have time to implement in code, but good to have. 

Now I try a first-shot at the gate layout on the board, to get a sense for generally where things will end up. 

![gates single layout](https://gitlab.cba.mit.edu/jakeread/mkbldcdriver/raw/master/images/board-single-halfbridge.jpg)

This is really just to figure out what the topology is - what wraps around what. When I really route the big juice, I'll be drawing big polygons that I can do copper pours in. I like this particular layout because I have a 'side' for each big current pull - my shunt is happy with a big tail to some groundplane I'll lay down over there, the phase (in between the FETS) gets a nice edge to face-out to where I'll put motor lines (and that pesky voltage divider), and I have another happy, big edge to dump VCC on. I can even sneak the gate-drives in without any vias, nice. 

Next I'm going to draw the DRV8302 Schematic and Footprint. Oy.

![drv device](https://gitlab.cba.mit.edu/jakeread/mkbldcdriver/raw/master/images/drv-device.jpg)

TI Provides an excellent example layout - I'm using this as a reference as I draw the circuit.

![drv reference](https://gitlab.cba.mit.edu/jakeread/mkbldcdriver/raw/master/images/drv-reference.jpg)

I got the DRV8302 all schematic'd out, and put a rough plan in place on the board. This looks a bit complicated .... That's because this DRV8302 packs a lot of circuit into one die. I'm actually not sure if it is only one die... You can see this all here: 

![drv blocks](https://gitlab.cba.mit.edu/jakeread/mkbldcdriver/raw/master/images/drv-blocks.jpg)

Here's my schematic block for just the DRV

![drv schematic](https://gitlab.cba.mit.edu/jakeread/mkbldcdriver/raw/master/images/schematic-drv.jpg)

There's gate drivers - that's the real business that we're after - this comes with a swath of external capacitors. Critically, BST_x and CP1, CP2 connected capacitors are Charge Pump caps - these are what the DRV uses to crank voltage into the high-side gates - very important that these find a good home really close to the DRV. 

GVDD and DVDD are still a bit of a mystery to me - internal voltages in the DRV that want external bypass capacitors. 

There's also the current sense amplifiers - the bottom chunk of the chip. These amps *TODO: explain current sensing* and they get a tiny filter on the inputs, a reference voltage.

On the 'top' of the chip is a buck voltage regulator, a world into itself. The buck gets a whole whack of support - a BST_BK capacitor, a whole bunch of R_Sense circuitry, etc. 

And then there are a few fault outputs (that I connect to LEDS for super-simple debugging, but should properly trigger interrupts on a microcontroller), some settings-related pins, etc. It's a lot, and with more time I would write my process for this up in detail. Wrestling with this chip taught me a lot about EE.

OK. Next, I'm going to try to reconcile the pin-inputs I need for this driver with the pin-outputs I've developed for Machine Kit. Briefly, I know I'm going to need 6 PWM Pins (3 phases x hi/lo) and 5 ADC pins - two for current sensing *TODO: explain kirchoff's summing*, and 3 for phase voltage measurement. Actually 6, because I want to measure the input voltage as well. So I already know I need (want) another ADC pin on the MK Header. This is a lot of analog (er, PWM-ish) circuitry! I'm glad I have this test case in developing the MK Switch. Turns out I am maxed on the analog pins available on the ATSAMS70. So I have to jettison measuring source voltage, which is OK when we are not running on batteries - but kind of a bummer to have that hardware limitation. Could open one phase to high and measure then, I think, but wouldn't be able to do it continuously during operation. That's fine. 

I'm also going to add a header / plug for 'other things' - I'll use the SPI pins, that I think I can set as GPIO as well - I want to be able to talk to SPI devices (sensors, etc) and / or ABI input encoders. Wow. I am biting off a lot. 

Also - I want to push 48v into this chip (I am planning on most machines running at 48V, the power supplies are readily available, and 48v is the right amount for a KW spindle - says reasons?). This means I have to push a bunch of big capacitors onto the chip - 10uF at 100v (next step up) is a 2220 package - big !	

OK - I finally got my schematic dialed down where I wanted it, and footprints worked out. I'm going to start routing.

#### Routing

![routing begins](https://gitlab.cba.mit.edu/jakeread/mkbldcdriver/raw/master/images/routing-begins.jpg)

I have one ground plane (bottom trace) and three voltage planes on the top - a VCC (motor voltage, will be nominally 48v), a +5V (the output from the buck regulator, also - turns out I need this on some SPI / encoder devices, nice bonus) and a +3V3 - that's my logic level - the DRV uses 5v logic, but, all good.

I got started routing the inputs to the DRV, working over to the drive side. Once there, I focused on making the FETS and Caps etc... power side... work well. I'm also keeping an eye on my ground plane - a big problem with two-sided routing is that you can 'pinch' the plane out in some cases. This was a big issue when I routed that 100-pin switch, here it seems a bit easier - there's less going on overall - the scale is a bit different.

![routing halfway](https://gitlab.cba.mit.edu/jakeread/mkbldcdriver/raw/master/images/routing-halfway.jpg)

I got this routed, and I'm feeling good about the board. I turned on only the layers that will actually show up in fab - so the solder mask, traces, holes, etc - really I want to take some time now to make a decent silkscreen - this can make component placing much easier!

![routing presilk](https://gitlab.cba.mit.edu/jakeread/mkbldcdriver/raw/master/images/routing-presilk.jpg)

For adding silk, I tried to go about doing it by adding silk to the components - not just drawing them on the board.

OK, actually I want to keep the silk really clean. Things should be beautiful.

I sent these out, and sent an order to digikey for parts. Now some waiting. Here's a list of notes, as they occur, for next time.

## Fab: the board

OK, I got these boards in from the fab. Noice. I also splurged for the solder paste stencil (spending the CBA's money, thanks CBA!) - actually it's only $15 to get a stencil, and it saves me 2hr/board that I assemble, so if time = money... well. 

![fab stencil and board](https://gitlab.cba.mit.edu/jakeread/mkbldcdriver/raw/master/images/fab-stencil-and-board.jpg)

I'm well into reflowing boards (it's amazing, and has a surprisingly low overhead - highly recommended) and this is kind of the next step. The stencil lets me apply all of my solder paste at once, a huge time-saver, and prevents me from accidentally applying too much and shorting pins together. [Here is a great tutorial](https://www.sparkfun.com/tutorials/58) from Sparkfun on how to do this.

After I lay the paste down, it takes about ~35 minutes to lay the components down. If I did this in parallel, I would guess I could do 10 boards in ~1.5 hours, probably less if I got really into the zone and had a nice workstation.

In any case, here's the back of one of those DirectFets and it's associated footprint to the left. Here you can see the heckin' silicone die RIGHT THERE underneath the tin. I'm not super sure that I nailed this footprint, so we'll see what happens when I try to boot it up.

![fab directfet](https://gitlab.cba.mit.edu/jakeread/mkbldcdriver/raw/master/images/fab-directfet.jpg)

A group of components pre-reflow:

![fab prereflow](https://gitlab.cba.mit.edu/jakeread/mkbldcdriver/raw/master/images/fab-prereflow.jpg)

And afterwards - I am excited that these 56-HTSSOP pins on the DRV8302 are not welding together. Go solder paste, go.

![fab postreflow](https://gitlab.cba.mit.edu/jakeread/mkbldcdriver/raw/master/images/fab-postreflow.jpg)

And completed - looks nice!

![fab smd complete](https://gitlab.cba.mit.edu/jakeread/mkbldcdriver/raw/master/images/fab-smd-complete.jpg)

## Testing

Here's my setup:

![test getdown](https://gitlab.cba.mit.edu/jakeread/mkbldcdriver/raw/master/images/test-stand.jpg)

And I'm using a 24V Supply - I designed & rated this to 83V (capacitors are largely 100v, 83v rating comes from the DRV8302 and the FETS). But I want to stay safe and steer clear of burnouts for now. 

I carefully checked all of my pinouts and double-checked that I was sending a PWM signal that I liked, then I turned it on! I had some success, and then a stall. I *thought* I saw some smoke, so not sure what's up. 

I also noticed the power lines to the ATSAM chip started to flash - so I am having a power supply issue to my brain-board. This whole situation is more complicated than it needs to be. The DRV has a 5V Buck regulator, and uses that 5V Signal on a few pins... so I can't just throw it out. The ATSAM board has a 5V line, that it passes through a regulator to get the 3v3 it needs.

I imagine the 5V Buck Regulator on the DRV8302 is unhappy - it's actually a fairly complex thing, using some feedback loops to control a PWM cycle, that then is filtered with an LC on the output to generate a smooth 5V. I imagine this is going unstable.

What I did to diagnose the whole thing was connect my logic analyzer to the 5V line, 3V3 line, and to my three phases. Note - not *directly* to my three phases, as they will be being driven to 24v, and I'm quite sure I would fry my logic analyzer. I already have voltage dividers setup for the microcontroller to read the phase voltages, so here I'm just reading off of those. Nice.

![test getdown](https://gitlab.cba.mit.edu/jakeread/mkbldcdriver/raw/master/images/test-buck-unbuck.jpg)

The first three are my gate voltages - they're being driven very nicely, thanks very much. The bottom two lines are my 5V (Channel 3) and 3V3 lines (Channel 4). So, this is dropping, the pwms are becoming very sad, and this oscillation starts.

It looks like I can remedy this by just supplying also 3V3 with my UART Cable - although I know I'll be introducing basically conflicting 3V3 lines, which is bad. We'll give it a go.

OK, great success! First thing's first, my motor turns:

![test first turns](https://gitlab.cba.mit.edu/jakeread/mkbldcdriver/raw/master/images/test-first-turns.gif)

This is good news. Better news, I can read all of my phase voltages, and it looks like I'm getting satisfactory reading on my current sensing circuitry as well.

The traces here are: (0) Phase U Voltage, (1) Phase V Voltage, (2) Phase W Voltage, (3) 5V Line, (4) 3V3 Line, (5) Phase V Current, and (6) Phase W Current.

Here we see happy PWM cycles above, and what looks like a chopped sinusiod (of current) below.

![test getdown](https://gitlab.cba.mit.edu/jakeread/mkbldcdriver/raw/master/images/test-all-sensing-one.jpg)

Zoom in:

![test getdown](https://gitlab.cba.mit.edu/jakeread/mkbldcdriver/raw/master/images/test-all-sensing-two.jpg)

I sped this up some and it looks more like a chopped sinusoid - this means I should either dial down the sense amplification (I think the two options are gains of 10 and 40), or put smaller shunts on - right now I have 0.005 Ohm shunts (I wanted a more sensitive measurement for smaller currents). 

![test getdown](https://gitlab.cba.mit.edu/jakeread/mkbldcdriver/raw/master/images/test-all-sensing-three.jpg)

That's it for now.