# Machine Kit BLDC Driver: Circuit Development

![schematic](/images/schematic.png)

![routing](/images/routed.png)

The circuit uses a DRV8302 to drive CSD88584 (or CSD88599) half-bridges, which are rated to 40V / 50A and 60V / 30A respectively. Normally, I'll run 24V power. It connects to an AS5147 or AS5047D or AEAT6600 absolute encoder over SPI, and reads phase currents from the amplifier built into the DRV8302, as well as phase voltages from simple voltage dividers (which are clamped to 3V3 with a series of diodes). 

Power is bussed to the board with two M3 Screw Terminals. The board includes room for a 16mm (?) diameter capacitor, and I'll normally use a 1000uF 35V cap here, or as big as I can fit happily.

## Tackling Noise on the BLDC Driver

OK, this is concerning! I have lots of noise coming through the SPI lines, I'm a bit worried if these are causing errors:

![noise](/images/logic-spi-noise-bldcv05-pwm.png)

This perhaps means I need to run that SPI cable with a differential driver, to be honest this would be dissapointing. 

## Notes

 - capacitors should be able to live sticking topside-up, we're going to want to pull heat out of the top ! small space penalty 
 - small FET re-jiggering to get all 12 under two 21.1x21.1mm heatsinks on the topside as well 
 - and mount with sideways fan 1570-1162-ND across both from fet-side 

Fan 
 - 1053-1210-ND 
 - 1053-1375-ND

Heatsinks
 - 

# BOM

1nF
22nF
0.1uF
1uF
2.2uF
10uF

2R2
100R
470R
5K6
10k
39K

Resonator
10uH 0805
LED Y,G,B
DIODE 0805
RTD

SHNT 2520
PWRPAD
2x3 6PIN
RJ45

SLIDE SWITCH
RST BUTTON

XMEGA
DRV8302
FET